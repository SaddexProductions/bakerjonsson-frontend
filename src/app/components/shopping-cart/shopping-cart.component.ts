//npm modules
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { faTrash, faMinus, faPlus, IconDefinition } from '@fortawesome/free-solid-svg-icons';

//local modules
import { StoreService } from 'src/app/views/store/store.service';
import { Product } from 'src/app/views/store/product.model';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in',
        style({ transform: 'translateX(0%)'})
      ),
      transition('void => *', [
        style({
          transform: 'translateX(100%)'
        }),
        animate('400ms ease')
      ]),
      transition('* => void', [
        animate('400ms ease', style({
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})
export class ShoppingCartComponent {

  constructor (private storeService: StoreService) {}

  @Input() readonly enabled: boolean;
  readonly faMinus: IconDefinition = faMinus;
  readonly faPlus: IconDefinition = faPlus;
  readonly faTrash: IconDefinition = faTrash;
  @Input() readonly mobileSearch: boolean;
  @Input() readonly shoppingCart: {count: number; _id: Product}[];
  @Input() readonly sum: number = 0;
  temp: number = 0;
  @Output() closeCartEvent: EventEmitter<void> = new EventEmitter();

  closeShoppingCart (): void {
    this.closeCartEvent.emit();
  }

  clearCart (): void {
    this.storeService.clearCart();
  }
}
