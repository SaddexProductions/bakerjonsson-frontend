import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemRightSideComponent } from './search-item-right-side.component';

describe('SearchItemRightSideComponent', () => {
  let component: SearchItemRightSideComponent;
  let fixture: ComponentFixture<SearchItemRightSideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemRightSideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemRightSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
