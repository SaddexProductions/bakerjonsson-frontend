//npm modules
import { Component, OnInit, Input } from '@angular/core';

//local modules
import { Product } from 'src/app/views/store/product.model';

@Component({
  selector: 'app-search-item-right-side',
  templateUrl: './search-item-right-side.component.html',
  styleUrls: ['./search-item-right-side.component.css']
})
export class SearchItemRightSideComponent implements OnInit {

  @Input() readonly product: Product;

  constructor() { }

  ngOnInit(): void {
  }

}
