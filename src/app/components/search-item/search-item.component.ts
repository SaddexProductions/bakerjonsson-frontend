//npm modules
import { Component, OnInit, Input } from '@angular/core';

//local modules
import { Product } from 'src/app/views/store/product.model';

@Component({
  selector: 'app-search-item',
  templateUrl: './search-item.component.html',
  styleUrls: ['./search-item.component.css']
})
export class SearchItemComponent implements OnInit {

  @Input() readonly product: Product;

  constructor() { }

  ngOnInit(): void {
  }

  replaceChar (input: string): string {

    return input.replace(/ /g, "+");
  }

}
