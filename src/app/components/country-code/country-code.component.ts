//npm modules
import { Component, OnInit, Output, EventEmitter, OnDestroy, Input, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

//local modules
import { Country } from 'src/app/shared/get-country.interface';
import { AuthService } from 'src/app/views/auth/auth.service';

@Component({
  selector: 'app-country-code',
  templateUrl: './country-code.component.html',
  styleUrls: ['./country-code.component.css']
})
export class CountryCodeComponent implements OnInit, OnDestroy {

  private countrySub: Subscription;
  @Output() country: EventEmitter<Country> = new EventEmitter();
  private cleaningTimeout: ReturnType<typeof setTimeout>;
  countries: Country[];
  @Input() readonly includePlus: boolean;
  index: number = -1;
  @Input() inputCountryCode: string;
  selected: string;
  @Input() readonly small: boolean;
  private timeout: ReturnType<typeof setTimeout>;
  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;
  private virtualSearch: string = "";

  constructor(private authService: AuthService) { }

  ngOnInit(): void {

    this.selected = this.inputCountryCode;

    this.countrySub = this.authService.countryStatus
    .subscribe(status => {

      if (status.countries && !this.countries) {
        this.countries = status.countries.map(c => ({
          ...c,
          icon: this.authService.codeToSafeHTML(c.code)
        }));
      }
    });
  }

  ngOnDestroy (): void {

    this.countrySub.unsubscribe();
    
    if (this.timeout) clearTimeout(this.timeout);
    if (this.cleaningTimeout) clearTimeout(this.cleaningTimeout);
  }

  shouldScroll (open: boolean): void {

    if (!open) this.virtualSearch = "";

    if (!this.countries || !open) return;

    const selectedIndex = this.countries.findIndex(elem => elem.code === this.selected);
    if(selectedIndex > -1){
      this.viewPort.scrollToIndex(selectedIndex);
    }
  }

  keySearch (e: KeyboardEvent): void {

    switch (e.keyCode) {        
      case 13: // Enter
      case 16: // Shift
      case 17: // Ctrl
      case 18: // Alt
      case 19: // Pause/Break
      case 20: // Caps Lock
      case 27: // Escape
      case 35: // End
      case 36: // Home
      case 37: // Left
      case 38: // Up
      case 39: // Right
      case 40: // Down

      // Mac CMD Key
      case 91: // Safari, Chrome
      case 93: // Safari, Chrome
      case 224: // Firefox
      break;
      default:
        this.virtualSearch += e.key;

        if (this.cleaningTimeout) {

          clearTimeout(this.cleaningTimeout);
          this.cleaningTimeout = null;
        }

        this.cleaningTimeout = setTimeout(() => {

          this.virtualSearch = "";
        }, 700);

        let selectedIndex = this.countries
        .findIndex(elem => elem.code.match(new RegExp(`^${this.virtualSearch}`, "i")));
        if(selectedIndex === -1){
          
          selectedIndex = this.countries
          .findIndex(elem => elem.name.match(new RegExp(`^${this.virtualSearch}`, "i")));
        }
        
        if (selectedIndex > -1)
        this.viewPort.scrollToIndex(selectedIndex);
      break;
  }
  }

  emitCountry (): void {

    if (this.timeout) return;

    this.timeout = setTimeout(() => {

      this.country.emit(this.countries.find(elem => elem.code === this.selected));
      this.timeout = null;
    }, 10);
  }

}
