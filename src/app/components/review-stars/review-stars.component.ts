//npm modules
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faStarHalfAlt, faStar } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-review-stars',
  templateUrl: './review-stars.component.html',
  styleUrls: ['./review-stars.component.css']
})
export class ReviewStarsComponent implements OnInit {
  
  @Output() emitSetRating: EventEmitter<number> = new EventEmitter();
  readonly faStarHalfAlt: IconDefinition = faStarHalfAlt;
  readonly faStar: IconDefinition = faStar;
  @Input() hover: boolean;
  hoverIndex: number;
  readonly numbers = Array(5).fill(5).map((x,i)=>i);
  @Input() noTitle: boolean;
  @Input() rating: number;
  firstClick: boolean = true;
  firstHover: boolean = true;
  @Input() readonly white: boolean;

  
  setRating (newRating: number): void {
    this.emitSetRating.emit(newRating);

    this.firstClick = false;
  }

  setIndex (i: number): void {

    if (!this.hover) return;

    this.hoverIndex = i;
    this.firstHover = false;
  }

  ngOnInit(): void {

    this.hoverIndex = this.rating;
  }

}
