import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSettingsPopupComponent } from './search-settings-popup.component';

describe('SearchSettingsPopupComponent', () => {
  let component: SearchSettingsPopupComponent;
  let fixture: ComponentFixture<SearchSettingsPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchSettingsPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSettingsPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
