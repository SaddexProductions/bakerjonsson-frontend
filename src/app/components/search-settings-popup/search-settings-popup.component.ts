//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Options } from '@angular-slider/ngx-slider';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

//local modules
import { StoreService } from 'src/app/views/store/store.service';
import { UIService } from '../menu/uiService';
import { Search } from 'src/app/views/store/store.interface';
import { ProductType } from 'src/app/views/store/product-enums';
import { maxPrice } from 'src/app/shared/constants';

@Component({
  selector: 'app-search-settings-popup',
  templateUrl: './search-settings-popup.component.html',
  styleUrls: ['./search-settings-popup.component.css']
})
export class SearchSettingsPopupComponent implements OnInit, OnDestroy {

  doNotClose: boolean;
  enabled: boolean;
  private loadTimeout: ReturnType<typeof setTimeout>;
  readonly priceOptions: Options = {
    ceil: maxPrice,
    floor: 0,
    step: 0.1
  };
  readonly ratingOptions: Options = {
    ceil: 5,
    floor: 1,
    step: 0.1
    
  };
  searchForm: FormGroup;
  private searchFromService: string = "";
  private storeSub: Subscription;
  private uiSub: Subscription;

  constructor(
    private router: Router,
    private storeService: StoreService,
    private uiService: UIService) { }
  
  changeSearchString (): void {

    this.setSearch();
  }

  clearFilters (): void {

    this.initForm();

    this.setSearch();

    this.storeService.clearSearch();
  }

  close (e: Event): void {

    if (this.doNotClose) {

      e.stopPropagation();

      this.doNotClose = false;
    }
  }
  
  ngOnInit (): void {

    this.initForm();

    this.storeSub = this.storeService.status
    .subscribe((status) => {

      if (status.search && status.search.searchstring) {
        this.searchFromService = status.search.searchstring;

        this.searchForm.patchValue({
          ...this.searchForm.value,
          searchstring: status.search.searchstring
        });
      }

      if (status.search && status.search.lowerprice !== undefined &&
      status.search.higherprice !== undefined) {

        this.searchForm.patchValue({
          ...this.searchForm.value,
          priceLow: status.search.lowerprice,
          priceHigh: status.search.higherprice
        });

        this.searchForm.controls.priceLow.markAsTouched();
        this.searchForm.controls.priceHigh.markAsTouched();
      }

      if (status.search && status.search.lowerrating !== undefined &&
      status.search.higherrating !== undefined) {

        this.searchForm.patchValue({
          ...this.searchForm.value,
          ratingLow: status.search.lowerrating,
          ratingHigh: status.search.higherrating
        });

        this.searchForm.controls.ratingLow.markAsTouched();
        this.searchForm.controls.ratingHigh.markAsTouched();
      }

      if (status.search && status.search.available) {

        this.searchForm.patchValue({
          ...this.searchForm.value,
          inStock: true
        });
      }

      if (status.search && status.search.type) {

        for (const key in ProductType) {

          if (status.search.type.indexOf(ProductType[key]) > -1) {

            this.searchForm.patchValue({
              ...this.searchForm.value,
              [key.toLowerCase()]: true
            });
            
          } else {

            this.searchForm.patchValue({
              ...this.searchForm.value,
              [key.toLowerCase()]: false
            });
          }
        }
      }
    });

    this.uiSub = this.uiService.status
    .subscribe(({searchSettings}) => {
      this.enabled = searchSettings;
    });
  }

  ngOnDestroy (): void {

    this.storeSub.unsubscribe();
    this.uiSub.unsubscribe();
  }

  search (): void {

    if (!this.searchForm.valid) return;

    const qParams = this.storeService.createSearchUrl();

    this.router.navigate(['/search'], {
      queryParams: qParams
    });

    this.uiService.resetAll();

  }

  setValue (value: number, keyName: string): void {

    if (this.searchForm.value[keyName] === undefined) return;
    
    this.searchForm.patchValue({
      ...this.searchForm.value,
      [keyName]: value
    });

    this.searchForm.controls[keyName].markAsTouched();

    this.doNotClose = true;

    if (this.loadTimeout) {
      clearTimeout(this.loadTimeout);
      this.loadTimeout = null;
    }

    const instance = this;

    this.loadTimeout = setTimeout(() => { //sliders triggers the function tens of times per second
      instance.setSearch();
    }, 180);
  }

  private setSearch (): void {

    const search: Search = {
      searchstring: this.searchForm.value.searchstring
    };

    if (this.searchForm.value.inStock)
      search.available = true;

    if (this.searchForm.controls.priceLow.touched ||
    this.searchForm.controls.priceHigh.touched) {

      search.lowerprice = this.searchForm.value.priceLow;
      search.higherprice = this.searchForm.value.priceHigh;
    }

    if (this.searchForm.controls.ratingLow.touched ||
    this.searchForm.controls.ratingHigh.touched) {

      search.lowerrating = this.searchForm.value.ratingLow;
      search.higherrating = this.searchForm.value.ratingHigh;
    }

    let allTrue = true;

    for (const key in ProductType) {

      if (this.searchForm.value[key.toLowerCase()]) {

        if (!search.type) search.type = [];
        search.type.push(ProductType[key]);
      } else {

        allTrue = false;
      }
    }

    if (allTrue) delete search.type;

    this.storeService.setSearch({
      ...search
    });
  }

  private initForm (): void {

    this.searchForm = new FormGroup({
      searchstring: new FormControl(this.searchFromService, [Validators.required]),
      priceLow: new FormControl(0, 
        [Validators.required, Validators.min(0), Validators.max(maxPrice)]),
      priceHigh: new FormControl(maxPrice, 
        [Validators.required, Validators.min(0), Validators.max(maxPrice)]),
      ratingLow: new FormControl(1, [Validators.min(1), Validators.max(5)]),
      ratingHigh: new FormControl(5, [Validators.min(1), Validators.max(5)]),
      inStock: new FormControl(false),
      bread: new FormControl(true),
      cookie: new FormControl(true),
      sweet: new FormControl(true)
    });
  }

}
