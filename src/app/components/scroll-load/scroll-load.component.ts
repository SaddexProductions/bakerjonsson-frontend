//npm modules
import { 
  Component, 
  OnInit, 
  Input, 
  Output, 
  EventEmitter, 
  ViewChild, 
  ElementRef 
} from '@angular/core';

//local modules
import { isInViewport } from 'src/app/shared/isInViewport';
import { swipe, SwipeInit } from 'src/app/shared/swipe';

@Component({
  selector: 'app-scroll-load',
  templateUrl: './scroll-load.component.html',
  styleUrls: ['./scroll-load.component.css']
})
export class ScrollLoadComponent implements OnInit {

  @Input() readonly inverted: boolean;
  @Input() readonly loading: boolean;
  @ViewChild('load', { static: true }) loadTriggerElement: ElementRef;
  private swipeCoord: [number, number] = [0, 0];
  private swipeTime: number = 0;
  private timeOut: ReturnType<typeof setTimeout>;
  @Output() private triggerLoadEvent: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  wheelTrigger (e: WheelEvent) {

    if (e.deltaY > 0 && !this.inverted || this.inverted && e.deltaY < 0)
      this.triggerLoad();
  }


  private triggerLoad (): void {

    const isInView = isInViewport(this.loadTriggerElement.nativeElement);

    if (this.timeOut) {

      return;
    }

    if (isInView) {

      const instance = this;

      this.timeOut = setTimeout(() => {
        instance.triggerLoadEvent.emit();
        instance.timeOut = null;
      },500);
    }
  }

  swipePre (e: TouchEvent, when: 'start' | 'end'): void {

    const swipeResult = swipe({
      swipeTime: this.swipeTime,
      swipeCoord: this.swipeCoord,
      e,
      when
    });

    if (swipeResult instanceof(SwipeInit)) {

      this.swipeCoord = swipeResult.swipeCoord;
      this.swipeTime = swipeResult.swipeTime;
    } else if (swipeResult && swipeResult.plane === 'vertical') {

      if ((swipeResult.direction === 'previous' && !this.inverted))
        this.triggerLoad();
    }
  }

}
