//npm modules
import { Component, Input, Output, EventEmitter } from '@angular/core';

//local modules
import { HelperInterface } from 'src/app/shared/helper.interface';
import { generateExpandCollapse } from 'src/app/shared/generate-animation-presets';

@Component({
  selector: 'app-helper',
  templateUrl: './helper.component.html',
  styleUrls: ['./helper.component.css'],
  animations: [
    generateExpandCollapse("0.6s ease")
  ]
})
export class HelperComponent {

  @Output() closeHelper: EventEmitter<void> = new EventEmitter(); 
  @Input() readonly content: HelperInterface;
  @Input() readonly enabled: boolean = false;

  close (): void {

    this.closeHelper.emit();
  }
}
