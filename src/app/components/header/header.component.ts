//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { faShoppingCart, faBars, faUser, faSearch, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

//local modules
import { AuthService } from 'src/app/views/auth/auth.service';
import { User } from 'src/app/views/auth/user.model';
import { UIService } from '../menu/uiService';
import { Product } from 'src/app/views/store/product.model';
import { ContactService } from 'src/app/views/contact/contact.service';
import { generateExpandCollapse } from 'src/app/shared/generate-animation-presets';
import { swipe, SwipeInit } from 'src/app/shared/swipe';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    generateExpandCollapse("0.4s ease")
  ]
})
export class HeaderComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private contactSub: Subscription;
  readonly faBars: IconDefinition = faBars;
  readonly faSearch: IconDefinition = faSearch;
  readonly faShoppingCart: IconDefinition = faShoppingCart;
  readonly faUser: IconDefinition = faUser;
  hamburgerMenu: boolean;
  isAuth: boolean;
  private iV: ReturnType<typeof setInterval>;
  menu: boolean;
  messageCount: number = 0;
  searchMobile: boolean;
  searchSettings: boolean;
  shoppingCart: boolean;
  subMenu: boolean;
  sum: number = 0;
  private swipeCoord: [number, number] = [0, 0];
  private swipeTime: number = 0;
  private uiSub: Subscription;
  user: User;

  constructor(
    private authService: AuthService,
    private contactService: ContactService,
    private titleService: Title,
    private uiService: UIService) { }

  ngOnInit (): void {
  
    this.authSub = this.authService.status.subscribe((status) => {
      this.isAuth = status.isAuth;
      this.user = status.user;

      this.sum = 0;

      if (this.user) {

        this.contactService.countAllUnread(this.user._id);

        for (const item of this.user.shoppingCart) {

          const { count } = item;
          
          const castedItem = <Product>item._id;
    
          this.sum += (castedItem.discountPrice ? castedItem.discountPrice : castedItem.price)*count;
        }

        this.sum = Math.floor((this.sum)*100)/100;
      }

    });

    this.iV = setInterval(() => {

      if (this.user) {

        this.contactService.countAllUnread(this.user._id);
      }

    }, 4000);

    this.contactSub = this.contactService.status
    .subscribe(({unreadMessageCount}) => {

      this.messageCount = unreadMessageCount;

      const oldTitle = this.titleService.getTitle().replace(/\(\d\+?\)/, "");

      if (this.messageCount) {

        this.titleService.setTitle(`(${this.messageCount <= 9 ? this.messageCount : '9+'}) ${oldTitle}`)
      } else {

        this.titleService.setTitle(oldTitle);
      }
    });

    this.uiSub = this.uiService.status.subscribe((status) => {

      this.searchMobile = status.mobileSearch;
      this.menu = status.menu;
      this.shoppingCart = status.shoppingCart;
      this.hamburgerMenu = status.hamburgerMenu;
      this.searchSettings = status.searchSettings;
      this.subMenu = status.subMenu;

    });
  }

  closeMenu (): void {
    this.uiService.setMenu(false);
  }

  closeShoppingCart (): void {
    
    this.uiService.setShoppingCart(false);
  }

  swipePre (e: TouchEvent, when: 'start' | 'end'): void {

    const swipeResult = swipe({
      e,
      when,
      swipeCoord: this.swipeCoord,
      swipeTime: this.swipeTime,
      useDuration: 300,
      distance: 50
    });

    if (swipeResult instanceof(SwipeInit)) {

      this.swipeCoord = swipeResult.swipeCoord;
      this.swipeTime = swipeResult.swipeTime;

    }
    else if (swipeResult) {

      if (swipeResult.plane === "horizontal") {

        if (swipeResult.direction === "previous" && this.hamburgerMenu) {

          this.uiService.setHamburgerMenu(false);
        }
        if (swipeResult.direction === "next" && this.shoppingCart) {

          this.closeShoppingCart();
        }
      }

      this.swipeTime = 0;
      this.swipeCoord = [0, 0];
    }
  }

  logout (): void {
    this.authService.logout();
  }

  toggleMenu (e: Event): void {
    e.stopPropagation();

    this.uiService.setReviewContextMenu(null);
    this.uiService.setShoppingCart(false);
    this.uiService.setHamburgerMenu(false);

    this.uiService.setMenu(!this.menu);
  }

  toggleHamburgerMenu (e: Event): void {

    e.stopPropagation();

    this.uiService.setMenu(false);
    this.uiService.setReviewContextMenu(null);
    this.uiService.setShoppingCart(false);

    this.uiService.setHamburgerMenu(!this.hamburgerMenu);

  }

  toggleSearch (e: Event): void {

    e.stopPropagation();

    this.uiService.setMobileSearch(!this.searchMobile);
  }

  toggleShoppingCart (e: Event): void {

    e.stopPropagation();

    this.uiService.setMenu(false);
    this.uiService.setHamburgerMenu(false);
    this.uiService.setReviewContextMenu(null);

    this.uiService.setShoppingCart(!this.shoppingCart);
  }

  toggleSubMenu (): void {

    this.uiService.setSubmenu(!this.subMenu);
  }

  ngOnDestroy (): void {

    if (this.iV) clearInterval(this.iV);

    this.authSub.unsubscribe();
    this.contactSub.unsubscribe();
    this.uiSub.unsubscribe();
  }

}
