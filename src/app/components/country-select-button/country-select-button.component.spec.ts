import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrySelectButtonComponent } from './country-select-button.component';

describe('CountrySelectButtonComponent', () => {
  let component: CountrySelectButtonComponent;
  let fixture: ComponentFixture<CountrySelectButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrySelectButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrySelectButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
