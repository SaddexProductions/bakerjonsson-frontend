//npm modules
import { Component, OnInit, Input } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faHourglass } from '@fortawesome/free-solid-svg-icons';

//local modules
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-country-select-button',
  templateUrl: './country-select-button.component.html',
  styleUrls: ['./country-select-button.component.css']
})
export class CountrySelectButtonComponent implements OnInit {

  @Input() countrySelected: Country;
  readonly faHourglass: IconDefinition = faHourglass;
  @Input() readonly order: boolean;
  @Input() readonly small: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
