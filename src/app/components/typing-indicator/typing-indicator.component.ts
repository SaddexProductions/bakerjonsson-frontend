//npm modules
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-typing-indicator',
  templateUrl: './typing-indicator.component.html',
  styleUrls: ['./typing-indicator.component.css']
})
export class TypingIndicatorComponent implements OnInit {

  @Input() readonly big: boolean;
  readonly dots = [0, 1, 2];
  @Input() readonly respondent_name: string;
  @Input() readonly typing: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
