//npm modules
import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { 
  faEllipsisV, 
  faThumbsUp,
  faThumbsDown,
  IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';

//local modules
import { Review } from 'src/app/shared/review.model';
import { User } from 'src/app/views/auth/user.model';
import { UIService } from '../menu/uiService';
import { LoginPromptActions } from '../popup-login/actions.enum';
import { VoteOnReviewValue } from '../menu/menu-status.interface';
import { ReviewInputInterface } from 'src/app/views/reviews/review.interfaces';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit, OnDestroy {

  contextMenu: string;
  @Input() edit: boolean = false;
  @Output() deleteEvent: EventEmitter<void> = new EventEmitter();
  editForm: FormGroup;
  readonly faEllipsisV: IconDefinition = faEllipsisV;
  readonly faThumbsUp: IconDefinition = faThumbsUp;
  readonly faThumbsDown: IconDefinition = faThumbsDown;
  @Input() loading: boolean;
  @Input() noLink: boolean;
  @Input() review: Review;
  reviewPrompt = {
    promptMode: null,
    description: ""
  }
  @Output() saveEvent: EventEmitter<ReviewInputInterface> = new EventEmitter();
  private uiSub: Subscription;
  @Input() user: User;
  @Output() voteEvent: EventEmitter<boolean> = new EventEmitter();

  constructor(private uiService: UIService, private router: Router) { }

  ngOnInit (): void {

    this.uiSub = this.uiService.status
    .subscribe(({reviewContextMenu}) => {
      this.contextMenu = reviewContextMenu;
    });

    this.initForm();
  }

  ngOnDestroy (): void {

    this.uiSub.unsubscribe();
  }

  checkChanged: ValidatorFn = (form: FormGroup): ValidationErrors => {

    if (this.review) {
      const reviewValues = {
        title: this.review.title,
        rating: this.review.rating,
        content: this.review.content
      }
  
      for (const key in reviewValues) {
  
        if (reviewValues[key] !== form.value[key])
          return null;
      }
    } else {

      for (const key in form.value) {

        if (form.value[key] && key !== "rating" || key === "rating" && form.value[key] > -1) return null; 
      }
    }

    return {notChanged: true};
  }

  cancelEdit (): void {

    let shouldCancel = this.editForm.errors && this.editForm.errors.notChanged ?
    true : confirm("You have unsaved changes. Are you sure?");

    if (!shouldCancel) return;

    this.edit = false;
    this.initForm();
    this.router.navigate([]);
  }

  deleteReview (): void {

    if (!confirm("Are you sure you want to delete this review? This action cannot be undone."))
      return;

    this.deleteEvent.emit();
  }

  saveReview (): void {

    if (this.editForm.invalid) return;

    this.saveEvent.emit({...this.editForm.value, _id: this.review ? this.review._id : "new"});
  }

  openContextMenu (id: string, e: Event): void {

    e.stopPropagation();

    this.uiService.setShoppingCart(false);
    this.uiService.setHamburgerMenu(false);
    this.uiService.setMenu(false);

    this.uiService
    .setReviewContextMenu(this.contextMenu && this.contextMenu === this.review._id ? null : id);
  }

  openReport (e: Event): void {

    e.stopPropagation();
    this.uiService.setReviewContextMenu(null);

    this.uiService.setReportWindow({enabled: true, _id: this.review._id});
  }

  setRating (rating: number): void {

    this.editForm.patchValue({
      ...this.editForm.value,
      rating
    });
  }

  login (voteValue: boolean, e: Event): void {

    e.stopPropagation();

    this.uiService.setShoppingCart(false);
    this.uiService.setHamburgerMenu(false);
    this.uiService.setMenu(false);
    this.uiService.setReviewContextMenu(null);

    const productId = typeof this.review.product._id === "string" ?
    this.review.product._id
    : this.review.product._id._id;

    this.uiService.setLoginPrompt({value: new VoteOnReviewValue(voteValue,
      this.review),
      action: LoginPromptActions.VOTE_ON_REVIEW,
      enabled: true,
      reload: productId
    });
  }

  vote (voteValue: boolean): void {

    if (!this.user) return;

    this.voteEvent.emit(voteValue);
  }

  private initForm (): void {

    this.editForm = new FormGroup({

      title: new FormControl(this.review ? this.review.title : "", [Validators.required, 
        Validators.minLength(5), Validators.maxLength(50)]),
      content: new FormControl(this.review ? this.review.content : "", [Validators.minLength(10), Validators.maxLength(500)]),
      rating: new FormControl(this.review ? this.review.rating : -1, [Validators.required, Validators.min(1), Validators.max(5)])
    }, this.checkChanged);
  }

}
