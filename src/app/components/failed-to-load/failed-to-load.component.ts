//npm modules
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { faExclamationTriangle, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-failed-to-load',
  templateUrl: './failed-to-load.component.html',
  styleUrls: ['./failed-to-load.component.css']
})
export class FailedToLoadComponent {

  @Input() readonly error: string;
  readonly faExclamationTriangle: IconDefinition = faExclamationTriangle;
  @Output() loadAgainEvent: EventEmitter<void> = new EventEmitter();

  loadAgain (): void {

    this.loadAgainEvent.emit();
  }

}
