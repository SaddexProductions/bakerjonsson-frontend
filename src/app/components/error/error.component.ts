//npm modules
import { Component, Input } from '@angular/core';
import { 
  IconDefinition, 
  faExclamationTriangle, 
  faCheckCircle } from '@fortawesome/free-solid-svg-icons';

//local modules
import { generateFadeAnimation } from 'src/app/shared/generate-animation-presets';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css'],
  animations: [
    generateFadeAnimation(600)
  ]
})
export class ErrorComponent {

  @Input() error: boolean = true;
  @Input() errorMessage: string = "";
  readonly faCheckCircle: IconDefinition = faCheckCircle;
  readonly faExclamationTriangle: IconDefinition = faExclamationTriangle;

}
