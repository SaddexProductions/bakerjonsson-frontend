//npm modules
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle-slider',
  templateUrl: './toggle-slider.component.html',
  styleUrls: ['./toggle-slider.component.css']
})
export class ToggleSliderComponent implements OnInit {

  @Input() readonly color: string = "#00aaff";
  @Input() readonly value: boolean;
  @Output() private readonly setValueEvent: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  toggle (): void {

    this.setValueEvent.emit(!this.value);
  }

}
