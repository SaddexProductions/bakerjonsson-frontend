//npm modules
import { Component, OnInit } from '@angular/core';
import { marker, icon, tileLayer, latLng } from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  readonly layers = [
    marker([ 62.629311, 17.9308 ], {
      icon: icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 41 ],
        iconUrl: 'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
      })})
  ];
  readonly options = {
    layers: [
      tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic2FkZGV4IiwiYSI6ImNqb2lqbmtkNDA4em8zcW1uczA0Nzc0ZGYifQ.w0F2_Yj243eE69C_o8jNHQ', 
      { 
        tileSize: 512,
        maxZoom: 18,
        zoomOffset: -1,
        id: 'mapbox/streets-v11',
        attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>' 
      })
    ],
    zoom: 16,
    center: latLng(62.6291288, 17.9310449)
  };

  constructor() { }

  ngOnInit(): void {
  }

}
