//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { faCheckCircle, faExclamationTriangle, faTimes } from '@fortawesome/free-solid-svg-icons';
import { trigger, state, style, transition, animate } from '@angular/animations';

//local modules
import { UIService } from '../menu/uiService';

@Component({
  selector: 'app-store-message',
  templateUrl: './store-message.component.html',
  styleUrls: ['./store-message.component.css'],
  animations: [
    trigger('fadeInOut', [
      state('in',
        style({ opacity: '1'})
      ),
      transition('void => *', [
        style({
          opacity: '0'
        }),
        animate('700ms ease')
      ]),
      transition('* => void', [
        animate('700ms ease', style({
          opacity: '0'
        }))
      ])
  ])]
})
export class StoreMessageComponent implements OnInit, OnDestroy {

  error: boolean;
  readonly faCheckCircle = faCheckCircle;
  readonly faTimes = faTimes;
  readonly faExclamationTriangle = faExclamationTriangle;
  private menuSub: Subscription;
  message: string;
  offsetLeft: boolean;
  private timeOut: ReturnType<typeof setTimeout>;
  visible: boolean = false;

  constructor(
    private menuService: UIService) { }

  ngOnInit (): void {
    
    this.menuSub = this.menuService.status.subscribe(({shoppingCart, error, info}) => {

      this.offsetLeft = shoppingCart;

      if (info) {
        this.message = info;
        this.error = false;
        this.visible = true;
      }

      else if (error) {
        this.message = error;
        this.error = true;
        this.visible = true;
      }

      if (this.visible) {

        if (this.timeOut) clearTimeout(this.timeOut);

        this.timeOut = setTimeout(() => {

          this.close();
        }, 4000);
      }
    });
  }

  ngOnDestroy (): void {
    this.menuSub.unsubscribe ();
  }

  private close (): void {
    this.menuService.resetError()
    this.menuService.resetInfo();
    
    this.visible = false;
  }

  forceClose (e: Event): void {

    e.stopPropagation();
    
    clearTimeout(this.timeOut);
    this.close();

  }

}
