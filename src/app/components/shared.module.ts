//npm modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';

//local modules
import { ErrorMessageComponent } from './error-message/error-message.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ErrorComponent } from './error/error.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { HelperComponent } from './helper/helper.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { MenuComponent } from './menu/menu.component';
import { RadioButtonsComponent } from './radio-buttons/radio-buttons.component';
import { ResultComponent } from './result/result.component';
import { StoreMessageComponent } from './store-message/store-message.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HamburgerMenuComponent } from './hamburger-menu/hamburger-menu.component';
import { PopupLoginComponent } from './popup-login/popup-login.component';
import { ReviewStarsComponent } from './review-stars/review-stars.component';
import { CartbuttonComponent } from './cartbutton/cartbutton.component';
import { PlusminusComponent } from './plusminus/plusminus.component';
import { ReviewComponent } from './review/review.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { RemoveCharPipe } from './pipes/remove-char.pipe';
import { SortBarComponent } from './sort-bar/sort-bar.component';
import { ScrollLoadComponent } from './scroll-load/scroll-load.component';
import { ErrorBlockComponent } from './error-block/error-block.component';
import { ConvertTimestampPipe } from './pipes/convert-timestamp.pipe';
import { ReadableTimestampPipe } from './pipes/readable-timestamp.pipe';
import { OrderComponent } from './order/order.component';
import { MapComponent } from './map/map.component';
import { FailedToLoadComponent } from './failed-to-load/failed-to-load.component';
import { ReportPromptComponent } from './report-prompt/report-prompt.component';
import { SearchSettingsPopupComponent } from './search-settings-popup/search-settings-popup.component';
import { CloseCrossComponent } from './close-cross/close-cross.component';
import { SearchItemComponent } from './search-item/search-item.component';
import { CutShortPipe } from './pipes/cut-short.pipe';
import { ProductListComponent } from './product-list/product-list.component';
import { AddressBoxComponent } from './address-box/address-box.component';
import { ZeroHourPipe } from './pipes/zero-hour.pipe';
import { OrderResultComponent } from './order-result/order-result.component';
import { CountryPipe } from './pipes/country.pipe';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchItemRightSideComponent } from './search-item-right-side/search-item-right-side.component';
import { ConversationComponent } from './conversation/conversation.component';
import { AvatarComponent } from './avatar/avatar.component';
import { TypingIndicatorComponent } from './typing-indicator/typing-indicator.component';
import { ToggleSliderComponent } from './toggle-slider/toggle-slider.component';
import { CountryCodeComponent } from './country-code/country-code.component';
import { CountrySelectButtonComponent } from './country-select-button/country-select-button.component';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        LeafletModule,
        MatOptionModule,
        ScrollingModule,
        NgxSliderModule,
        ReactiveFormsModule,
        RouterModule,
        MatSelectModule
    ],
    declarations: [
        AddressBoxComponent,
        ErrorMessageComponent,
        ErrorComponent,
        CheckboxComponent,
        HelperComponent,
        SpinnerComponent,
        MenuComponent,
        ShoppingCartComponent,
        RadioButtonsComponent,
        ResultComponent,
        StoreMessageComponent,
        GalleryComponent,
        HamburgerMenuComponent,
        PopupLoginComponent,
        ReviewStarsComponent,
        CartbuttonComponent,
        PlusminusComponent,
        ReviewComponent,
        CapitalizePipe,
        RemoveCharPipe,
        SortBarComponent,
        ScrollLoadComponent,
        ErrorBlockComponent,
        ConvertTimestampPipe,
        ReadableTimestampPipe,
        OrderComponent,
        MapComponent,
        FailedToLoadComponent,
        ReportPromptComponent,
        SearchSettingsPopupComponent,
        CloseCrossComponent,
        SearchItemComponent,
        CutShortPipe,
        ProductListComponent,
        ZeroHourPipe,
        OrderResultComponent,
        CountryPipe,
        SearchBarComponent,
        SearchItemRightSideComponent,
        ConversationComponent,
        AvatarComponent,
        TypingIndicatorComponent,
        ToggleSliderComponent,
        CountryCodeComponent,
        CountrySelectButtonComponent
    ],
    exports: [
        AddressBoxComponent,
        AvatarComponent,
        CapitalizePipe,
        CartbuttonComponent,
        CheckboxComponent,
        CommonModule,
        ConvertTimestampPipe,
        ConversationComponent,
        CountryCodeComponent,
        CountrySelectButtonComponent,
        CutShortPipe,
        ErrorMessageComponent,
        ErrorComponent,
        ErrorBlockComponent,
        FailedToLoadComponent,
        GalleryComponent,
        HamburgerMenuComponent,
        HelperComponent,
        MapComponent,
        MenuComponent,
        OrderComponent,
        OrderResultComponent,
        PlusminusComponent,
        PopupLoginComponent,
        ProductListComponent,
        RadioButtonsComponent,
        ReadableTimestampPipe,
        ReportPromptComponent,
        ResultComponent,
        ReviewComponent,
        ReviewStarsComponent,
        SearchBarComponent,
        SearchItemComponent,
        SearchItemRightSideComponent,
        SearchSettingsPopupComponent,
        ScrollLoadComponent,
        ShoppingCartComponent,
        SortBarComponent,
        SpinnerComponent,
        StoreMessageComponent,
        ToggleSliderComponent,
        TypingIndicatorComponent,
        RemoveCharPipe,
        ZeroHourPipe
    ]
})
export class SharedModule {}