import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseCrossComponent } from './close-cross.component';

describe('CloseCrossComponent', () => {
  let component: CloseCrossComponent;
  let fixture: ComponentFixture<CloseCrossComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseCrossComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseCrossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
