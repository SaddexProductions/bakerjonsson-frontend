//npm modules
import { Component} from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

//local modules
import { UIService } from '../menu/uiService';

@Component({
  selector: 'app-close-cross',
  templateUrl: './close-cross.component.html',
  styleUrls: ['./close-cross.component.css']
})
export class CloseCrossComponent {

  readonly faTimes: IconDefinition = faTimes;

  constructor(private uiService: UIService) { }

  close (): void {

    this.uiService.resetAll();
  }

}
