//npm modules
import { Component, Input } from '@angular/core';

//local modules
import { StreetAddress } from '../../shared/address.interface';

@Component({
  selector: 'app-address-box',
  templateUrl: './address-box.component.html',
  styleUrls: ['./address-box.component.css']
})
export class AddressBoxComponent {

  @Input() address: StreetAddress;

}
