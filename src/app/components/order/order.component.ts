//npm modules
import { Component, Input, OnInit } from '@angular/core';
import { faTimesCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl, Validators, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

//local modules
import { Order } from 'src/app/views/orders/order.model';
import { OrderStatusEnum } from 'src/app/views/orders/order-status.enum';
import { nameRegex } from 'src/app/shared/constants';
import { User } from 'src/app/views/auth/user.model';
import { Option } from '../radio-buttons/option.interface';
import { OrdersService } from 'src/app/views/orders/orders.service';
import { AuthService } from 'src/app/views/auth/auth.service';
import { generateExpandCollapse } from 'src/app/shared/generate-animation-presets';
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css'],
  animations: [
    generateExpandCollapse("0.6s ease")
  ]
})
export class OrderComponent implements OnInit {

  addressArray: string[] = [];
  addressMode: 0 | 1 = 0;
  displayAddress: boolean;
  displayProducts: boolean;
  @Input() edit: boolean;
  editAddressForm: FormGroup;
  readonly faTimesCircle: IconDefinition = faTimesCircle;
  @Input() readonly loading: boolean;
  noLink: boolean;
  readonly options: Option[] = [
    {
      value: "standard",
      description: "Standard address"
    },
    {
      value: "standard",
      description: "Custom"
    }
  ];
  @Input() readonly order: Order;
  orderStatus: typeof OrderStatusEnum = OrderStatusEnum;
  @Input() readonly presetCountry: Country;
  @Input() redirect: boolean;
  selectedCountry: Country;
  @Input() readonly redirectOnCancel: boolean;
  @Input() readonly shouldReload: boolean;
  @Input() readonly user: User;

  constructor (
    private authService: AuthService,
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private router: Router) {}

  checkChanged: ValidatorFn = (form: FormGroup): ValidationErrors => {

    if (!this.order.address) return null;

    const address = {...this.order.address};
  
    for (const key in form.value) {

      if (!address[key]) address[key] = "";

      if (form.value[key] !== address[key]) {
        return null;
      }
    }

    return { notChanged: true };
  }

  cancelOrder (): void {

    const shouldCancel = confirm("Are you sure you want to cancel this order?");
    if (this.order.status !== this.orderStatus.PROCESSING || !shouldCancel) return;

    this.orderService.cancelOrder(this.order._id);
  }

  cancelEdit (): void {

    let empty = true;

    for (const key in this.editAddressForm.value) {

      if (this.editAddressForm.value[key] !== "" && key !== "countryCode") empty = false;
    }

    const isChanged = this.editAddressForm.errors && !this.editAddressForm.errors.notChanged ||
    !this.editAddressForm.errors;

    if ((this.addressMode === 1 && isChanged
    && this.order.address) || (!this.order.address && !isChanged &&
    !empty)) {

      const discard = confirm("You have unsaved changes. Are you sure you want to discard them?");
      if (!discard) return;
    }

    if (this.redirect) this.router.navigate([]);

    if (this.redirectOnCancel) this.router.navigate([], {replaceUrl: true});
    this.edit = false;
    this.initForm();
  }

  clearOrders (): void {

    this.orderService.clearOrders();
  }

  async ngOnInit (): Promise<void> {

    this.initForm();

    this.noLink = this.route.snapshot.params.id === this.order._id;
    
    this.selectedCountry = {
      ...this.presetCountry
    };

    if (this.order.address) {

      this.selectedCountry = 
      await this.authService.getCountryById(this.order.address.countryCode);

      Object.keys(this.order.address).map(k => {
        this.addressArray.push(this.order.address[k]);
      });
    }
  }

  setCountry (country: Country): void {

    this.selectedCountry = country;

    this.editAddressForm.patchValue({
      ...this.editAddressForm.value,
      countryCode: country.code.toUpperCase()
    });
  }

  submitAddressForm (): void {

    if (this.editAddressForm.invalid && this.addressMode) return;

    this.orderService
    .editOrderAddress(this.order._id, this.addressMode ? this.editAddressForm.value : false);
  }

  convertToStringArray (value: string): string {

    return JSON.stringify([value]);
  }

  private initForm (): void {

    this.addressMode = this.order.address ? 1 : 0;

    this.editAddressForm = new FormGroup({
      street: new FormControl(this.order.address ? this.order.address.street : "", [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(200)
      ]),
      additional: new FormControl(this.order.address && this.order.address.additional ? 
        this.order.address.additional : "", [
        Validators.maxLength(50)
      ]),
      zip: new FormControl(this.order.address ? this.order.address.zip : "", [
        Validators.required,
        Validators.maxLength(10),
      ]),
      city: new FormControl(this.order.address ? this.order.address.city : "", [
        Validators.required,
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      province: new FormControl(this.order.address && this.order.address.province ? 
        this.order.address.province : "", [
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      countryCode: new FormControl(this.order.address ?  
        this.order.address.countryCode : this.presetCountry.code, [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(3)
        ])
    }, this.checkChanged);
  }

}
