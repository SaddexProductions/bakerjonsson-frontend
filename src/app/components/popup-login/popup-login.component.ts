//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { faTimes, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { trigger, state, style, transition, animate } from '@angular/animations';

//local modules
import { UIService } from '../menu/uiService';
import { AuthService } from 'src/app/views/auth/auth.service';
import { StoreService } from 'src/app/views/store/store.service';
import { ExtendedProduct } from 'src/app/views/store/front/product-categories.interface';
import { VoteOnReviewValue } from '../menu/menu-status.interface';
import { splitDigits } from 'src/app/shared/splitDigits';
import { ReviewsService } from 'src/app/views/reviews/reviews.service';

@Component({
  selector: 'app-popup-login',
  templateUrl: './popup-login.component.html',
  styleUrls: ['./popup-login.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in',
        style({ transform: 'translateY(0%)'})
      ),
      transition('void => *', [
        style({
          transform: 'translateY(-100%)'
        }),
        animate('400ms ease')
      ]),
      transition('* => void', [
        animate('400ms ease', style({
          transform: 'translateY(-100%)'
        }))
      ])
    ])
  ]
})
export class PopupLoginComponent implements OnInit, OnDestroy {

  private authSub: Subscription
  enabled: boolean;
  errorMessage: string = "";
  readonly faTimes: IconDefinition = faTimes;
  private isAuth: boolean;
  private item: ExtendedProduct | VoteOnReviewValue;
  loading: boolean;
  loginForm: FormGroup;
  private reload: string = null;
  private reviewCount: number = 20;
  private reviewSub: Subscription;
  sentSms: boolean = false;
  twoFactor: boolean = false;
  twoFactorForm: FormGroup;
  private uiSub: Subscription;

  constructor(private authService: AuthService,
    private storeService: StoreService, 
    private reviewService: ReviewsService,
    private uiService: UIService) { }

  ngOnInit (): void {

    this.uiSub = this.uiService.status
    .subscribe((status) => {

      if (status.loginPrompt) {
        this.initLoginForm();
        this.enabled = status.loginPrompt.enabled;

        this.reload = status.loginPrompt.reload;
        this.item = status.loginPrompt.value;
      }
    });

    this.reviewSub = this.reviewService.status.subscribe((status) => {

      if (status.reviews && status.reviews.length > 20) this.reviewCount = status.reviews.length;
    });

    this.authSub = this.authService.status
    .subscribe(async (status) => {

      this.errorMessage = status.error;
      this.loading = status.loading;

      if (!this.enabled) return;

      if (status.isAuth && !this.isAuth) {

        let doneLoading = false;
        if (this.reload) {
          doneLoading = await this.reviewService.getReviews(null, {
            customLimit: this.reviewCount, 
            productId: this.item instanceof VoteOnReviewValue &&
            status.user &&
            this.item.review.creator._id !== status.user._id ? this.item.review.product._id.toString() : null});
          this.storeService.checkIfUserHasOrderedAndReviewed(this.reload);
        } else {
          doneLoading = true;
        }

        if (doneLoading && this.item) {
          if (this.item instanceof VoteOnReviewValue) {

            if (status.user._id !== this.item.review.creator._id) {
  
              this.reviewService.vote({
                positive: this.item.positive,
                review: this.item.review
              });
            }
          }
  
          else {
  
            this.storeService.addToCart({...this.item});
          }
        }

        this.close();
      } else if (status.twoFactorToken) {

        this.initTwoFactorForm();
        this.twoFactor = true;
      }

      this.isAuth = status.isAuth;
    });

    if (!this.loginForm) this.initLoginForm();
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.reviewSub.unsubscribe();
    this.uiSub.unsubscribe();
  }

  close (): void {

    this.uiService.resetAll();

    this.authService.resetTwoFactor();
    this.authService.resetError();
    this.twoFactor = false;
  }

  login (): void {

    if (this.loginForm.invalid) return;

    this.authService.login(this.loginForm.value);
  }

  sendSms (): void {

    if (this.sentSms) return;

    this.authService.sendSms();
  }

  twoFactorAuth (): void {

    if (this.twoFactorForm.invalid) return;

    this.authService.twoFactorAuth(this.twoFactorForm.value.authyToken, 
      this.twoFactorForm.value.saveDevice);
  }

  toggleCheckbox (value: boolean) {

    this.twoFactorForm.patchValue({
      ...this.twoFactorForm.value,
      saveDevice: value
    })
  }

  formatDigits (e: Event) {

    this.twoFactorForm.patchValue({
      ...this.twoFactorForm.value,
      authyToken: splitDigits((e.target as HTMLInputElement).value, 
      {value: this.twoFactorForm.value.authyToken, offset: 2, max: 9})
    });
  }

  private initLoginForm () {

    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(25)])
    });
  }

  private initTwoFactorForm () {

    this.twoFactorForm = new FormGroup({
      authyToken: new FormControl(null, [Validators.required, Validators.pattern(/^[\d\s]+$/)]),
      saveDevice: new FormControl(false)
    });
  }


}
