export enum LoginPromptActions {
    ADD_TO_CART = "ADD_TO_CART",
    VOTE_ON_REVIEW = "VOTE_ON_REVIEW"
}