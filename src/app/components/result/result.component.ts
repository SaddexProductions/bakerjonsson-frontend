//npm modules
import { Component, Input } from '@angular/core';
import { faTimesCircle, faCheckCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent {

  readonly faCheckCircle = faCheckCircle;
  readonly faTimesCircle = faTimesCircle;
  @Input() readonly error: boolean;
  @Input() readonly text: string;

}
