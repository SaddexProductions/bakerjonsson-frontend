//npm modules
import { Component, OnInit, Input } from '@angular/core';
import { faArrowCircleLeft, faArrowCircleRight, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

//local modules
import { Image } from './img.interface';
import { generateFadeAnimation } from 'src/app/shared/generate-animation-presets';
import { swipe, SwipeInit } from 'src/app/shared/swipe';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css'],
  animations: [
    generateFadeAnimation(400)
  ]
})
export class GalleryComponent implements OnInit {

  @Input() readonly images: Image[];
  ix: number = 0;
  readonly faArrowCircleLeft: IconDefinition = faArrowCircleLeft;
  readonly faArrowCircleRight: IconDefinition = faArrowCircleRight;
  private interval: ReturnType<typeof setInterval>;
  private swipeCoord: [number, number] = [0, 0];
  private swipeTime: number = 0;

  constructor(private router: Router) { }

  ngOnInit(): void {

    this.setIV();
  }

  redirectTo (link: string): void {
    this.router.navigate([link]);
  }

  next (clear?: boolean): void {

    if (clear) this.setIV();

    if (this.ix === (this.images.length-1)) {
      this.ix = 0;
    } else {
      this.ix++;
    }
  }

  previous (): void {

    this.setIV();

    if (this.ix === 0) {
      this.ix = this.images.length-1;
    }  else {
      this.ix--;
    }
  }

  swipe (e: TouchEvent, when: 'start' | 'end'): void {

    const swipeResult = swipe(
      {
        when,
        e,
        swipeCoord: this.swipeCoord,
        swipeTime: this.swipeTime,
        distance: 100
      }
    );

    if (swipeResult instanceof SwipeInit) {

      this.swipeTime = swipeResult.swipeTime;
      this.swipeCoord = swipeResult.swipeCoord;
    } else if (swipeResult) {

      if (swipeResult.plane === "horizontal") {
        
        if (swipeResult.direction === "next") this.previous();
        else this.next();
      }

      this.swipeCoord = [0, 0];
      this.swipeTime = 0;
    }
  }

  setIx (i: number): void {

    this.setIV();

    this.ix = i;
  }

  private setIV (): void {
    clearInterval(this.interval);
    this.interval = setInterval(() => this.next(), 5000);
  }

}
