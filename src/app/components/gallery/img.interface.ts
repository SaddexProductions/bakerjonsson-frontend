export interface Image {
    src: string;
    desc: string;
    alt: string;
    link: string;
}