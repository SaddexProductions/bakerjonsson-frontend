//npm modules
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})
export class AvatarComponent implements OnInit, OnChanges {

  constructor() { }

  @Input() readonly name: string;
  @Input() readonly unread: boolean;
  toDisplay: string;

  ngOnInit(): void {

    this.setToDisplay();
  }

  ngOnChanges (): void {

    this.setToDisplay();
  }

  private setToDisplay (): void {

    const firstLetter = this.name.charAt(0).toUpperCase();

    let secondLetter = "";

    const splitRegex: RegExp = /[-_\s]|([a-z](?=[A-Z]))/;

    if (splitRegex.test(this.name)) {

      const splitName = this.name.split(splitRegex);

      secondLetter = splitName[splitName.length-1].charAt(0).toUpperCase();
    } else {

      secondLetter = this.name.charAt(1).toLowerCase();
    }

    this.toDisplay = `${firstLetter}${secondLetter}`;
  }

}
