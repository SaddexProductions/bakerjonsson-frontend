//npm modules
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

//local modules
import { SortOrdersEnum } from 'src/app/views/orders/sort-orders.enum';
import { SortOrder } from 'src/app/views/store/item/reviews/sort-order.enum';
import { SortReviewAfter } from 'src/app/views/store/item/reviews/sort-reviews.enum';

@Component({
  selector: 'app-sort-bar',
  templateUrl: './sort-bar.component.html',
  styleUrls: ['./sort-bar.component.css']
})
export class SortBarComponent implements OnInit {

  @Output() private changeOrder: EventEmitter<Event> = new EventEmitter();
  @Output() private changeSort: EventEmitter<Event> = new EventEmitter();
  @Input() noFlex: boolean;
  @Input() orderValue: SortOrder;
  @Input() sortAfter: typeof SortOrdersEnum | typeof SortReviewAfter;
  sortOrder: typeof SortOrder = SortOrder;
  @Input() sortValue: SortOrdersEnum | SortReviewAfter;

  constructor() { }

  ngOnInit(): void {
  }

  setOrder (e: Event): void {
    
    this.changeOrder.emit(e);
  }

  setSort (e: Event): void {
    
    this.changeSort.emit(e);
  }

}
