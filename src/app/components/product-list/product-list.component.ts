//npm modules
import { Component, OnInit, Input } from '@angular/core';

//local modules
import { Product } from 'src/app/views/store/product.model';
import { CartItem } from 'src/app/shared/cartitem.interface';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @Input() readonly cancelled: boolean;
  @Input() readonly cartItems: CartItem[];
  @Input() readonly links: boolean;
  @Input() readonly products: Product[];
  @Input() readonly success: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
