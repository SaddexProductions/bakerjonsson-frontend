//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { faCheckCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';

//local modules
import { ReportPrompt } from '../menu/menu-status.interface';
import { UIService } from '../menu/uiService';
import { AuthService } from 'src/app/views/auth/auth.service';
import { User } from 'src/app/views/auth/user.model';
import { ContactService } from 'src/app/views/contact/contact.service';

@Component({
  selector: 'app-report-prompt',
  templateUrl: './report-prompt.component.html',
  styleUrls: ['./report-prompt.component.css']
})
export class ReportPromptComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private contactSub: Subscription;
  error: string;
  readonly faCheckCircle: IconDefinition = faCheckCircle;
  info: string;
  loading: boolean;
  reportForm: FormGroup;
  reportProps: ReportPrompt;
  timeOut: ReturnType<typeof setTimeout>;
  private uiSub: Subscription;
  user: User;

  constructor(
    private authService: AuthService,
    private contactService: ContactService,
    private uiService: UIService) { }

  ngOnInit (): void {

    this.authSub = this.authService
    .status.subscribe((status) => {
      this.user = status.user;
    });

    this.contactSub = this.contactService.status
    .subscribe((status) => {

      this.error = status.error;
      this.info = status.info;
      this.loading = status.loading;

      const instance = this;

      if (this.info) this.timeOut = setTimeout(() => {

        if (instance.reportProps && instance.reportProps.enabled) {
          instance.uiService.setReportWindow(null);
          instance.contactService.reset();
        }
        
      }, 4000);
    });

    this.uiSub = this.uiService.status
    .subscribe((status) => {

      if (status.report && status.report.enabled) this.initForm();
      else {

        clearTimeout(this.timeOut);

        const instance = this;

        setTimeout(() => {

          if (this.reportProps && this.reportProps.enabled) {

            instance.contactService.setError("");
            instance.contactService.setLoading(false);
          }
        }, 300);
      }

      this.reportProps = {...status.report};
    });

    if (!this.reportForm) this.initForm();
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.contactSub.unsubscribe();
    this.uiSub.unsubscribe();

    this.contactService.reset();
  }

  private initForm (): void {

    this.reportForm = new FormGroup({
      reason: new FormControl("", [
        Validators.required, 
        Validators.minLength(3), 
        Validators.maxLength(50)]),
      details: new FormControl("", [
        Validators.minLength(3),
        Validators.maxLength(150)
      ]),
      sendEmail: new FormControl(false),
      email: new FormControl(this.user ? this.user.email : "", 
        [Validators.email, Validators.maxLength(200)])
    });
  }

  checkSendEmail (value: boolean): void {

    this.reportForm.patchValue({
      ...this.reportForm.value,
      sendEmail: value
    });
  }

  submitReport (): void {

    if (this.reportForm.invalid) return;

    this.contactService.submitReport({
      ...this.reportForm.value,
      id: this.reportProps._id
    });
  }

}
