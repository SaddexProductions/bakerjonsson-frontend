import { ValidatorFn, FormGroup } from '@angular/forms';

export const emailRequiredCheckValidator: ValidatorFn = (form: FormGroup): null => {
    
    const email = form.get("email");
    const sendEmail = form.get("sendEmail");

    const errors = {
        ...sendEmail.errors
    }

    if(sendEmail.value && !email.value) {

        email.setErrors({
            ...errors,
            required: true
        });
    } else {
        if (errors && errors.required) delete errors.required;

        email.setErrors({
            ...errors
        });
    }

    return null;
}