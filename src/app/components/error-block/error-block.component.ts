//npm modules
import { Component, Input } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-error-block',
  templateUrl: './error-block.component.html',
  styleUrls: ['./error-block.component.css']
})
export class ErrorBlockComponent {

  readonly faExclamationTriangle: IconDefinition = faExclamationTriangle;
  @Input() readonly loading: boolean;
  @Input() readonly big: boolean;
  @Input() readonly error: string;

}
