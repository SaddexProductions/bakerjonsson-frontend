//npm modules
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCheck, faCheckDouble } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

//local modules
import { Conversation, Participant } from 'src/app/views/contact/conversation.model';
import { User } from 'src/app/views/auth/user.model';
import { ContactService } from 'src/app/views/contact/contact.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit, OnDestroy {

  @Input() readonly conversation: Conversation;
  private contactSub: Subscription;
  readonly dots = [0, 1, 2];
  readonly faCheck: IconDefinition = faCheck;
  readonly faCheckDouble: IconDefinition = faCheckDouble;
  otherParticipant: Participant;
  respondent_name: string;
  timeStamp: Date;
  @Input() readonly user: User;

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {

    this.timeStamp = new Date();

    if (!this.otherParticipant) this.getOther();

    this.respondent_name = this.otherParticipant.name ? this.otherParticipant.name : this.otherParticipant.id;

    this.contactSub = this.contactService.status.subscribe(({timeStamp}) => {

      if (timeStamp) this.timeStamp = timeStamp;
      this.setTyping();
    });
  }

  private setTyping (): void {

    this.otherParticipant.typingNow = ContactService
    .setTypingNow(this.timeStamp ? this.timeStamp : new Date(), new Date(this.otherParticipant.lastTyping));
  }

  ngOnDestroy (): void {

    this.contactSub.unsubscribe();
  }

  private getOther (): void {

    this.otherParticipant = ContactService.getOtherParticipant(this.conversation, this.user._id);
  }

}
