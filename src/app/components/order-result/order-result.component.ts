//npm modules
import { Component, Input } from '@angular/core';
import { faCheckCircle, faTimesCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-order-result',
  templateUrl: './order-result.component.html',
  styleUrls: ['./order-result.component.css']
})
export class OrderResultComponent {

  @Input() readonly cancelled: boolean;
  readonly faCheckCircle: IconDefinition = faCheckCircle;
  readonly faTimesCircle: IconDefinition = faTimesCircle;
}
