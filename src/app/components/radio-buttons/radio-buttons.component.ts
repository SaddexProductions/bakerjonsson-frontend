//npm modules
import { Component, Input, Output, EventEmitter } from '@angular/core';

//local modules
import { Option } from './option.interface';

@Component({
  selector: 'app-radio-buttons',
  templateUrl: './radio-buttons.component.html',
  styleUrls: ['./radio-buttons.component.css']
})
export class RadioButtonsComponent {

  @Input() options: Option[];
  @Input() selected: number;
  @Output() private selectEvent: EventEmitter<number> = new EventEmitter();

  select (index: number): void {
    this.selectEvent.emit(index);
  }
}
