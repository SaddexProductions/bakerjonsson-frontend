//npm modules
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeChar'
})
export class RemoveCharPipe implements PipeTransform {

  transform(value: string, charToRemove: string, replacement?: string): string {
    
    const regex = new RegExp(charToRemove, "gi");

    return value.replace(regex, replacement ? replacement : " ");
  }

}
