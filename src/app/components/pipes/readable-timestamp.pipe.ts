//npm modules
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'readableTimestamp'
})
export class ReadableTimestampPipe implements PipeTransform {

  transform(value: string): string {

    try {
      return value.split('T').join('\xa0\xa0\xa0\xa0').split(':').splice(0, 2).join(':') + " (UTC)";
    } catch (_) {
      return value;
    }
    
  }

}
