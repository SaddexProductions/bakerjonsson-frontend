//npm modules
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutShort'
})
export class CutShortPipe implements PipeTransform {

  transform(value: string, length: number): string {

    let returnString = value;
    
    if (value.length > length) {

      const splitStr = returnString.split("");
      splitStr.length = length-2;

      returnString = splitStr.join("") + "...";
    } 

    return returnString;
  }

}
