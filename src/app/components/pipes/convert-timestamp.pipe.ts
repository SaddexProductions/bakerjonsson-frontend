//npm modules
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertTimestamp'
})
export class ConvertTimestampPipe implements PipeTransform {

  transform(firstDate: string, secondDate?: Date): string {

    try {
      new Date(firstDate);
    } catch (_) {
      return firstDate;
    }

    if (!secondDate) secondDate = new Date();
    
    // get total seconds between the times
    const delta = Math.abs(secondDate.getTime() - new Date(firstDate).getTime())/1000;

    const values = [
        //calculate years
        Math.floor(delta / (86400 * 365)),
        //calculate months
        Math.floor(delta / (86400 * 30)),
        // calculate weeks
        Math.floor(delta / (86400 * 7)),
        // calculate whole days
        Math.floor(delta / 86400),
        // calculate whole hours
        Math.floor(delta / 3600),
        // calculate whole minutes
        Math.floor(delta / 60)
    ];
    
    const labels = [
        "year",
        "month",
        "week",
        "day",
        "hour",
        "minute",
        "second"
    ];

    for (let i = 0; i < labels.length; i++) {

        if (values[i] >= 1) return `${values[i]} ${labels[i]}${values[i] > 1 ? 's' : ''} ago`;
    }

    return "just now";
  }

}
