//npm modules
import { Pipe, PipeTransform } from '@angular/core';

//local modules
import countries from '../../shared/countries.json';

@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {

  transform(value: string): string {
     
    const country = countries.find(c => c.code === value.toLowerCase());
    return country ? country.name : "Unknown";

  }

}
