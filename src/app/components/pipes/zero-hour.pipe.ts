//npm modules
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'zeroHour'
})
export class ZeroHourPipe implements PipeTransform {

  transform(value: string): string {
    
    const splitted = value.split(":");

    const secondPart = splitted[1].split("");
    secondPart[0] = "0";
    secondPart[1] = "0";
    splitted[1] = secondPart.join("");

    return splitted.join(":");
  }

}
