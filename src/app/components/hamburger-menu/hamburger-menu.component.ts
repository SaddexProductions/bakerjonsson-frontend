//npm modules
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';
import { trigger, state, style, transition, animate } from '@angular/animations';

//local modules
import { ProductType } from 'src/app/views/store/product-enums';

@Component({
  selector: 'app-hamburger-menu',
  templateUrl: './hamburger-menu.component.html',
  styleUrls: ['./hamburger-menu.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in',
        style({ transform: 'translateX(0%)'})
      ),
      transition('void => *', [
        style({
          transform: 'translateX(-100%)'
        }),
        animate('400ms ease')
      ]),
      transition('* => void', [
        animate('400ms ease', style({
          transform: 'translateX(-100%)'
        }))
      ])
    ])
  ]
})
export class HamburgerMenuComponent {

  readonly faChevronDown: IconDefinition = faChevronDown;
  readonly faChevronUp: IconDefinition = faChevronUp;
  readonly categories: typeof ProductType = ProductType;
  @Input() readonly enabled: boolean;
  @Input() readonly mobileSearch: boolean;
  @Input() readonly subMenu: boolean;
  @Output() toggleSubMenuEvent: EventEmitter<void> = new EventEmitter();

  toggleSubMenu (e: Event): void {

    e.stopPropagation();

    this.toggleSubMenuEvent.emit();
  }

}
