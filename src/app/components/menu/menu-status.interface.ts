//local modules
import { LoginPromptActions } from '../popup-login/actions.enum';
import { ExtendedProduct } from 'src/app/views/store/front/product-categories.interface';
import { Review } from 'src/app/shared/review.model';

export interface MenuStatus {
    error: string;
    hamburgerMenu: boolean;
    info: string;
    loginPrompt: LoginPrompt;
    menu: boolean;
    mobileSearch: boolean;
    report: ReportPrompt;
    reviewContextMenu: string;
    searchSettings: boolean;
    subMenu: boolean;
    shoppingCart: boolean;
}

export interface LoginPrompt {
    enabled: boolean;
    reload: string;
    action?: LoginPromptActions;
    value?: VoteOnReviewValue | ExtendedProduct;
}

export interface ReportPrompt {

    _id: string;
    enabled: boolean;
}

export class VoteOnReviewValue {

    constructor (public positive: boolean,
        public review: Review){

    }
}