//npm modules
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

//local modules
import { MenuStatus, LoginPrompt, ReportPrompt } from './menu-status.interface';


@Injectable({providedIn: 'root'})
export class UIService {

    readonly status: BehaviorSubject<MenuStatus> = new BehaviorSubject({
        error: "",
        info: "",
        menu: false,
        hamburgerMenu: false,
        loginPrompt: {
            enabled: false,
            reload: null
        },
        mobileSearch: false,
        report: null,
        reviewContextMenu: null,
        searchSettings: false,
        subMenu: false,
        shoppingCart: false
    });

    resetAll (): void {

        this.status.next({
            ...this.status.value,
            loginPrompt: {
                enabled: false,
                action: null,
                value: null,
                reload: null
            },
            menu: false,
            hamburgerMenu: false,
            report: null,
            reviewContextMenu: null,
            searchSettings: false,
            subMenu: false,
            shoppingCart: false
        });
    }

    setError (value: string): void {

        this.status.next({
            ...this.status.value,
            error: value
        });
    }

    resetError (): void {
        this.status.next({
            ...this.status.value,
            error: ""
        });
    }

    resetInfo (): void {

        this.status.next({
            ...this.status.value,
            info: ""
        });
    }

    setInfo (value: string): void {
        this.status.next({
            ...this.status.value,
            info: value
        });
    }

    setLoginPrompt (value: LoginPrompt): void {

        this.status.next({
            ...this.status.value,
            loginPrompt: {
                ...value
            }
        });
    }

    setMenu (value: boolean): void {

        this.status.next({
            ...this.status.value,
            menu: value
        });
    }

    setReportWindow (value: ReportPrompt): void {

        this.status.next({
            ...this.status.value,
            report: value
        });
    }

    setReviewContextMenu (value: string): void {
        this.status.next({
            ...this.status.value,
            reviewContextMenu: value
        });
    }

    setSearchSettings (value: boolean): void {

        this.status.next({
            ...this.status.value,
            searchSettings: value
        });
    }

    setShoppingCart (value: boolean): void {

        this.status.next({
            ...this.status.value,
            shoppingCart: value
        });
    }

    setHamburgerMenu (value: boolean): void {

        this.status.next({
            ...this.status.value,
            hamburgerMenu: value
        });
    }

    setMobileSearch (value: boolean): void {

        this.status.next({
            ...this.status.value,
            mobileSearch: value
        });
    }

    setSubmenu (value: boolean): void {

        this.status.next({
            ...this.status.value,
            subMenu: value
        });
    }
}