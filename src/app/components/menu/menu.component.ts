//npm modules
import { Component, Input, Output, EventEmitter } from '@angular/core';

//local modules
import { User } from 'src/app/views/auth/user.model';
import { OrdersService } from 'src/app/views/orders/orders.service';
import { ReviewsService } from 'src/app/views/reviews/reviews.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {

  @Input() readonly messageCount: number;
  @Input() readonly mobileSearch: boolean;
  @Input() readonly user: User;
  @Output() closeMenuEvent: EventEmitter<void> = new EventEmitter();
  @Output() logoutEvent: EventEmitter<void> = new EventEmitter();

  closeMenu (): void {

    this.closeMenuEvent.emit();
  }

  logout (): void {
    this.logoutEvent.emit();
  }

  constructor(private orderService: OrdersService,
    private reviewService: ReviewsService) { }

  clearOrders (): void {

    this.orderService.clearOrders();
  }

  clearReviews (): void {

    this.reviewService.clearReviews();
  }

}
