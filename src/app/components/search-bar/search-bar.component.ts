//npm modules
import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCog, faSearch } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

//local modules
import { StoreService } from 'src/app/views/store/store.service';
import { UIService } from '../menu/uiService';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit, OnDestroy {

  changedAdvanced: boolean;
  readonly faCog: IconDefinition = faCog;
  readonly faSearch: IconDefinition = faSearch;
  @Input() readonly mobile: boolean;
  searchForm: FormGroup;
  @ViewChild('mainSearchBar') searchBarEl: ElementRef;
  @Input() readonly searchSettings: boolean;
  private storeSub: Subscription;

  constructor(
    private router: Router,
    private storeService: StoreService,
    private uiService: UIService) { }

  ngOnInit(): void {

    this.initForm();

    this.storeSub = this.storeService.status
    .subscribe((status) => {

      if (status.search && status.search.searchstring !== undefined) {

        this.changedAdvanced = StoreService.checkChangedAdvanced(status.search);

        this.searchForm.patchValue({
          search: status.search.searchstring
        });
      }
    });
  }

  ngOnDestroy (): void {

    this.storeSub.unsubscribe();
  }

  private initForm (): void {

    this.searchForm = new FormGroup({
      search: new FormControl("", Validators.required)
    });
  }

  changeSearch (): void {

    const searchstring = this.searchForm.value.search;

    this.storeService.setSearch({
      searchstring
    }, true);
  }

  searchItems (): void {

    if (this.searchForm.invalid) return;

    const qParams = this.storeService.createSearchUrl();

    this.searchBarEl.nativeElement.blur();

    this.router.navigate(['/search'], {
      queryParams: qParams
    });
  }

  toggleSearchOptions (e: Event): void {

    e.stopPropagation();

    this.uiService.setMenu(false);
    this.uiService.setHamburgerMenu(false);
    this.uiService.setReviewContextMenu(null);

    this.uiService.setSearchSettings(!this.searchSettings);
  }

}
