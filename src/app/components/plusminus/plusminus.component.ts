//npm modules
import { Component, Input } from '@angular/core';
import { faMinus, IconDefinition, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

//local modules
import { Product } from 'src/app/views/store/product.model';
import { CartItem } from 'src/app/shared/cartitem.interface';
import { StoreService } from 'src/app/views/store/store.service';

@Component({
  selector: 'app-plusminus',
  templateUrl: './plusminus.component.html',
  styleUrls: ['./plusminus.component.css']
})
export class PlusminusComponent {

  readonly faMinus: IconDefinition = faMinus;
  readonly faPlus: IconDefinition = faPlus;
  readonly faTrash: IconDefinition = faTrash;
  @Input() item: {count: number; _id: Product};
  @Input() noDelete: boolean;
  @Input() noFloat: boolean;

  constructor(private storeService: StoreService) { }

  changeCount (item: CartItem, countToAdd): void {

    const castedItem = <Product>item._id;

    if (item.count + countToAdd > castedItem.stock ||
      item.count + countToAdd < 1) return;

    this.storeService.changeCount({_id: castedItem._id, count: countToAdd});
  }

  deleteItem (id: string): void {
    this.storeService.deleteItem(id);
  }

}
