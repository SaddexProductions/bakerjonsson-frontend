//npm modules
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCartArrowDown, faTrash, faStoreSlash } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

//local modules
import { AuthService } from 'src/app/views/auth/auth.service';
import { ExtendedProduct } from 'src/app/views/store/front/product-categories.interface';
import { StoreService } from 'src/app/views/store/store.service';
import { LoginPromptActions } from '../popup-login/actions.enum';
import { UIService } from '../menu/uiService';

@Component({
  selector: 'app-cartbutton',
  templateUrl: './cartbutton.component.html',
  styleUrls: ['./cartbutton.component.css']
})
export class CartbuttonComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  readonly faCartArrowDown: IconDefinition = faCartArrowDown;
  readonly faStoreSlash: IconDefinition = faStoreSlash;
  readonly faTrash: IconDefinition = faTrash;
  isAuth: boolean;
  @Input() readonly pink: boolean;
  @Input() readonly product: ExtendedProduct;

  constructor(private authService: AuthService,
    private storeService: StoreService,
    private uiService: UIService) { }

  ngOnInit(): void {

    this.authSub = this.authService.status
    .subscribe(({isAuth}) => {
      this.isAuth = isAuth;
    });
  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();
  }

  addToCart(item: ExtendedProduct, e: Event): void {

    e.stopPropagation();
    e.preventDefault();

    if (!this.isAuth) {

      this.uiService.setLoginPrompt({
        enabled: true,
        action: LoginPromptActions.ADD_TO_CART,
        value: {
          ...item
        },
        reload: this.product.reviews.length > 0 ?
        this.product._id : null
      });

      return;
    }

    this.storeService.addToCart(item);
  }

  deleteItem(id: string, e: Event): void {

    e.stopPropagation();
    e.preventDefault();

    this.storeService.deleteItem(id);
  }

}
