//npm modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

//local modules
import { AboutComponent } from './views/about/about.component';
import { ContactComponent } from './views/contact/contact.component';
import { AuthGuard } from './views/auth/auth.guard';


const routes: Routes = [
  {path: 'auth', loadChildren: () => import ('./views/auth/auth.module').then(m => m.AuthModule)},
  {path: 'reviews', loadChildren: () => import ('./views/reviews/reviews.module').then(m => m.ReviewsModule)},
  {path: 'orders', loadChildren: () => import ('./views/orders/orders.module').then(m => m.OrdersModule), canActivate: [AuthGuard]},
  {path: 'messages', loadChildren: () => import ('./views/messages/messages.module').then(m => m.MessagesModule), canActivate: [AuthGuard]},
  {path: 'about', component: AboutComponent},
  {path: 'settings' , loadChildren: () => import ('./views/settings/settings.module').then(m => m.SettingsModule), canActivate: [AuthGuard]},
  {path: 'contact', component: ContactComponent},
  {path: '', loadChildren: () => import ('./views/store/store.module').then(m => m.StoreModule)},
  {
		path: "**",
		redirectTo: ""
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

