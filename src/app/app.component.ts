//npm modules
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';

//local modules
import { AuthService } from './views/auth/auth.service';
import { UIService } from './components/menu/uiService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  first: boolean;
  loading: boolean;
  mobileSearch: boolean;
  private uiSub: Subscription;
  
  constructor (
    private authService: AuthService, 
    private uiService: UIService,
    @Inject(DOCUMENT) private document: Document){}

  closeMenus (): void {

    this.uiService.resetAll();
  }

  ngOnInit (): void {

    this.authSub = this.authService.status.subscribe(status => {

      this.first = status.first;
      this.loading = status.loading;
    });

    this.uiSub = this.uiService.status
    .subscribe((status) => {

      this.mobileSearch = status.mobileSearch;

      if (status.loginPrompt && status.loginPrompt.enabled || 
        status.report || status.searchSettings) {
        this.document.body.classList.add('locked');
      } else {
        this.document.body.classList.remove('locked');
      }
    });

    this.authService.autoLogin();
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.uiSub.unsubscribe();
  }

}
