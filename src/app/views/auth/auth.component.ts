//npm modules
import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

//local modules
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy, AfterViewChecked {

  authSub: Subscription;
  private checked: boolean;
  emailChangePage: boolean;
  errorMessage: string = "";
  infoMessage: string = "";
  loading: boolean;

  constructor(
    private authService: AuthService, 
    private router: Router) { }

  ngOnInit(): void {

    this.authSub = this.authService.status.subscribe(status => {

      this.errorMessage = status.error;

      if (status.loading) this.checked = false;

      this.infoMessage = status.info;
      this.loading = status.loading;

      this.emailChangePage = this.router.url.indexOf("confirmemailupdate") !== -1;

      if (status.isAuth && !this.emailChangePage && !status.leaveConfirmNewEmail) {

        this.router.navigate(['/']);
      }
      
    });
  }

  ngAfterViewChecked(): void {
    if (this.errorMessage && !this.checked && !this.loading) {
      window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
      this.checked = true;
    }
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.authService.resetSignup();
    this.authService.resetTwoFactor();

    this.authService.setLeaveConfirmNewEmail(true);
  }

}
