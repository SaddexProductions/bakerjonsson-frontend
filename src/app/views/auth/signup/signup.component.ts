//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

//local modules
import { passwordRegex, nameRegex, phoneRegex } from 'src/app/shared/constants';
import { AuthService } from '../auth.service';
import { splitDigits } from 'src/app/shared/splitDigits';
import { equalPasswordsValidator } from 'src/app/shared/validators/equalPasswords';
import { postalCodeValidator } from 'src/app/shared/validators/postalCodeValidator';
import { SignupDetails } from '../auth.interface';
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  countrySelected: Country;
  info: string = "";
  phoneCountrySelected: Country;
  readTOS: boolean = false;
  signupForm: FormGroup;
  private timeOut: ReturnType<typeof setTimeout>;

  constructor(private authService: AuthService,
    private router: Router,
    private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle("Sign up - Bagare Jönsson");

    this.authService.getAndSaveCountry();
    this.authService.resetTwoFactor();
  
    this.authSub = this.authService.status
    .subscribe((status) => {
      
      if (status.info) {

        this.info = status.info;

        this.timeOut = setTimeout(() => {

          this.router.navigate(['/auth']);
        }, 3500);
      }

      if (!this.signupForm) this.initForm(status.signup);

      if (status.presetCountry) {

        this.countrySelected = {
          ...status.presetCountry
        }
  
        this.phoneCountrySelected = {
          ...this.countrySelected
        }
  
        this.signupForm.patchValue({
          ...this.signupForm.value,
          countryCode: this.countrySelected.code.toLowerCase(),
          phoneCountryCode: this.phoneCountrySelected.code.toLowerCase()
        });
      }
    });
  }

  setCheckBox (value: boolean): void {
    this.signupForm.patchValue({
      ...this.signupForm.value,
      twoFactorAuth: value
    });
  }

  setCountry (e: Country, toChange: string): void {

    if (toChange === "countryCode") {
      this.countrySelected = e;
    }  else {
      this.phoneCountrySelected = e;
    }

    this.signupForm.patchValue({
      ...this.signupForm.value,
      [toChange]: e.code.toLowerCase()
    });
    
  }

  formatNumber (event: Event): void {

    const value = (event.target as HTMLInputElement).value;

    this.signupForm.patchValue({
      ...this.signupForm.value,
      phone: splitDigits(value, {value: this.signupForm.value.phone, offset: 1})
    });
  }

  signup (): void {

    if (this.signupForm.invalid || !this.readTOS) return;

    this.authService.signup(this.signupForm.value);
  }


  ngOnDestroy (): void {

    this.authSub.unsubscribe();

    clearTimeout(this.timeOut);

    this.authService.resetInfo();
  }

  private initForm (details: SignupDetails): void {

    this.signupForm = new FormGroup({
      email: new FormControl(details.email, [
        Validators.required, 
        Validators.email, 
        Validators.maxLength(200)
      ], this.authService.emailAsyncValidator),
      password: new FormControl(details.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern(passwordRegex)
      ]),
      repeatPassword: new FormControl(details.repeatPassword),
      firstName: new FormControl(details.firstName, [
        Validators.required, 
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern(nameRegex)
      ]),
      lastName: new FormControl(details.lastName, [
        Validators.required, 
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern(nameRegex)
      ]),
      street: new FormControl(details.street, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(200)
      ]),
      additional: new FormControl(details.additional, [
        Validators.maxLength(50)
      ]),
      zip: new FormControl(details.zip, [
        Validators.required,
        Validators.maxLength(10),
      ]),
      city: new FormControl(details.city, [
        Validators.required,
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      province: new FormControl(details.province, [
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      phone: new FormControl(details.phone, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(14),
        Validators.pattern(phoneRegex)
      ]),
      countryCode: new FormControl(details.countryCode, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(3)
      ]),
      phoneCountryCode: new FormControl(details.phoneCountryCode, [
        Validators.minLength(2),
        Validators.maxLength(3)
      ]),
      twoFactorAuth: new FormControl(details.twoFactorAuth)
    }, [equalPasswordsValidator, postalCodeValidator], [this.authService.phoneAsyncValidator]);
  }

}
