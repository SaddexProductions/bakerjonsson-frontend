//npm modules
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

//local modules
import { AuthService } from './auth.service';
import { getToken } from 'src/app/shared/getToken';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(_: ActivatedRouteSnapshot, 
        _2: RouterStateSnapshot):
         boolean | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | UrlTree {
        return this.authService.status.pipe(
            take(1),
            map(status => {
            const isAuth = status.isAuth;

            if (isAuth || getToken() && status.first && status.loading) return true;

            return this.router.createUrlTree(['/auth']);
        }))
    }
}