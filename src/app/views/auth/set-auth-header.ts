export const setAuthHeader = (token: string) => ({
    "Authorization": `Bearer ${token}`
});