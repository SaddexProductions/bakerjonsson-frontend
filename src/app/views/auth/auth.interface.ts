//local modules
import { User } from './user.model';
import { Country } from 'src/app/shared/get-country.interface';

export interface AuthStatus {
    authToken: string;
    error: string;
    first: boolean;
    info: string;
    isAuth: boolean;
    leaveConfirmNewEmail: boolean;
    user: User;
    presetCountry: Country;
    signup: SignupDetails;
    loading: boolean;
    twoFactorToken: string;
}

export interface CountryStatus {

    svgs: {
        [key: string]: string;
    };
    countries: {
        name: string;
        code: string;
        "dial_code": string;
    }[];
}

export interface Login2FAEnabled {
    twoFactor: boolean;
    twoFactorToken: string;
}

export interface LoginSuccess {
    user: User;
    token: string;
    deviceToken?: string;
}

export interface SignupDetails {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    repeatPassword: string;
    countryCode: string;
    phoneCountryCode: string;
    twoFactorAuth: boolean;
    phone: string;
    street: string;
    zip: string;
    city: string;
    additional: string;
    province: string;
}

export interface ValidateDTO {
    token: string; 
    rejectChange?: boolean;
}