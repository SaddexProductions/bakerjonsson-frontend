
//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

//local modules
import { equalPasswordsValidator } from '../../../shared/validators/equalPasswords';
import { AuthService } from '../auth.service';
import { passwordRegex } from 'src/app/shared/constants';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private routerSub: Subscription;
  private checkResetTokenSub: Subscription;
  private first: boolean = true;
  loading: boolean = false;
  rejectedChange: boolean = false;
  resetForm: FormGroup;
  private token: string;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit(): void {
    this.initForm();


    this.titleService.setTitle("Reset password - Bagare Jönsson");

    this.routerSub = this.route.queryParams.subscribe(param => {

      if (!this.first) return;
      
      if(param && param.token) {
        this.token = param.token;
      }
 
      let validToken = false;
 
      this.checkResetTokenSub = this.authService.checkResetToken({
        token: this.token,
        rejectChange: param.reject && param.reject === "true"
      }).subscribe(
        ({success, rejectedChange}) => {
          validToken = success;
          if(!validToken && !rejectedChange) {

            this.router.navigate(['/auth']);
          }

          if (rejectedChange) {

            this.rejectedChange = true;
            this.titleService.setTitle("Password reset rejected - Bagare Jönsson");

            setTimeout(() => {

              this.router.navigate(['/auth']);
              this.rejectedChange = false;
            }, 4000);
          }

          if (!this.resetForm) this.initForm();
        }
      );
    });

    this.authSub = this.authService.status.subscribe(({info}) => {

      if (info) {
        setTimeout(() => {
          this.router.navigate(['/auth']);
        }, 3500);
      }
    });
  }

  resetPassword (): void {
    this.authService.resetPassword({
      token: this.token,
      password: this.resetForm.value.password
    });
  }

  private initForm (): void {

    this.resetForm = new FormGroup({
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern(passwordRegex)]),
      repeatPassword: new FormControl("")
    }, equalPasswordsValidator);
  }

  ngOnDestroy (): void {

    this.routerSub.unsubscribe();
    this.authSub.unsubscribe();
    this.checkResetTokenSub.unsubscribe();
  }

}
