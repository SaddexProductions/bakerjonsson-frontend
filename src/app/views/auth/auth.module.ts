//npm modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

//local modules
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { RequestpasswordresetComponent } from './requestpasswordreset/requestpasswordreset.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { SharedModule } from 'src/app/components/shared.module';
import { ConfirmemailupdateComponent } from './confirmemailupdate/confirmemailupdate.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {path: '', component: AuthComponent, children: [
                {path: '', component: LoginComponent},
                {path: 'signup', component: SignupComponent},
                {path: 'requestpasswordreset', component: RequestpasswordresetComponent},
                {path: 'resetpassword', component: ResetpasswordComponent},
                {path: 'confirmemailupdate', component: ConfirmemailupdateComponent}
            ]}
        ]),
        SharedModule,
        FontAwesomeModule
    ],
    declarations: [
        AuthComponent,
        LoginComponent, 
        ResetpasswordComponent, 
        RequestpasswordresetComponent,
        SignupComponent,
        ConfirmemailupdateComponent
    ]
})
export class AuthModule {}