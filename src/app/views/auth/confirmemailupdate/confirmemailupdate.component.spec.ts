import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmemailupdateComponent } from './confirmemailupdate.component';

describe('ConfirmemailupdateComponent', () => {
  let component: ConfirmemailupdateComponent;
  let fixture: ComponentFixture<ConfirmemailupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmemailupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmemailupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
