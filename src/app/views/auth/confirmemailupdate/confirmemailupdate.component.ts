//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

//local modules
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-confirmemailupdate',
  templateUrl: './confirmemailupdate.component.html',
  styleUrls: ['./confirmemailupdate.component.css']
})
export class ConfirmemailupdateComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  error: string;
  info: string;
  private loading: boolean;
  private timeOut: ReturnType<typeof setTimeout>;

  constructor(private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit (): void {

    const token = this.route.snapshot.queryParams.token;
    const rejectChange = this.route.snapshot.queryParams.reject === "true";

    this.authSub = this.authService.status
    .subscribe((status) => {

      this.error = status.error;
      this.info = status.info;
      this.loading = status.loading;

      if (!this.info && !this.error && !this.loading) {

        setTimeout(() => this.authService.confirmEmailUpdate({token, rejectChange: token && !rejectChange ? false : true}), 10);
      }

      if (this.info) {

        this.timeOut = setTimeout(() => this.router.navigate(['/']), 3000);
      }
    });
  }

  ngOnDestroy (): void {

    if (this.timeOut) clearTimeout(this.timeOut);

    this.authSub.unsubscribe();

    this.authService.resetInfo();
    this.authService.resetError();

    this.authService.setLeaveConfirmNewEmail(true);
  }

}
