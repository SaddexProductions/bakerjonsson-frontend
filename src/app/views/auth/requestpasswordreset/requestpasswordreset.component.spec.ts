import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestpasswordresetComponent } from './requestpasswordreset.component';

describe('RequestpasswordresetComponent', () => {
  let component: RequestpasswordresetComponent;
  let fixture: ComponentFixture<RequestpasswordresetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestpasswordresetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestpasswordresetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
