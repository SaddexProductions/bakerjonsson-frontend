//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

//local modules
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-requestpasswordreset',
  templateUrl: './requestpasswordreset.component.html',
  styleUrls: ['./requestpasswordreset.component.css']
})
export class RequestpasswordresetComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  info: string;
  reqResetPasswordForm: FormGroup;

  constructor(private authService: AuthService, private router: Router,
    private titleService: Title) { }

  ngOnInit (): void {

    this.initForm();

    this.titleService.setTitle("Request password reset - Bagare Jönsson");

    this.authSub = this.authService.status.subscribe(({info}) => {
      
      if (info) {
        setTimeout(() => {
          this.authService.resetInfo();
          this.router.navigate(['/auth']);
        }, 3500);
      }
    });
  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();
    this.authService.resetError();
    this.authService.resetInfo();
  }

  reqPasswordReset (): void {

    this.authService.requestPasswordReset(this.reqResetPasswordForm.value);
  }
  
  private initForm () {

    this.reqResetPasswordForm = new FormGroup({
      "email": new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(200)])
    });
  }

}
