//local modules
import { CartItem } from 'src/app/shared/cartitem.interface';
import { StreetAddress } from 'src/app/shared/address.interface';

export class User {

    constructor (
        public email: string, 
        public _id: string, 
        public firstName: string,
        public lastName: string,
        public countryCode: string,
        public phoneCountryCode: string,
        public phonenumber: string,
        public shoppingCart: CartItem[],
        public twoFactorAuth: boolean,
        public address?: StreetAddress,
        ) {

    }
}