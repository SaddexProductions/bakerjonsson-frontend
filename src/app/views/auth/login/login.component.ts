//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { faExclamationTriangle, IconDefinition, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

//local modules
import { AuthService } from '../auth.service';
import { splitDigits } from '../../../shared/splitDigits';
import { twoFactorHelper } from 'src/app/shared/2fa.obj';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  error: string;
  helperEnabled: boolean = false;
  loading: boolean;
  readonly faExclamationTriangle: IconDefinition = faExclamationTriangle;
  readonly faQuestionCircle: IconDefinition = faQuestionCircle;
  loginForm: FormGroup;
  sentSms: boolean = false;
  twoFactor: boolean;
  readonly twoFactorHelper = twoFactorHelper;  
  twoFactorForm: FormGroup;

  constructor(private authService: AuthService, private titleService: Title) { }

  ngOnInit(): void {

    this.titleService.setTitle("Login - Bagare Jönsson");

    this.authService.setLeaveConfirmNewEmail(false);

    this.initLoginForm();
    this.authSub = this.authService.status.subscribe(status => {

      if (status.twoFactorToken) {
        this.twoFactor = true;
        this.initTwoFactorForm();
      }

      if (status.info) {
        this.sentSms = true;
        setTimeout(() => this.authService.resetInfo(), 3500);
      }
    });

  }

  login (): void {
    if (this.loginForm.invalid) return;

    this.authService.login(this.loginForm.value);
  }

  sendSms (): void {

    if (this.sentSms) return;

    this.authService.sendSms();
  }

  twoFactorAuth (): void {

    if (this.twoFactorForm.invalid) return;

    this.authService.twoFactorAuth(this.twoFactorForm.value.authyToken, 
      this.twoFactorForm.value.saveDevice);
  }

  toggleCheckbox (value: boolean) {

    this.twoFactorForm.patchValue({
      ...this.twoFactorForm.value,
      saveDevice: value
    })
  }

  formatDigits (e: Event) {

    this.twoFactorForm.patchValue({
      ...this.twoFactorForm.value,
      authyToken: splitDigits((e.target as HTMLInputElement).value, 
      {value: this.twoFactorForm.value.authyToken, offset: 2, max: 9})
    });
  }

  private initLoginForm () {

    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(25)])
    });
  }

  private initTwoFactorForm () {

    this.titleService.setTitle("Two-Factor Authentication - Bagare Jönsson")

    this.twoFactorForm = new FormGroup({
      authyToken: new FormControl(null, [Validators.required, Validators.pattern(/^[\d\s]+$/)]),
      saveDevice: new FormControl(false)
    });
  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();
    this.authService.resetError();
    this.authService.resetInfo();
  }

}
