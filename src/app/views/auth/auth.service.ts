//npm modules
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators'; 
import { Router } from '@angular/router';
import { AsyncValidatorFn, FormControl, ValidationErrors, FormGroup } from '@angular/forms';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

//local modules
import { AuthStatus, Login2FAEnabled, LoginSuccess, SignupDetails, ValidateDTO, CountryStatus } from './auth.interface';
import { baseUrl } from 'src/app/shared/constants';
import handleError from 'src/app/shared/handleError';
import { SuccessBool } from 'src/app/shared/success.interface';
import { getToken } from 'src/app/shared/getToken';
import { User } from './user.model';
import { CartItem } from 'src/app/shared/cartitem.interface';
import { setAuthHeader } from './set-auth-header';
import { Product } from '../store/product.model';
import { GetCountry, Country } from 'src/app/shared/get-country.interface';
import { SaveSettingsInterface } from '../settings/settings.interface';

@Injectable({providedIn: "root"})
export class AuthService {

    private readonly signupBase = {
        firstName: "",
        lastName: "",
        phone: "",
        twoFactorAuth: false,
        countryCode: "DE",
        phoneCountryCode: "DE",
        email: "",
        password: "",
        repeatPassword: "",
        presetCountry: null,
        street: "",
        city: "",
        zip: "",
        province: "",
        additional: ""
    }

    readonly status = new BehaviorSubject<AuthStatus>({
        authToken: "",
        error: "",
        first: true,
        isAuth: false,
        info: "",
        leaveConfirmNewEmail: false,
        loading: false,
        presetCountry: null,
        user: null,
        signup: {...this.signupBase},
        twoFactorToken: ""
    });

    //because of the size of the data, keep country data separate from the main status
    readonly countryStatus = new BehaviorSubject<CountryStatus>({
        svgs: null,
        countries: null
    });

    constructor(
        private http: HttpClient, 
        private router: Router, 
        private _sanitizer: DomSanitizer) {}

    emailAsyncValidator: AsyncValidatorFn = 
    (control: FormControl): Promise<ValidationErrors> | Observable<ValidationErrors> => {

        let headers = {};
        const token = getToken();

        if (token) headers = setAuthHeader(token);

        return this.http.post<SuccessBool>(`${baseUrl}/auth/checkemail`, {
            email: control.value
        }, { headers })
        .pipe(map((data: SuccessBool) => data.success ? null : {emailExists: true}), 
        catchError((_) => of(null)
        ))
    }

    phoneAsyncValidator: AsyncValidatorFn =
    (form: FormGroup): Promise<ValidationErrors> | Observable<ValidationErrors> => {

        let headers = {};
        
        const token = getToken();
        if (token) headers = setAuthHeader(token);

        const phoneCountryCode = form.get("phoneCountryCode");
        const phone = form.get("phone");

        return this.http.post<SuccessBool>(`${baseUrl}/auth/checkphone`, {
            phone: phone.value.replace(/ /g, ""),
            phoneCountryCode: phoneCountryCode.value.toUpperCase()
        }, {headers}).pipe(map((data: SuccessBool) => {

            const errors = phone.errors;

            if (data.success) {
                delete errors.phoneExists;
                phone.setErrors(errors);
            }  else {
                phone.setErrors({
                    ...errors,
                    phoneExists: true
                })
            }

            return null;
        }), 
        catchError((_) => of(null)))
    }

    autoLogin (): void {

        const token = getToken();

        if (!token) {

            this.status.next({
                ...this.status.value,
                first: false
            });
            return;
        }

        this.setLoading(true);

        this.http.get<User>(baseUrl + '/auth', {
            headers: setAuthHeader(token)
        }).pipe(catchError(err => of(handleError(err))))
        .subscribe(data => {

            if (typeof data !== "string") {
                this.status.next({
                    ...this.status.value,
                    user: data,
                    isAuth: true
                });
            } else {
                if(token) this.router.navigate(['/auth']);
            }

            this.status.next({
                ...this.status.value,
                first: false,
                loading: false,
                authToken: token
            });
        });
    }

    addToCart (addedItem: CartItem): void {
        
        const itemArray = [...this.status.value.user.shoppingCart, addedItem];
        
        this.setCart(itemArray);
    }

    changeItem (changedItem: CartItem): void {

        const itemArray = [...this.status.value.user.shoppingCart];

        const i = this.getItemIndex(itemArray, changedItem._id.toString()); //_id is guaranteed to be string
        itemArray[i].count = changedItem.count;

        this.setCart(itemArray);
    }

    clearCart (): void {
        this.setCart([]);
    }

    confirmEmailUpdate (validateDTO: ValidateDTO): void {

        this.setLoading(true);

        this.http.post<SuccessBool>(`${baseUrl}/auth/confirmemailupdate`, validateDTO)
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | SuccessBool) => {

            if (typeof data === "string") {

                this.setError(data);
            } else {
                
                if (data.success) {

                    this.setInfo("Email address successfully updated, redirecting...");
                } else {

                    this.setError("Email address change rejected");
                }
            }
        });
    }

    getUser (): User {

        return this.status.value.user;
    }

    login (credentials: {email: string; password: string; deviceToken?: string}): void {

        const deviceToken = localStorage.getItem("deviceToken");

        if (deviceToken) credentials.deviceToken = deviceToken;

        this.setLoading(true);

        this.http.post<Login2FAEnabled | LoginSuccess>(baseUrl + '/auth', credentials)
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe(data => {

            if (typeof data === "string") {
                this.setError(data);
                return;
            }
            else if ("twoFactor" in data) {
                this.status.next({
                    ...this.status.value,
                    loading: false,
                    twoFactorToken: data.twoFactorToken
                });

            }  else {
                this.status.next({
                    ...this.status.value,
                    loading: false,
                    isAuth: true,
                    authToken: data.token,
                    user: data.user,
                    twoFactorToken: ""
                });

                this.saveToken(data.token);
            }
            this.resetError();
        });
    }

    logout(): void {

        this.status.next({
            ...this.status.value,
            isAuth: false,
            authToken: "",
            user: null,
            leaveConfirmNewEmail: false
        });
        document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        this.router.navigate(['/']);
    }

    requestPasswordReset (credentials: {email: string}): void {

        let error = false;

        this.setLoading(true);

        this.http.post(baseUrl + '/auth/requestpasswordreset', credentials, 
        {responseType: 'text'})
        .pipe(catchError((err: HttpErrorResponse) => {

            error = true;
            return of(handleError(err));
        }))
        .subscribe(data => {
            
            if (error) {
                this.setError(data);
            }  else {

                this.resetError();
                this.setInfo(data);
            }
        });
    }

    removeFromCart (id: string): void {

        const itemArray = [...this.status.value.user.shoppingCart];
        const i = this.getItemIndex(itemArray, id);

        itemArray.splice(i, 1);

        this.setCart(itemArray);
    }

    signup (obj: SignupDetails): void {

        for (const key in obj) {

            if (!obj[key]) delete obj[key];
        }

        this.status.next({
            ...this.status.value,
            loading: true,
            signup: obj
        })

        this.http.post<SuccessBool>(`${baseUrl}/auth/signup`, obj)
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | SuccessBool) => {

            if (typeof data === "string") {
                this.setError(data);
            }  else {
                this.resetSignup();
                this.setInfo("Sign-up successful, redirecting...");
            }
        });
    }

    saveSettings (obj: SaveSettingsInterface): void {

        const token = getToken();

        if (!token) return;
        
        this.setLoading(true);

        this.http.put<User>(`${baseUrl}/auth/edituser`, {
            ...obj
        }, {headers: setAuthHeader(token)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: User | string) => {

            if (typeof data === "string") {

                this.setError(data);
            } else {

                this.status.next({
                    ...this.status.value,
                    loading: false,
                    user: data,
                    info: "Settings successfully saved, redirecting..."
                });
            }
        });
    }

    twoFactorAuth (token: string, saveDevice: boolean): void {

        this.resetInfo();
        this.setLoading(true);

        this.http.post<LoginSuccess>(baseUrl + '/auth/twofactorauth', {
            token: this.status.value.twoFactorToken,
            key: token.replace(/\s/g, ''),
            saveDevice
        }).pipe(catchError(err => of(handleError(err))))
        .subscribe((data: string | LoginSuccess) => {

            if (typeof data === "string") {
                this.setError(data);
            }  else {

                this.status.next({
                    ...this.status.value,
                    loading: false,
                    isAuth: true,
                    authToken: data.token,
                    user: data.user
                });

                this.saveToken(data.token);

                if (data.deviceToken) {
                    localStorage.setItem("deviceToken", data.deviceToken);
                }
            }
        });
    }

    checkResetToken (validateDTO: ValidateDTO): Observable<SuccessBool> {

        //when the resetpassword request gets sent, that component gets
        // destroyed and re-created, thus triggering the check again.
        // The check below, provided that the action is successful,
        //prevents that
        if (this.status.value.info) return of({success: true});

        return this.http.post<SuccessBool>(`${baseUrl}/auth/validateresettoken`, validateDTO).pipe(
        catchError(() => of({success: false})));
    }

    async getCountryById (code: string): Promise<Country> {

        await this.initCountries();

        return {
            icon: this._sanitizer.bypassSecurityTrustHtml(this.countryStatus.value.svgs[code.toLowerCase()]),
            ...this.countryStatus.value.countries.find(c => c.code === code.toLowerCase())
        }
    }

    private async initCountries (): Promise<void> {

        return new Promise(async (res, _2) => {

            if (this.countryStatus.value.countries && this.countryStatus.value.svgs) return res();

            const countryArray = await (await import ('../../shared/countries.json')).default

            this.http.get<Object>(`${baseUrl}/countries/allcountries`)
            .pipe(catchError((_) => of(null)))
            .subscribe((data: {[key: string]: string}) => {

                this.countryStatus.next({
                    countries: countryArray,
                    svgs: data
                });

                return res();
            });
        });
    }

    codeToSafeHTML (code: string): SafeHtml {

        return this._sanitizer
            .bypassSecurityTrustHtml(this.countryStatus.value.svgs[code.toLowerCase()]);
    }

    async getAndSaveCountry (): Promise<void> {

        await this.initCountries();

        this.http.get<GetCountry>(`${baseUrl}/countries`)
        .pipe(catchError((() => of({code: "de"}))))
        .subscribe((country) => {

            const iconHTML: SafeHtml =
            this.codeToSafeHTML(country.code);

            this.status.next({
                ...this.status.value,
                presetCountry: {
                    ...this.countryStatus.value.countries.find(c => c.code === country.code.toLowerCase()),
                    icon: iconHTML
                }
            });
        });
    }

    resetPassword (resetPasswordObj: {token: string; password: string}): void {

        this.setLoading(true);

        this.http.post<SuccessBool>(`${baseUrl}/auth/resetpassword`,
        resetPasswordObj).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: SuccessBool | string) => {

            if (typeof data === "string") {
                this.setError(data)
            }  else {
                this.resetError();
                this.setInfo("Password successfully updated. Redirecting...");
            }
        });
    }

    resetSignup (): void {

        this.status.next({
            ...this.status.value,
            signup: {... this.signupBase}
        });
    }

    getUserWithAddress (): void {

        const token = getToken();
        if (!token) return;

        this.setLoading(true);

        this.http.get<User>(`${baseUrl}/auth/getuserwithaddress`, {headers: setAuthHeader(token)})
        .pipe(catchError(err => of(handleError(err))))
        .subscribe((data: User | string) => {

            if (typeof data === "string") {

                this.setError(data);
            } else {

                this.status.next({
                    ...this.status.value,
                    loading: false,
                    user: data
                });
            }
        });
    }

    sendSms (): void {

        this.resetError();
        this.setLoading(true);

        this.http.post<SuccessBool>(baseUrl + '/auth/sendsms', {
            token: this.status.value.twoFactorToken
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: SuccessBool | string) => {

            if (typeof data === "string") {
                this.setError(data);
            }   else {

                this.setInfo("Sms successfully sent!");
            }
        });
    }

    private setCart (itemArray: CartItem[]): void {
        this.status.next({
            ...this.status.value,
            user: {
                ...this.status.value.user,
                shoppingCart: itemArray
            }
        });
    }

    private getItemIndex (itemArray: CartItem[], id: string): number {
        return itemArray.findIndex(item => {

            const castedItem = <Product>item._id;

            return castedItem._id === id;
        });
    }

    private setError (error: string): void {
        this.status.next({
            ...this.status.value,
            error: error,
            loading: false
        });
    }

    private setInfo (info: string): void {
        this.status.next({
            ...this.status.value,
            info,
            loading: false
        });
    }

    private setLoading (value: boolean): void {
        this.status.next({
            ...this.status.value,
            loading: value
        });
    }

    private saveToken (token: string): void {
        let d = new Date();
        d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000)); //cookie stored for 30 days
        const expires = "expires=" + d.toUTCString();
        document.cookie = "token =" + token +
            ";" + expires + ";path=/; Secure; SameSite=Strict;";
    }

    setLeaveConfirmNewEmail (value: boolean): void {

        this.status.next({
            ...this.status.value,
            leaveConfirmNewEmail: value
        });
    }

    resetError (): void {

        this.status.next({
            ...this.status.value,
            error: ""
        });
    }

    resetInfo (): void {

        this.status.next({
            ...this.status.value,
            info: ""
        });
    }

    resetTwoFactor (): void {

        this.status.next({
            ...this.status.value,
            twoFactorToken: ""
        });
    }
}