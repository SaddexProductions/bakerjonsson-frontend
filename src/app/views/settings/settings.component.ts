//npm modules
import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

//local modules
import { nameRegex, passwordRegex, phoneRegex } from 'src/app/shared/constants';
import { SettingsDetails, BooleanValue, SaveSettingsInterface } from './settings.interface';
import { AuthService } from '../auth/auth.service';
import { equalPasswordsValidator } from 'src/app/shared/validators/equalPasswords';
import { splitDigits } from 'src/app/shared/splitDigits';
import { HelperInterface } from 'src/app/shared/helper.interface';
import { postalCodeValidator } from 'src/app/shared/validators/postalCodeValidator';
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, OnDestroy, AfterViewChecked {

  private authSub: Subscription;
  countrySelected: Country;
  details: SettingsDetails;
  error: string;
  readonly faQuestionCircle: IconDefinition = faQuestionCircle;
  info: string;
  help: boolean;
  readonly helpSteps: HelperInterface = {
    steps: [
      {
        desc: "Change the email address and save settings"
      },
      {
        desc: "Open the email inbox associated with the previous email address"
      },
      {
        desc: "Open the confirmation email sent from noreply@saddexproductions.com"
      },
      {
        desc: "Click on the button to confirm for the change to take effect"
      }
    ]
  };
  loading: boolean;
  notSent: boolean;
  phoneCountrySelected: Country;
  settingsForm: FormGroup;
  private timeOut: ReturnType<typeof setTimeout>;

  constructor(
    private authService: AuthService,
    private router: Router,
    private titleService: Title
  ) { }

  ngOnInit(): void {

    this.authService.getUserWithAddress();

    this.authSub = this.authService.status
    .subscribe(async (status) => {

      this.loading = status.loading;
      this.error = status.error;
      this.info = status.info;

      if (this.info) {

        const instance = this;

        this.timeOut = setTimeout(() => {

          instance.router.navigate(['/']);
        }, 3500);
      }

      if (!status.loading && status.user) {

        if (!status.user.address)
          this.router.navigate(['/']);

        const addressProps = {};

        Object.keys(status.user.address).map(key => {
          addressProps[key] = this.createValue(status.user.address[key])
        });

        this.countrySelected = await this.authService
        .getCountryById(status.user.countryCode);
        this.phoneCountrySelected = await this.authService
        .getCountryById(status.user.phoneCountryCode);

        this.details = {
          email: this.createValue(status.user.email),
          firstName: this.createValue(status.user.firstName),
          lastName: this.createValue(status.user.lastName),
          countryCode: this.createValue(status.user.countryCode),
          phoneCountryCode: this.createValue(status.user.phoneCountryCode),
          phone: this.createValue(splitDigits(status.user.phonenumber, {
            value: status.user.phonenumber,
            offset: 1
          })),
          twoFactorAuth: <BooleanValue>this.createValue(status.user.twoFactorAuth),
          ...addressProps
        };

        if (!this.settingsForm) this.initForm();
      } else {

        this.notSent = false;
      }
    });

    this.authService.getAndSaveCountry();
    this.titleService.setTitle("User settings - Bagare Jönsson")
  }

  ngAfterViewChecked(): void {
    if (this.error && !this.loading && !this.notSent) {

      this.notSent = true;

      window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    }
  }

  ngOnDestroy (): void {

    if (this.timeOut) clearTimeout(this.timeOut);

    this.authService.resetInfo();
    this.authSub.unsubscribe();
  }

  setCountry (e: Country, toChange: string): void {

    if (toChange === "countryCode") {
      this.countrySelected = e;
    }  else {
      this.phoneCountrySelected = e;
    }

    this.settingsForm.patchValue({
      ...this.settingsForm.value,
      [toChange]: e.code.toLowerCase()
    });
    
  }

  setCheckBox (value: boolean): void {
    
    this.settingsForm.patchValue({
      ...this.settingsForm.value,
      twoFactorAuth: value
    });
  }

  formatNumber (event: Event): void {

    const value = (event.target as HTMLInputElement).value;

    this.settingsForm.patchValue({
      ...this.settingsForm.value,
      phone: splitDigits(value, {value: this.settingsForm.value.phone, offset: 1})
    });
  }

  hasChangedValidator: ValidatorFn = (form: FormGroup) => {

    let changed = false;
    

    for (const key in form.value) {

      changed = this.checkIfPropChanged(key, form) || changed;
    }

    if (changed) {
      return null;
    }
    
    return {notChanged: true};
  }

  private initForm (): void {

    this.settingsForm = new FormGroup({
      oldPassword: new FormControl("", Validators.required),
      email: new FormControl(this.details.email.value, [
        Validators.required, 
        Validators.email, 
        Validators.maxLength(200)
      ], this.authService.emailAsyncValidator),
      firstName: new FormControl(this.details.firstName.value, [
        Validators.required, 
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern(nameRegex)
      ]),
      lastName: new FormControl(this.details.lastName.value, [
        Validators.required, 
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern(nameRegex)
      ]),
      password: new FormControl("", [
        Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern(passwordRegex)
      ]),
      repeatPassword: new FormControl(""),
      countryCode: new FormControl(this.details.countryCode.value, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(3)
      ]),
      phoneCountryCode: new FormControl(this.details.phoneCountryCode.value, [
        Validators.minLength(2),
        Validators.maxLength(3)
      ]),
      phone: new FormControl(this.details.phone.value, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(14),
        Validators.pattern(phoneRegex)
      ]),
      street: new FormControl(this.details.street.value, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(200)
      ]),
      additional: new FormControl(this.details.additional.value, [
        Validators.maxLength(50)
      ]),
      zip: new FormControl(this.details.zip.value, [
        Validators.required,
        Validators.maxLength(10),
      ]),
      city: new FormControl(this.details.city.value, [
        Validators.required,
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      province: new FormControl(this.details.province.value, [
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      twoFactorAuth: new FormControl(this.details.twoFactorAuth.value)
    }, [this.hasChangedValidator, equalPasswordsValidator, postalCodeValidator],
    [this.authService.phoneAsyncValidator]);
  }

  saveSettings (): void {

    if (this.settingsForm.invalid) return;

    const settingsObj: SaveSettingsInterface = {
      password: this.settingsForm.value.oldPassword
    }

    for (const key in this.settingsForm.value) {

      if (key !== "oldPassword" && key !== "password" && key !== "repeatPassword" && this.details[key].changed) {

        settingsObj[key] = this.settingsForm.value[key];
      }
    }

    if (this.settingsForm.value.password) settingsObj.newPassword = this.settingsForm.value.password;

    this.authService.saveSettings(settingsObj);
  }

  private checkIfPropChanged (key: string, form: FormGroup): boolean {

    if (this.details[key] !== undefined
      && (key === "countryCode" || key === "phoneCountryCode" ? form.value[key].toUpperCase() : form.value[key]) !== this.details[key].value 
      && key !== "oldPassword" ||
      this.details[key] === undefined && form.value[key] !== "" && key !== "oldPassword") {

          if (!this.details[key]) this.details[key] = {
            value: form.value[key],
            changed: true
          }

          this.details[key].changed = true;

          return true;
      } else if (this.details[key] !== undefined 
        && (key === "countryCode" || key === "phoneCountryCode" ? form.value[key].toUpperCase() : form.value[key]) === this.details[key].value) {

          this.details[key].changed = false;
      }

    return false;
  }

  private createValue (value: string | boolean) {

    if (typeof value === "string") {
      return {
        value: <string>value,
        changed: false
      }
    } else {

      return {
        value: <boolean>value,
        changed: false
      }
    }
  }

}
