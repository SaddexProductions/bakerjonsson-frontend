//npm modules
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

//local modules
import { SettingsComponent } from './settings.component';
import { SharedModule } from 'src/app/components/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {path: '', component: SettingsComponent}
        ]),
        SharedModule
    ],
    declarations: [
        SettingsComponent
    ]
})
export class SettingsModule {}