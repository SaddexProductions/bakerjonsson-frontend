export interface SettingsDetails {

    [key: string]: GenericValue;
    twoFactorAuth: BooleanValue;
}

export interface GenericValue {
    value: string | boolean;
    changed: boolean;
}

export interface BooleanValue {
    value: boolean;
    changed: boolean;
}

export interface SaveSettingsInterface {

    password: string;
    email?: string;
    countryCode?: string;
    newPassword?: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    twoFactorAuth?: boolean;
    street?: string;
    zip?: string;
    city?: string;
    additional?: string;
    province?: string;
}