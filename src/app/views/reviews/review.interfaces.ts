//local modules
import { Review } from 'src/app/shared/review.model';
import { SortReviewAfter } from '../store/item/reviews/sort-reviews.enum';

export interface ReviewStatus {
    error: string;
    indLoading: string;
    loading: boolean;
    productId: string;
    reviews: Review[];
    end: boolean;
    sortOptions: GetReviewFilter;
    successfullyDeleted: string;
    successfullyUpdated: string;
    userReview: Review;
}

export interface ReviewInputInterface {
    _id?: string;
    rating: number;
    title: string;
    content?: string;
}

export interface GetReviewFilter {
    sortAfter: SortReviewAfter;
    ascending: boolean;
    skip?: number;
    limit?: number;
}

export interface GetReviewsOptions {

    customLimit?: number;
    spread?: boolean;
    productId?: string;
}

export interface SetReviews {
    reviews: Review[];
    userId?: string;
    productId?: string;
}

export interface Vote {
    positive: boolean;
    review: Review;
}

export interface VoteReturnInterface {
    voteSum: number;
    positive?: boolean;
}