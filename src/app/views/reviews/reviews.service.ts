//npm modules
import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

//local modules
import { Review } from 'src/app/shared/review.model';
import { 
    ReviewStatus, 
    GetReviewFilter, 
    SetReviews, 
    Vote, 
    VoteReturnInterface, 
    ReviewInputInterface, 
    GetReviewsOptions 
} from './review.interfaces';
import { baseUrl } from 'src/app/shared/constants';
import handleError from 'src/app/shared/handleError';
import { getToken } from 'src/app/shared/getToken';
import { setAuthHeader } from 'src/app/views/auth/set-auth-header';
import { StoreService } from '../store/store.service';
import { SortReviewAfter } from '../store/item/reviews/sort-reviews.enum';
import { SuccessBool } from 'src/app/shared/success.interface';
import { UIService } from 'src/app/components/menu/uiService';
import { removeDuplicates } from 'src/app/shared/removeDuplicateObjects';
import { AuthService } from '../auth/auth.service';

@Injectable({providedIn: "root"})
export class ReviewsService {

    private readonly initState: ReviewStatus = {
        loading: false,
        indLoading: null,
        error: "",
        productId: "",
        reviews: [],
        end: false,
        sortOptions: null,
        successfullyDeleted: null,
        successfullyUpdated: null,
        userReview: null
    }

    readonly status: BehaviorSubject<ReviewStatus> = new BehaviorSubject({
        ...this.initState
    });

    constructor(
        private authService: AuthService,
        private http: HttpClient, 
        private storeService: StoreService,
        private uiService: UIService){}

    clearReviews (): void {

        this.status.next({
            ...this.initState
        });
    }

    //returns a promise in cases where otherwise two subscriptions would have
    //to be accessed at once
    getReviews (filter: GetReviewFilter =
        this.status.value.sortOptions,
        options?: GetReviewsOptions): Promise<boolean> {

        return new Promise((resolve, _) => {
            this.setLoading(true);

            if (!filter) filter = {
                sortAfter: SortReviewAfter.VOTES,
                ascending: false,
                limit: options.customLimit
            };

            if (!filter.limit) delete filter.limit;

            let params = "";

            let headers = {};
            const token = getToken();

            if (token) headers = setAuthHeader(token);
            else if (!token && !this.status.value.productId && (!options || !options.productId))
                return resolve(true);

            for (const key in filter) {
                if (params !== "") {
                    params += "&";
                }
                params += key + "=" + encodeURIComponent(filter[key]);
            }

            this.http.get<Review[]>(`${baseUrl}/reviews${this.status.value.productId || options && options.productId ?
            '/product/' + (options && options.productId ? options.productId : this.status.value.productId) : 
            ''}?${params}`, {
                headers
            })
            .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
            .subscribe((data: Review[] | string) => {

                if (typeof data === "string") {

                   this.setError(data);
                   this.uiService.setError(data);

                    return resolve(false);

                } else {

                    delete filter.skip;

                    const user = this.authService.getUser();

                    if (user) {

                        const userReview = data.find(r => r.creator._id === user._id);
                        if (userReview) this.setUserReview(userReview);
                    }

                    this.status.next({
                        ...this.status.value,
                        loading: false,
                        reviews: options && options.spread ? [...this.status.value.reviews,
                        ...data] : data,
                        end: data.length < 20,
                        sortOptions: filter
                    });

                    this.addUserReview();

                    return resolve(true);
                }
            });
        });
    }

    getSingleReview (id: string): void {

        this.setLoading(true);

        let headers = {};
        const token = getToken();

        if (token) headers = setAuthHeader(token);

        this.http.get<Review>(`${baseUrl}/reviews/${id}`, {
            headers
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Review) => {

            if (typeof data === "string") {

                this.uiService.setError(data);
                this.setError(data);
            } else {

                this.status.next({
                    ...this.status.value,
                    loading: false,
                    reviews: [data]
                });
            }
        });
    }

    deleteReview (id: string): void {

        this.setIndLoading(id);

        const token = getToken(); 
        if (!token) return;

        this.http.delete<SuccessBool>(`${baseUrl}/reviews/${id}`,
        {headers: setAuthHeader(token)}).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | SuccessBool) => {

            if (typeof data === "string") {
                this.setIndLoading(null);
                this.uiService.setError(data);
            } else {

                this.setIndLoading(null);

                if (data.success) {

                    const reviews = [...this.status.value.reviews];
                    let indexToDelete = -1;

                    reviews.map((r, i) => {

                        if (r._id === id) indexToDelete = i;
                    });

                    if (indexToDelete > -1) {
                        const productId = <string>reviews[indexToDelete].product._id;

                        reviews.splice(indexToDelete, 1);

                        this.status.next({
                            ...this.status.value,
                            reviews,
                            successfullyDeleted: id,
                            userReview: null
                        });

                        this.uiService.setInfo("Review deleted successfully");

                        this.storeService.checkIfUserHasOrderedAndReviewed(productId);
                    }
                }
            }
        });
    }

    postReview (reviewContent: ReviewInputInterface, productId: string): void {

        const { title, content, rating, _id } = reviewContent;

        const objToSend: ReviewInputInterface = {
            title,
            rating
        };

        if (content) objToSend.content = content;

        const token = getToken(); 
        if (!token) return;

        this.setIndLoading(_id);

        this.http.post<Review>(`${baseUrl}/reviews/product/${productId}`, {
            ...objToSend
        },
        {
            headers: setAuthHeader(token)
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Review) => {

            if (typeof data === "string") {

                this.setIndLoading(null);
                this.uiService.setError(data);
            } else {

                const reviews = [...this.status.value.reviews];
                reviews.unshift(data);

                this.status.next({
                    ...this.status.value,
                    reviews,
                    indLoading: null,
                    userReview: data
                });

                this.storeService.checkIfUserHasOrderedAndReviewed(productId);
            }
        });
    }

    saveReview (newContent: ReviewInputInterface): void {

        const {_id, title, content, rating} = newContent;

        const objToSend: ReviewInputInterface = {
            title,
            rating
        };

        if (content) objToSend.content = content;

        const token = getToken(); 
        if (!token) return;

        this.setIndLoading(_id);

        this.http.put<Review>(`${baseUrl}/reviews/${_id}`, {
            ...objToSend
        }, { headers: setAuthHeader(token) }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: Review | string) => {

            if (typeof data === "string") {

                this.setIndLoading(null);
                this.uiService.setError(data);
            } else {

                const reviews = [...this.status.value
                .reviews];

                this.uiService.setInfo("Review successfully updated");

                reviews[0] = data;
                this.status.next({
                    ...this.status.value,
                    indLoading: null,
                    reviews,
                    successfullyUpdated: data._id,
                    userReview: data
                });
            }
        });
    }

    private setIndLoading (value: string): void {
        
        this.status.next({
            ...this.status.value,
            indLoading: value
        });
    }

    private setLoading (value: boolean): void {

        this.status.next({
            ...this.status.value,
            loading: value
        });
    }

    private addUserReview (): void {

        if (!this.status.value.userReview) return;

        let index = -1;

        const isInArray = this.status
        .value.reviews.filter((r, i) => {

            if (r._id === this.status.value.userReview._id) index = i;

            return r._id === this.status.value.userReview._id
        })
        .length > 0;

        const reviews = [...this.status.value.reviews];

        if (isInArray && index > -1) {

            reviews.splice(index, 1);
            reviews.unshift(this.status.value.userReview);
        } else {

            reviews.unshift(this.status.value.userReview);
        }

        this.status.next({
            ...this.status.value,
            reviews
        });
    }

    private setError (value: string): void {

        this.status.next({
            ...this.status.value,
            error: value,
            loading: false
        });
    }

    resetError (): void {
        this.status.next({
            ...this.status.value,
            error: ""
        });
    }

    setReviews (reviewInput: SetReviews, spread?: boolean): void {

        if (this.status.value.successfullyDeleted) {
            this.resetSuccess();
            return;
        }

        const { reviews, userId, productId } = reviewInput;

        let index = -1;
        const userReview = reviews
        .filter((r, i) => {
            if (r.creator._id === userId) {
                index = i;
                return true;
            }
        });

        if (userReview.length > 0) {

            this.setUserReview(userReview[0]);

            reviews.splice(index, 1);
            reviews.unshift(this.status.value.userReview);
        }

        this.status.next({
            ...this.status.value,
            productId: productId || "",
            reviews: spread ? <Review[]>removeDuplicates([
                ...this.status.value.reviews,
                ...reviews
            ]) : [...reviews],
            end: reviews.length < 20
        });
    }

    vote (voteValue: Vote): void {

        const { review, positive } = voteValue;

        const token = getToken();
        const headers = setAuthHeader(token);


        if (review.hasVoted && review.hasVoted.positive === positive) {

            this.http.delete<VoteReturnInterface>(`${baseUrl}/reviews/${review._id}/vote`, {
                headers
            }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
            .subscribe((data: string | VoteReturnInterface) => this.voteHandler(data, review));

        } else {

            this.http.post<VoteReturnInterface>(`${baseUrl}/reviews/${review._id}/vote`, {
                positive
            }, {
                headers
            }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
            .subscribe((data: string | VoteReturnInterface) => this.voteHandler(data, review));
        }
    }

    resetSuccess (): void {

        this.status.next({
            ...this.status.value,
            successfullyUpdated: null,
            successfullyDeleted: null
        });
    }

    setEnd (value: boolean): void {

        this.status.next({
            ...this.status.value,
            end: value
        });
    }

    private voteHandler (data: string | VoteReturnInterface, review: Review) {

        if (typeof data === "string") {

            this.uiService.setError(data);
        } else {

            this.updateVote(data, review);
        }
    }

    private updateVote (data: VoteReturnInterface, review: Review): void {
        let reviewToUpdateIndex = -1;
                    
        const updatedReviews = [...this.status.value.reviews];
        updatedReviews.map((r, i) => {

            if (r._id === review._id) {

                reviewToUpdateIndex = i;
                return;
            }
        });

        updatedReviews[reviewToUpdateIndex] = {
            ...updatedReviews[reviewToUpdateIndex],
            voteSum: data.voteSum
        }

        if (data.positive || data.positive === false) {

            updatedReviews[reviewToUpdateIndex].hasVoted = {
                positive: data.positive
            }
        } else {
            delete updatedReviews[reviewToUpdateIndex].hasVoted;
        }

        this.status.next({
            ...this.status.value,
            reviews: [
                ...updatedReviews
            ]
        });
    }

    private setUserReview (review: Review) {

        this.status.next({
            ...this.status.value,
            userReview: review
        });
    }
}