//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

//local modules
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';
import { ReviewsService } from './reviews.service';
import { Review } from 'src/app/shared/review.model';
import { SortReviewAfter } from '../store/item/reviews/sort-reviews.enum';
import { SortOrder } from '../store/item/reviews/sort-order.enum';
import { ReviewInputInterface } from './review.interfaces';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  editId: string;
  end: boolean;
  error: string;
  indLoading: string;
  loading: boolean;
  reviews: Review[] = [];
  private reviewSub: Subscription;
  readonly sortAfterReviews: typeof SortReviewAfter = SortReviewAfter;
  sortValue: SortReviewAfter = SortReviewAfter.VOTES;
  orderValue: SortOrder = SortOrder.DESCENDING;
  user: User;

  constructor(private authService: AuthService,
    private reviewsService: ReviewsService,
    private titleService: Title) { }

  ngOnInit (): void {

    this.authSub = this.authService.status
    .subscribe(status => {

      this.user = status.user;
      this.titleService
      .setTitle(`Reviews made by ${this.user.firstName} ${this.user.lastName} - Bagare Jönsson`);
    });

    this.reviewSub = this.reviewsService.status
    .subscribe(status => {

      this.reviews = [...status.reviews.map(r => {
        return {...r, creator: {...r.creator}, product: {...r.product}}
      })];

      this.indLoading = status.indLoading;
      this.loading = status.loading;
      this.end = status.end;
      this.error = status.error;

      const notFromUser = this.reviews.filter(r => r.creator._id !== this.user._id).length > 0;
      if (notFromUser) {
        this.reviewsService.clearReviews();
      }

      if (this.reviews.length === 0 && !this.end && !this.loading) {

          this.reviewsService
        .getReviews({
          sortAfter: this.sortValue, 
          ascending: this.orderValue === SortOrder.ASCENDING, 
          skip: this.reviews.length
        },
        {spread: true});
      }
    });
  }

  loadMore (): void {

    if (this.end) return;

    this.reviewsService
    .getReviews({
      sortAfter: this.sortValue, 
      ascending: this.orderValue === SortOrder.ASCENDING, 
      skip: this.reviews.length
    },
    {spread: true});
  }

  setOrder (e: Event): void {

    const value = <SortOrder>(e.target as HTMLSelectElement).value;
    this.orderValue = value;

    this.reviewsService
    .getReviews({sortAfter: this.sortValue, ascending: this.orderValue === SortOrder.ASCENDING});
  }

  setSort (e: Event): void {

    const value = <SortReviewAfter>(e.target as HTMLSelectElement).value;
    this.sortValue = value;

    this.reviewsService
    .getReviews({sortAfter: this.sortValue, ascending: this.orderValue === SortOrder.ASCENDING});
  }

  deleteReview (id: string): void {
    this.reviewsService.deleteReview(id);
  }

  saveReview (newContent: ReviewInputInterface): void {

    this.reviewsService.saveReview(newContent);
  }


  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.reviewSub.unsubscribe();
  }

}
