//npm modules
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

//local modules
import { SharedModule } from 'src/app/components/shared.module';
import { ReviewsComponent } from './reviews.component';
import { AuthGuard } from '../auth/auth.guard';
import { SingleReviewComponent } from './single-review/single-review.component';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        RouterModule.forChild([
            {path: ":id", component: SingleReviewComponent},
            {path: "", component: ReviewsComponent, canActivate: [AuthGuard]}
        ]),
        SharedModule
    ],
    declarations: [ReviewsComponent, SingleReviewComponent]
})
export class ReviewsModule {}