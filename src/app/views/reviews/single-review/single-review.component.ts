//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';

//local modules
import { ReviewsService } from '../reviews.service';
import { Review } from 'src/app/shared/review.model';
import { objectIdRegex } from 'src/app/shared/constants';
import { AuthService } from '../../auth/auth.service';
import { User } from '../../auth/user.model';
import { Vote, ReviewInputInterface } from '../review.interfaces';

@Component({
  selector: 'app-single-review',
  templateUrl: './single-review.component.html',
  styleUrls: ['./single-review.component.css']
})
export class SingleReviewComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  edit: boolean = false;
  error: string;
  fromUser: boolean;
  indLoading: string;
  isAuth: boolean;
  loading: boolean = true;
  private hasToFetch: boolean;
  productId: string;
  review: Review;
  private reviewSub: Subscription;
  user: User;

  constructor(
    private authService: AuthService,
    private location: Location,
    private reviewsService: ReviewsService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit (): void {

    this.authSub = this.authService.status
    .subscribe(({isAuth, user}) => {

      this.user = user;
      this.isAuth = isAuth;
    });

    this.reviewSub = this.reviewsService.status
    .subscribe((status) => {

      this.fromUser = !status.productId;
      this.error = status.error;
      this.loading = status.loading;
      this.indLoading = status.indLoading;

      const id = this.route.snapshot.params.id;
      if (!objectIdRegex.test(id)) {
        this.router.navigate(['/'], {replaceUrl:true});
      }

      if (status.successfullyDeleted === id) {

        if (this.router.navigated) {
          this.location.back();
        } else {
          this.reviewsService.clearReviews();
          this.router.navigate(['/'], {replaceUrl:true});
        }
      }

      const reviews = [...status.reviews.map(r => ({...r, creator: {...r.creator}, product: {...r.product}}))];

      const selectedReview = reviews.find(r => r._id === id);
      if (!selectedReview 
        && !status.loading && !status.successfullyDeleted
        && !status.error && !this.hasToFetch) {
        this.reviewsService.getSingleReview(id);
        this.hasToFetch = true;
      } else {
        this.review = selectedReview;
      }

      if (this.review) {
        this.titleService.setTitle(`Review from ${this.review.creator.name} on ${this.review.product.title} - Bagare Jönsson`);

        if (typeof this.review.product._id === "string") {

          this.productId = this.review.product._id;
        } else {

          this.productId = this.review.product._id._id;
        }
      }

      if (status.successfullyUpdated) {

        this.edit = false;
  
        this.reviewsService.resetSuccess();
      }
    });
  }

  vote (voteValue: Vote): void {

    this.reviewsService.vote(voteValue);
  }

  deleteReview (id: string): void {
    this.reviewsService.deleteReview(id);
  }

  loadReview (): void {

    this.reviewsService.getSingleReview(this.route.snapshot.params.id);
  }

  saveReview (newContent: ReviewInputInterface): void {

    this.edit = true;

    this.reviewsService.saveReview(newContent);
  }

  ngOnDestroy (): void {

    if (this.fromUser && this.router.url.match(/^\/reviews$/) && this.hasToFetch)
      this.reviewsService.clearReviews();

    this.authSub.unsubscribe();
    this.reviewSub.unsubscribe();
  }

}
