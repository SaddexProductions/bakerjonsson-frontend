//local modules
import { Conversation } from './conversation.model';
import { Message } from './message.model';
import { Count } from '../../shared/count.interface';

export interface ContactStatus {

    convEnd: boolean;
    conversations: Conversation[];
    firstConvLoad: boolean;
    loading: boolean;
    noConvLoadingSpin: boolean;
    error: string;
    info: string;
    messageEnd: boolean;
    messageError: string;
    messages: Message[];
    messageLoading: boolean;
    timeStamp: Date;
    unreadMessageCount: number;
    unsentMessages: Message[];
}

export interface Report {
    id: string;
    reason: string;
    details: string;
    email?: string;
    sendEmail: boolean;
}

export interface ConversationFilter {

    limit: number;
    skip?: number;
}

export interface GetMessagesFilter extends ConversationFilter {

    newerThan?: string;
}

export interface GetConvOptions {

    noSpin?: boolean;
    spread?: boolean;
}

export interface CountMessages extends Count {

    nowReadMessagesFromMe?: string[];
}

export interface Reply {

    content: string;
    from: string;
    to: string;
    subject?: string;
    conversation_id: string;
}