//local modules
import { Message } from './message.model';

export class Conversation {

    constructor (
        public _id: string,
        public createdAt: string,
        public updatedAt: string,
        public participants: Participant[],
        public messages: Message[],
        public lastMessage: string,
        public lastMessageSentBy: string,
        public reply: boolean
    ){}
}

export interface Participant {
    id: string;
    name?: string;
    sendEmail?: boolean;
    lastTyping: string;
    typingNow?: boolean;
}