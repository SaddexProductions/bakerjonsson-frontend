//npm modules
import { Injectable } from '@angular/core';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';
import {v4 as uuid} from 'uuid';

//local modules
import { ContactStatus, Report, ConversationFilter, GetConvOptions, CountMessages, GetMessagesFilter, Reply } from './contact.interfaces';
import { getToken } from 'src/app/shared/getToken';
import { setAuthHeader } from '../auth/set-auth-header';
import { baseUrl } from 'src/app/shared/constants';
import handleError from 'src/app/shared/handleError';
import { MessageLite, Message } from './message.model';
import { SuccessBool } from 'src/app/shared/success.interface';
import { Conversation, Participant } from './conversation.model';
import { UIService } from 'src/app/components/menu/uiService';
import { removeDuplicates } from 'src/app/shared/removeDuplicateObjects';
import { User } from '../auth/user.model';
import { AuthService } from '../auth/auth.service';

@Injectable({providedIn: 'root'})
export class ContactService {

    private readonly initState: ContactStatus = {
        convEnd: false,
        conversations: [],
        firstConvLoad: true,
        loading: false,
        error: "",
        info: "",
        messageEnd: false,
        messageError: "",
        messageLoading: false,
        messages: [],
        noConvLoadingSpin: false,
        timeStamp: null,
        unreadMessageCount: 0,
        unsentMessages: []
    }
    private resendSub: Subscription;

    constructor (
        private authService: AuthService,
        private http: HttpClient,
        private uiService: UIService) {

            this.authService.status.subscribe(({
                user
            }) => {

                this.user = user;
            });
        }
    
    readonly status: BehaviorSubject<ContactStatus> = new BehaviorSubject({
        ...this.initState
    });
    private user: User;

    markAllAsRead (id?: string): Promise<boolean> {

        return new Promise((res, _2) => {
            const token = getToken();
            if (!token || !this.user) return res(false);

            let objToSend = {};

            if (id) objToSend = {
                id
            };

            this.status.next({
                ...this.status.value,
                loading: true,
                noConvLoadingSpin: !!id
            });

            this.http.post<SuccessBool>(`${baseUrl}/contact/conversations/markallasread`,
            objToSend, {headers: setAuthHeader(token)})
            .pipe((catchError((err: HttpErrorResponse) => of(handleError(err)))))
            .subscribe((data: SuccessBool | string) => {

                if (typeof data === "string") {

                    this.uiService.setError('Messages: ' + data);

                    this.status.next({

                        ...this.status.value,
                        loading: false,
                        noConvLoadingSpin: false
                    });

                    return res(false);
                } else {

                    const messages = cloneDeep(this.status.value.messages);

                    if (data.success) {
                        
                        messages.map((m, i, obj) => {

                            if (m.fromAdmin || m.from && m.from._id !== this.user._id) {

                                obj[i].read = true;
                            }
                        });
                    }

                    this.status.next({
                        ...this.status.value,
                        loading: false,
                        noConvLoadingSpin: false,
                        messages
                    });
                    
                    return res(data.success);
                }
            });
        });
    }

    setEmailNotice (id: string, value: boolean, user_id: string): void {

        const token = getToken();
        if (!token) return;

        this.http
        .put<SuccessBool>(`${baseUrl}/contact/conversations/${id}`, { value },
        {headers: setAuthHeader(token)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | SuccessBool) => {

            if (typeof data === "string") {

                this.uiService.setError(data);
            } else {

                let index = -1;

                const conversations = cloneDeep(this.status.value.conversations);

                conversations.map((c, i, convs) => {

                    if (c._id === id) {

                        convs[i].participants
                        .map((p, j, obj) => {

                            if (p.id === user_id) {

                                obj[j].sendEmail = value;
                            }

                        });
                    }
                });

                this.status.next({
                    ...this.status.value,
                    conversations
                });
            }
        });
    }

    chatReply (reply: Reply): void {

        const {
            conversation_id,
            subject, 
            content, 
            from, 
            to 
        } = reply;

        const token = getToken();
        if (!token) return;

        const newMessage: Message = {

            content,
            _id: "",
            messageIndLoading: true,
            localId: uuid(),
            conversation: {
                _id: conversation_id
            },
            createdAt: new Date().toISOString(),
            from: {
                _id: from
            },
            to: {
                _id: to
            }
        }

        if (subject) newMessage.subject = subject;

        let unsentMessages = cloneDeep(this.status.value.unsentMessages);
        unsentMessages.push(newMessage);

        this.status.next({
            ...this.status.value,
            unsentMessages
        });

        this.http.post<Message[]>(`${baseUrl}/contact/conversations/reply/${conversation_id}`, {
            messages: [newMessage]
        }, {
            headers: setAuthHeader(token)
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Message[]) => {

            unsentMessages = cloneDeep(this.status.value.unsentMessages);
            const index = unsentMessages.findIndex(m => m.localId === newMessage.localId);

            unsentMessages[index].messageIndLoading = false;
            
            if (typeof data === "string") {

                unsentMessages[index].messageIndError = true;

                this.status.next({
                    ...this.status.value,
                    unsentMessages
                });
            } else {

                unsentMessages.splice(index, 1);

                const messages = cloneDeep(this.status.value.messages);

                messages.push(data[0]);

                this.status.next({
                    ...this.status.value,
                    unsentMessages,
                    messages
                });
            }
        });
    }

    deleteUnsent (localId: string): void {

        const unsentMessages = cloneDeep(this.status.value.unsentMessages);
        const deleteIndex = unsentMessages.findIndex(m => m.localId === localId);

        if (deleteIndex === -1) {

            this.uiService.setError(`Error: Message not found`);
            return;
        }

        unsentMessages.splice(deleteIndex, 1);

        this.status.next({
            ...this.status.value,
            unsentMessages
        });

        this.uiService.setInfo(`Message successfully deleted`);
    }

    resendReplies (conversation_id: string): void {

        const token = getToken();

        const unsentMessages = cloneDeep(this.status.value.unsentMessages);
        if (!token || !unsentMessages.length) return;

        if (this.resendSub) {

            this.resendSub.unsubscribe();
            this.resendSub = null;
        }

        unsentMessages.map((_2, i, obj) => {

            obj[i].messageIndError = false;
            obj[i].messageIndLoading = true;
        });

        this.status.next({
            ...this.status.value,
            unsentMessages
        });

        this.resendSub = this.http.post<Message[]>(`${baseUrl}/contact/conversations/reply/${conversation_id}`,
            { messages: unsentMessages},
            { headers: setAuthHeader(token) }
        ).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Message[]) => {

            let messages = cloneDeep(this.status.value.messages);

            if (typeof data === "string") {

                unsentMessages.map((_2, i, obj) => {

                    obj[i].messageIndError = true;
                    obj[i].messageIndLoading = false;
                });
            } else {

                messages = messages.concat(data);
                unsentMessages.length = 0;
            }

            this.status.next({
                ...this.status.value,
                unsentMessages,
                messages
            });
        });
    }

    countAllUnread (id: string): void {

        const token = getToken();
        if (!token || !id) return;

        const toSend: {[key: string]: any} = {};

        if (this.status.value.messages.length) {

            const arr = this.status.value
            .messages.map(m => {

                if (m.from._id === id && !m.read) return m._id;
            }).filter(s => !!s);

            if (arr.length) toSend.unread = arr;
        }

        this.http.post<CountMessages>(`${baseUrl}/contact/countunread`, toSend, {headers: setAuthHeader(token)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: CountMessages | string) => {

            if (typeof data === "string") {

                console.log(data);
            } else {

                const obj: ContactStatus = {
                    ...cloneDeep(this.status.value),
                    unreadMessageCount: data.count
                };

                if (data.nowReadMessagesFromMe) {

                    data.nowReadMessagesFromMe
                    .map(id => {

                        const message = obj.messages.find(m => m._id === id);
                        if (message) message.read = true;
                    });
                }

                this.status.next({
                    ...this.status.value,
                    ...obj
                });
            }
        });
    }

    async getMessages (convId: string, filter: GetMessagesFilter, spread?: boolean): Promise<boolean> {

        return new Promise((res, _2) => {

            const token = getToken();
            if (!token) return res(false);

            this.setMessageLoading(true);

            this.http.get<Message[]>
            (`${baseUrl}/contact/conversations/${convId}?limit=${filter.limit}${filter.skip ? '&skip=' + filter.skip : ''}${
                filter.newerThan ? '&newerThan=' + filter.newerThan : ''
            }`, {
                headers: setAuthHeader(token)
            }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
            .subscribe((data: Message[] | string) => {

                if (typeof data === "string") {

                    this.setMessageError(data);
                    return res(false);

                } else {

                    this.status.next({
                        ...this.status.value,
                        messageError: "",
                        messageLoading: false,
                        messages: (spread ? 
                        <Message[]>removeDuplicates([...this.status.value.messages, ...data])
                        : data).sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()),
                        messageEnd: ((data.length < filter.limit) && !filter.newerThan)
                    });

                    return res(true);
                }
            });
        });
    }

    reset (): void {

        this.status.next({
            ...this.initState,
            unreadMessageCount: this.status.value.unreadMessageCount
        });
    }

    sendMessage (message: MessageLite, save?: boolean): void {

        let headers = {};
        const token = getToken();

        if (token) headers = setAuthHeader(token);

        for (const key in message) {

            if (!message[key]) delete message[key];
        }

        this.setLoading(true);

        this.http.post<Message | SuccessBool>(`${baseUrl}/contact/`, {
            ...message
        }, { headers })
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Message | SuccessBool) => {

            if (typeof data === "string") {

                this.setError(data);
            } else {

                this.setInfo("Message successfully sent, redirecting...");
            }
        });
    }

    submitReport (report: Report): void {

        let headers = {};
        
        const token = getToken();
        if (token) headers = setAuthHeader(token);

        this.setLoading(true);

        this.http.post<SuccessBool>(`${baseUrl}/contact/report`, report,
        {
            headers
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: SuccessBool | string) => {

            if (typeof data === "string") {
                this.setError(data);
            } else {

                this.setInfo("Report successfully submitted");
            }
        });
    }

    setLoading (value: boolean): void {

        this.status.next({
            ...this.status.value,
            loading: value
        });
    }

    private setMessageLoading (value: boolean): void {
        
        this.status.next({
            ...this.status.value,
            messageLoading: value
        });
    }

    setError (value: string): void {

        this.status.next({
            ...this.status.value,
            error: value,
            loading: false
        });
    }

    setMessageError (value: string): void {

        this.status.next({
            ...this.status.value,
            messageError: value,
            messageLoading: false
        });
    }

    getConversations (filter: ConversationFilter, getConvOptions?: GetConvOptions): void {

        const { skip, limit } = filter;

        const token = getToken();
        if (!token) return;

        if (!getConvOptions || !getConvOptions.noSpin) this.setLoading(true);

        this.http.get<Conversation[]>(`${baseUrl}/contact/conversations?limit=${limit}${skip ? "&skip=" + skip : ""}`,
        {headers: setAuthHeader(token)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Conversation[]) => {

            if (typeof data === "string") {

                this.setError(data);

            } else {

                this.status.next({
                    ...this.status.value,
                    loading: false,
                    conversations: getConvOptions && getConvOptions.spread ? <Conversation[]>removeDuplicates([
                        ...this.status.value.conversations,
                        ...data
                    ]) : data,
                    convEnd: data.length < filter.limit,
                    firstConvLoad: false
                });

            }
        });
    }

    checkIfChanged (): void {

        const token = getToken();
        if (!token) return;

        this.status.next({
            ...this.status.value,
            timeStamp: new Date()
        });

        if (!this.status.value.conversations.length) {

            this.getConversations({limit: 10}, {noSpin: true});
            return;
        }

        this.http.post<Conversation[]>(`${baseUrl}/contact/conversations/checkifchanged`,
        {
            conversationsToCheck: this.status.value.conversations
            .map(c => ({
                id: c._id,
                updatedAt: c.updatedAt
            }))
        }, {headers: setAuthHeader(token)})
        .pipe((catchError((err: HttpErrorResponse) => of(handleError(err)))))
        .subscribe((data: string | Conversation[]) => {

            if (typeof data === "string") {

                this.uiService.setError(data);

            } else {

                if (data.length) {

                    const clonedConversations = cloneDeep(this.status.value.conversations);
                
                    data.map(c => {

                        const existsInConversations = clonedConversations
                        .find(co => c._id === co._id);

                        if (existsInConversations) {

                            const i = clonedConversations.findIndex(cl => cl._id === c._id);
                            clonedConversations[i] = c;

                        } else {

                            clonedConversations.unshift(c);
                        }
                    });

                    clonedConversations.sort((a, b) => {
                        if(a.lastMessage > b.lastMessage) { return -1; }
                        if(a.lastMessage < b.lastMessage) { return 1; }
                        return 0;
                    });
    
                    this.status.next({
                        ...this.status.value,
                        conversations: clonedConversations
                    });
                }
            }
        });
    }

    clearMessages (): void {

        this.status.next({
            ...this.status.value,
            messages: [],
            unsentMessages: [],
            messageEnd: false
        });
    }

    setTyping (id: string): void {

        const token = getToken();
        if (!token) return;

        this.http.post<SuccessBool>(`${baseUrl}/contact/conversations/settyping/${id}`, {},
        { headers: setAuthHeader(token)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | SuccessBool) => {
            
            if (typeof data === "string") {

                console.log(data);
            }
        });
    }

    private setInfo (value: string): void {

        this.status.next({
            ...this.status.value,
            info: value,
            loading: false
        });
    }

    static getOtherParticipant (conversation: Conversation, user_id: string): Participant {

        return conversation
        .participants.find(p => p.id !== user_id);
    }

    static setTypingNow (firstDate: Date, secondDate: Date): boolean {

        const typingTimeDiff = firstDate.getTime() - secondDate.getTime();

        return Math.floor(typingTimeDiff/1000/60) < 1;
    }

    checkIfReadDiff (messagesFromComp: Message[], user_id: string): boolean {

        return messagesFromComp.filter(m => !m.read && m.from._id === user_id).length !==
        this.status.value.messages.filter(m => !m.read && m.from._id === user_id).length;
    }
}