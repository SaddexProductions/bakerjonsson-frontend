export class MessageLite {

    constructor (
        public email: string,
        public subject: string,
        public content: string
    ){}
}

export class Message {

    constructor (
        public content: string,
        public _id: string,
        public conversation?: {
            _id: string;
        },
        public subject?: string,
        public email?: string,
        public localId?: string,
        public createdAt?: string,
        public updatedAt?: string,
        public read?: boolean,
        public to?: {
            _id: string;
        },
        public from?: {
            _id: string;
        },
        public fromFullName?: string,
        public fromAdmin?: boolean,
        public toAdmin?: boolean,
        public button?: string,
        public messageIndLoading?: boolean,
        public messageIndError?: boolean
    ) {
    }
}