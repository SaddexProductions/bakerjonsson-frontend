//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

//local modules
import { AuthService } from '../auth/auth.service';
import { ContactService } from './contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private contactSub: Subscription;
  contactForm: FormGroup;
  error: string;
  info: string;
  isAuth: boolean;
  loading: boolean;
  notSent: boolean;
  private timeOut: ReturnType<typeof setTimeout>;

  constructor (
    private authService: AuthService,
    private contactService: ContactService,
    private router: Router,
    private titleService: Title) { }

  ngOnInit (): void {

    this.titleService.setTitle("Contact - Bagare Jönsson");

    this.authSub = this.authService.status
    .subscribe(({isAuth}) => {

      this.isAuth = isAuth;
    });

    this.contactSub = this.contactService
    .status.subscribe((status) => {
      
      this.error = status.error;
      this.info = status.info;
      this.loading = status.loading;

      if (this.loading) this.notSent = false;

      if (this.info) {

        if (this.timeOut) clearTimeout(this.timeOut);

        this.timeOut = setTimeout(() => {
          this.router.navigate(['/']);
  
        }, 3500);
      }

    });

    this.initForm();
  }

  checkBox (value: boolean): void {

    this.contactForm.patchValue({
      ...this.contactForm.value,
      sendEmail: value
    });
  }

  sendMessage (): void {

    if (this.contactForm.invalid) return;

    this.contactService.sendMessage({
      ...this.contactForm.value
    });
  }

  private initForm (): void {

    const pattern = /^(?!\&order\:).*$/;

    this.contactForm = new FormGroup({
      email: new FormControl("", [Validators.email, Validators.maxLength(200)]),
      subject: new FormControl("", 
        [
          Validators.required, 
          Validators.minLength(3), 
          Validators.maxLength(50),
          Validators.pattern(pattern)
        ]),
      content: new FormControl("", 
        [
          Validators.required, 
          Validators.minLength(15), 
          Validators.maxLength(300),
          Validators.pattern(pattern)
        ]),
      sendEmail: new FormControl(false)
    });
  }

  ngAfterViewChecked(): void {
    if (this.error && !this.loading && !this.notSent) {

      this.notSent = true;

      window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    }
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.contactSub.unsubscribe();

    this.contactService.reset();
    clearTimeout(this.timeOut);
  }

}
