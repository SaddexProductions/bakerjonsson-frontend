//npm modules
import { Component, OnInit, Input } from '@angular/core';

//local modules
import { ExtendedProduct } from '../front/product-categories.interface';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {

  @Input() color: string;
  @Input() product: ExtendedProduct;

  constructor() { }

  ngOnInit(): void {
  }

}
