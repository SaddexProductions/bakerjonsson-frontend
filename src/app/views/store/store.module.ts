//npm modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

//local modules
import { StoreComponent } from './store.component';
import { SearchComponent } from './search/search.component';
import { FrontComponent } from './front/front.component';
import { BrowseComponent } from './browse/browse.component';
import { SharedModule } from 'src/app/components/shared.module';
import { ListingComponent } from './listing/listing.component';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        RouterModule.forChild([
            {path: "", component: StoreComponent, children: [
                {path: "products", component: BrowseComponent},
                {path: "products/:id", loadChildren: () => import ('./item/item.module').then(m => m.ItemModule)},
                {path: "search", component: SearchComponent},
                {path: "", component: FrontComponent}
            ]},
        ]),
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        NgxSliderModule
    ],
    declarations: [StoreComponent, SearchComponent, FrontComponent, BrowseComponent, ListingComponent]
})
export class StoreModule {}