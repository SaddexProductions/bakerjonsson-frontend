//local modules
import { Product } from '../product.model';
import { ProductType } from '../product-enums';

export interface ProductCategories {
    [key: string]: ProductCategory;
}

interface ProductCategory {

    products: ExtendedProduct[];
    title: string;
    color: string;
    type: ProductType
}

export class ExtendedProduct extends Product {

    inShoppingCart?: boolean;
    count: number;
}