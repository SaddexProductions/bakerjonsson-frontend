//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { faPlusCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Title } from '@angular/platform-browser';

//local modules
import { StoreService } from '../store.service';
import { ProductCategories } from './product-categories.interface';
import { ProductType } from '../product-enums';
import { AuthService } from '../../auth/auth.service';
import { CartItem } from 'src/app/shared/cartitem.interface';
import { Image } from 'src/app/components/gallery/img.interface';
import { ReviewsService } from '../../reviews/reviews.service';

@Component({
  selector: 'app-front',
  templateUrl: './front.component.html',
  styleUrls: ['./front.component.css']
})
export class FrontComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  readonly faPlusCircle: IconDefinition = faPlusCircle;
  error: string = "";
  readonly images: Image[] = [
    {
      src: "baker1.jpg",
      alt: "Welcome to the bakery",
      desc: "Bagare Jönsson welcomes you",
      link: "/"
    },
    {
      src: "/products/semla.jpg",
      alt: "Semla",
      desc: "Now available",
      link: "/products/5f84a20ed9efba3a4cab89c1"
    },
    {
      src: "/products/pretzel.jpg",
      alt: "Pretzel",
      desc: "The classic",
      link: "/products/5f860391c4ede03b98910f0e"
    }
  ];
  loading: boolean;
  private shoppingCart: CartItem[];
  productCategories: ProductCategories = {
    bread: {
      title: "Bread",
      color: "#00aaff",
      products: [],
      type: ProductType.BREAD
    },
    sweets: {
      title: "Sweets",
      color: "#f37aaf",
      products: [],
      type: ProductType.SWEET
    },
    cookies: {
      title: "Cookies",
      color: "#d34286",
      products: [],
      type: ProductType.COOKIE
    }
  };
  private storeSub: Subscription;

  constructor(private authService: AuthService,
    private reviewService: ReviewsService,
    private storeService: StoreService,
    private titleService: Title) { }

  ngOnInit(): void {

    this.reviewService.clearReviews();

    this.storeSub = this.storeService.status
      .subscribe(status => {
        
        this.loading = status.loading;
        this.error = status.error;

        this.productCategories = {
          bread: {
            ...this.productCategories.bread,
            products: status.products.filter(p => p.type === ProductType.BREAD)
              .map(p => ({
                ...p,
                count: 1,
                inShoppingCart: StoreService.checkCart(p._id, this.shoppingCart)
              }))
          },
          sweets: {
            ...this.productCategories.sweets,
            products: status.products.filter(p => p.type === ProductType.SWEET)
              .map(p => ({
                ...p,
                count: 1,
                inShoppingCart: StoreService.checkCart(p._id, this.shoppingCart)
              }))
          },
          cookies: {
            ...this.productCategories.cookies,
            products: status.products.filter(p => p.type === ProductType.COOKIE)
              .map(p => ({
                ...p,
                count: 1,
                inShoppingCart: StoreService.checkCart(p._id, this.shoppingCart)
              }))
          }
        }
      });

    this.authSub = this.authService.status.subscribe(status => {

      if (status.user && status.user.shoppingCart) {

        this.shoppingCart = [...status.user.shoppingCart.map(item => ({...item}))];
      } else {

        if (this.shoppingCart && !status.user) this.storeService.getEqual();

        this.shoppingCart = null;
      }

      for (const key in this.productCategories) {

        this.productCategories[key] = {
          ...this.productCategories[key],
          products: this.productCategories[key].products
            .map(p => ({ ...p, inShoppingCart: StoreService.checkCart(p._id, this.shoppingCart) }))
        }
      }
      
    });

    this.storeService.getEqual();

    this.titleService.setTitle("Bagare Jönsson");
  }

  ngOnDestroy(): void {

    this.storeSub.unsubscribe();
    this.authSub.unsubscribe();

    this.storeService.resetError();
  }

}
