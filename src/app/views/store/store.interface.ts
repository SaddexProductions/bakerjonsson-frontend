//local modules
import { Product } from './product.model';
import { ProductType } from './product-enums';
import { Review } from 'src/app/shared/review.model';

export interface StoreStatus {
    loading: boolean;
    products: Product[];
    error: string;
    info: string;
    search: Search;
    end: boolean;
}

export interface Search {
    searchstring?: string;
    type?: ProductType[];
    limit?: number;
    offset?: number;
    available?: boolean;
    lowerrating?: number;
    higherrating?: number;
    lowerprice?: number;
    higherprice?: number;
    product_ids?: string[];
}

export interface CheckIfOrderedReviewed {
    hasOrdered: boolean;
    hasReviewed: Review;
}