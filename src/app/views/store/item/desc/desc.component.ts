//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

//local modules
import { StoreService } from '../../store.service';
import { Product } from '../../product.model';

@Component({
  selector: 'app-desc',
  templateUrl: './desc.component.html',
  styleUrls: ['./desc.component.css']
})
export class DescComponent implements OnInit, OnDestroy {

  product: Product;
  private storeSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private storeService: StoreService, 
    private titleService: Title) { }

  ngOnInit (): void {
    this.storeSub = this.storeService.status
    .subscribe(status => {
      
      if (status.products.length > 0) {
        this.product = status.products.find(p => p._id === this.route.snapshot.params.id);

        this.titleService.setTitle(`${this.product.title} - Bagare Jönsson`);
      }

    });
  }

  ngOnDestroy (): void {
    this.storeSub.unsubscribe();
  }

}
