export enum SortReviewAfter {
    VOTES = "VOTES",
    DATE_POSTED = "DATE_POSTED",
    SCORE = "SCORE"
}