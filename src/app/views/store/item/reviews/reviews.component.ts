//npm modules
import { Component, OnInit, OnDestroy, OnChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

//local modules
import { Review } from 'src/app/shared/review.model';
import { AuthService } from 'src/app/views/auth/auth.service';
import { User } from 'src/app/views/auth/user.model';
import { SortReviewAfter } from './sort-reviews.enum';
import { SortOrder } from './sort-order.enum';
import { Vote, ReviewInputInterface } from '../../../reviews/review.interfaces';
import { ReviewsService } from 'src/app/views/reviews/reviews.service';
import { StoreService } from '../../store.service';
import { Product } from '../../product.model';
import { isInViewport } from 'src/app/shared/isInViewport';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit, OnDestroy, OnChanges, AfterViewInit {

  private authSub: Subscription;
  end: boolean;
  editUserReview: boolean = false;
  error: string;
  indLoading: string;
  loading: boolean;
  prevent: boolean;
  newReview: boolean = false;
  reviews: Review[];
  sortValue: SortReviewAfter = SortReviewAfter.VOTES;
  orderValue: SortOrder = SortOrder.DESCENDING;
  product: Product;
  private reviewSub: Subscription;
  private routeSub: Subscription;
  @ViewChild('scrollElement') scrollElement: ElementRef;
  private scrollToBottom: boolean;
  private scrolledToBottomOnce: boolean;
  readonly sortOrder: typeof SortOrder = SortOrder;
  readonly sortReviews: typeof SortReviewAfter = SortReviewAfter;
  private storeSub: Subscription;
  user: User;

  constructor(private authService: AuthService,
    private reviewsService: ReviewsService,
    private storeService: StoreService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit (): void {

    this.reviewSub = this.reviewsService.status.subscribe((status) => {

      this.reviews = [...status.reviews.map(r => ({...r, creator: {...r.creator}, product: {...r.product}}))];
      this.loading = status.loading;
      this.error = status.error;
      this.indLoading = status.indLoading;
      this.end = status.end;

      const oldTitle = this.titleService.getTitle().split(' -')[0];

      this.titleService.setTitle(`Reviews for ${oldTitle} - Bagare Jönsson`);

      if (status.successfullyUpdated) {

        this.router.navigate([]);

        this.reviewsService.resetSuccess();
      }

    });

    this.routeSub = this.route.queryParams.subscribe((params: Params) => {

      this.scrollToBottom = params?.scroll === "true";

        
      this.editOrCreateThroughParams(params);
    });

    this.authSub = this.authService.status.subscribe((status) => {

      this.user = status.user;
    });

    this.storeSub = this.storeService.status.subscribe(({products}) => {

      this.product = products.find(p => p._id === this.route.snapshot.parent.params.id);
      this.editOrCreateThroughParams(this.route.snapshot.queryParams);
    });
  }

  ngOnChanges (): void {

    this.scrolledToBottomOnce = (this.scrollToBottom && isInViewport(this.scrollElement.nativeElement)) ||
    this.scrolledToBottomOnce;
  }

  ngAfterViewInit (): void {

    if (this.scrollToBottom && !this.scrolledToBottomOnce) 
      this.scrollElement.nativeElement.scrollIntoView();
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.reviewSub.unsubscribe();
    this.routeSub.unsubscribe();
    this.storeSub.unsubscribe();

    this.reviewsService.resetError();
  }

  deleteReview (id: string): void {

    this.reviewsService.deleteReview(id);
  }

  private editOrCreateThroughParams (params: Params): void {

      if (params.edit && this.product && this.product.hasReviewed) {

        this.editUserReview = true;
      } else if (params.new && this.product && this.product.hasOrdered && !this.product.hasReviewed) {
  
        this.newReview = true;
      } else {
        this.editUserReview = false;
        this.newReview = false;
        if (this.product) {
          this.router.navigate([], {
            queryParamsHandling: 'preserve'
          });

        }
      }
  }

  private fetchReviews (skip: number, spread?: boolean): void {

    this.reviewsService.getReviews({
      sortAfter: this.sortValue,
      ascending: this.orderValue === SortOrder.ASCENDING,
      skip
    }, {spread});
  }

  saveReview (newContent: ReviewInputInterface, newReview?: boolean) {

    if (newReview) {
      this.reviewsService.postReview(newContent, this.product._id);
    } else {
      this.reviewsService.saveReview(newContent);
    }
  }

  setOrder (e: Event): void {

    const value = <SortOrder>(e.target as HTMLSelectElement).value;
    this.orderValue = value;

    this.fetchReviews(0);
  }

  setSort (e: Event): void {

    const value = <SortReviewAfter>(e.target as HTMLSelectElement).value;
    this.sortValue = value;

    this.fetchReviews(0);
  }

  triggerLoad(): void {

    if (!this.end && !this.prevent) {

      this.prevent = true;

      this.fetchReviews(this.reviews.length, true);
      
      setTimeout(() => this.prevent = false, 200);
    }
  }

  vote (voteValue: Vote): void {

    this.reviewsService.vote(voteValue);
  }

}
