//npm modules
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

//local modules
import { ItemComponent } from './item.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { DescComponent } from './desc/desc.component';
import { SharedModule } from 'src/app/components/shared.module';
import { OrdersComponent } from './orders/orders.component';
import { AuthGuard } from '../../auth/auth.guard';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        RouterModule.forChild([
            {path: "", component: ItemComponent, children: [
                {path: "reviews", component: ReviewsComponent},
                {path: "orders", component: OrdersComponent, canActivate: [AuthGuard]},
                {path: "", component: DescComponent}
            ]}
        ]),
        SharedModule
    ],
    declarations: [ItemComponent, ReviewsComponent, DescComponent, OrdersComponent]
})
export class ItemModule {}