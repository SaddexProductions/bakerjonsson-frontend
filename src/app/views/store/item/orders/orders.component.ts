//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

//local modules
import { SortOrder } from '../reviews/sort-order.enum';
import { SortOrdersEnum } from 'src/app/views/orders/sort-orders.enum';
import { OrdersService } from 'src/app/views/orders/orders.service';
import { Order } from 'src/app/views/orders/order.model';
import { Product } from '../../product.model';
import { StoreService } from '../../store.service';
import { AuthService } from 'src/app/views/auth/auth.service';
import { User } from 'src/app/views/auth/user.model';
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  end: boolean;
  error: string;
  firstLoad: boolean = true;
  indLoading: string;
  loading: boolean;
  orderSub: Subscription;
  orders: Order[];
  orderValue: SortOrder = SortOrder.DESCENDING;
  presetCountry: Country;
  product: Product;
  readonly sortOrder: typeof SortOrder = SortOrder;
  readonly sortProductOrders: typeof SortOrdersEnum = SortOrdersEnum;
  sortValue: SortOrdersEnum = SortOrdersEnum.DATE;
  private storeSub: Subscription;
  user: User;

  constructor(
    private authService: AuthService,
    private ordersService: OrdersService,
    private route: ActivatedRoute,
    private titleService: Title,
    private storeService: StoreService) { }

  ngOnInit(): void {

    this.authService.getAndSaveCountry();

    this.authSub = this.authService.status
    .subscribe(({user, presetCountry}) => {

      this.user = {...user};
      this.presetCountry = {...presetCountry};
    });

    this.orderSub = this.ordersService.status
    .subscribe((status) => {

      this.error = status.error;
      this.loading = status.loading;
      this.indLoading = status.indLoading;
      this.orders = status.orders;
      this.end = status.end;
      
    });

    this.storeSub = this.storeService.status.subscribe((status) => {

      this.product = status.products.find(p => p._id === this.route.snapshot.parent.params.id);

      if (this.firstLoad) this.loadNew();

      this.firstLoad = false;

      this.titleService.setTitle(`Your orders of ${this.product.title} - Bagare Jönsson`);

    });

  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();
    this.orderSub.unsubscribe();
    this.storeSub.unsubscribe();
    this.ordersService.resetError();
  }

  setSort (e: Event): void {
    const value = <SortOrdersEnum>(e.target as HTMLSelectElement).value;
    this.sortValue = value;

    this.loadNew();
  }

  setOrder (e: Event): void {
    const value = <SortOrder>(e.target as HTMLSelectElement).value;
    this.orderValue = value;

    this.loadNew();
  }

  loadMore (): void {

    if (this.end) return;

    this.ordersService
    .getOrders({
      sort: SortOrdersEnum.DATE, 
      ascending: false, 
      product_id: this.product._id, 
      offset: this.orders.length
    }, true);
  }

  private loadNew () {
    this.ordersService.getOrders(
      {
        sort: this.sortValue ? this.sortValue : SortOrdersEnum.DATE, 
        ascending: this.orderValue === SortOrder.ASCENDING, 
        product_id: this.product._id
      }
    );
  }

}
