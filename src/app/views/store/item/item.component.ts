//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

//local modules
import { StoreService } from '../store.service';
import { ProductType } from '../product-enums';
import { ExtendedProduct } from '../front/product-categories.interface';
import { AuthService } from '../../auth/auth.service';
import { CartItem } from 'src/app/shared/cartitem.interface';
import { Product } from '../product.model';
import { Review } from 'src/app/shared/review.model';
import { ReviewsService } from '../../reviews/reviews.service';
import { objectIdRegex } from 'src/app/shared/constants';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  cartItem: CartItem = null;
  error: string;
  readonly faCheckCircle: IconDefinition = faCheckCircle;
  readonly faTimesCircle: IconDefinition = faTimesCircle;
  isAuth: boolean;
  private hasAddedReview: boolean = false;
  loading: boolean;
  private productId: string;
  product: ExtendedProduct;
  readonly productType: typeof ProductType = ProductType;
  reviewCount: number = 0;
  private reviewSub: Subscription;
  private routeSub: Subscription;
  private shoppingCart: CartItem[];
  private storeSub: Subscription;
  private userId: string = "";
  userReview: Review;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private reviewsService: ReviewsService,
    private storeService: StoreService,
    private titleService: Title) { }

  ngOnInit (): void {

    this.routeSub = this.route.params.subscribe((params) => {
      
      this.productId = params.id;

      if (!objectIdRegex.test(this.productId)) {
        this.router.navigate(['/'], {replaceUrl: true});

        return;
      }

      if (this.product && this.productId && this.product._id !== this.productId) {

        this.storeService.clearProducts();
        this.loadProduct();
      }

    });

    this.authSub = this.authService.status
    .subscribe((status) => {

      this.isAuth = status.isAuth;

      if (status.user && status.user.shoppingCart) {

        this.userId = status.user._id;

        this.shoppingCart = status.user.shoppingCart;

        if (this.productId) {

          const product = this.shoppingCart
          .find(item => {

            const castedItem = <Product>item._id;

            return castedItem._id === this.productId;

          });

          if (product) {
            this.cartItem = product;
          } else {
            this.cartItem = null;
          }
        }

      } else {
        this.shoppingCart = null;
      }

      if (this.product) 
        this.product.inShoppingCart = StoreService.checkCart(this.product._id, this.shoppingCart);

    });

    this.storeSub = this.storeService.status
    .subscribe(status => {

      this.loading = status.loading;

      const productIsInArray = status.products
      .find(p => p._id === this.productId);
      
      if (!productIsInArray && !this.loading && !this.error) {
        this.loadProduct();
        return;
      }
      
      if (status.products.length > 0) {

        this.product = {
          ...status.products.find(p => p._id === this.productId),
          count: 1 
        };

        this.product.inShoppingCart = StoreService.checkCart(this.product._id, this.shoppingCart)

        const reviews = <Review[]>[...this.product.reviews];

        if (this.product.hasReviewed) {

          const inArray: boolean = reviews
          .filter(r => r._id === this.product.hasReviewed._id)
          .length > 0;

          if (!inArray) reviews.unshift(this.product.hasReviewed);
        
        }
        if (!this.hasAddedReview) {
          
          this.reviewsService.setReviews({reviews: [...reviews], productId: this.product._id,
          userId: this.userId});

          this.hasAddedReview = reviews.length > 0;
        }

        if (reviews.length === 0) this.reviewsService.setEnd(true);

        this.error = "";
        this.titleService.setTitle(`${this.product.title} - Bagare Jönsson`);
      } else {

        this.titleService.setTitle(`Error - Bagare Jönsson`);
        this.error = status.error;
      }

    });

    this.reviewSub = this.reviewsService.status.subscribe(({userReview, reviews}) => {
      this.userReview = userReview;

      this.reviewCount = reviews.length;
    });
  }

  loadProduct (): void {

    this.storeService.getSingleItem(this.productId);
  }

  writeReview (): void {

    if (this.product.hasReviewed) {
      this.router.navigate([`/products/${this.product._id}/reviews`], {
        queryParams: {
          edit: true
        }
      });
    } else {
      this.router.navigate([`/products/${this.product._id}/reviews`], {
        queryParams: {
          new: true
        }
      });
    }
  }

  ngOnDestroy (): void {
    this.authSub.unsubscribe();
    this.reviewSub.unsubscribe();
    this.routeSub.unsubscribe();
    this.storeSub.unsubscribe();

    this.storeService.resetError();
  }

}
