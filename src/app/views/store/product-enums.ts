export enum ProductType {

    SWEET = "SWEET",
    BREAD = "BREAD",
    COOKIE = "COOKIE"
}

export enum InSortiment {
    COMING_SOON = "COMING_SOON",
    AVAILABLE = "AVAILABLE",
    EXPIRED = "EXPIRED"
};