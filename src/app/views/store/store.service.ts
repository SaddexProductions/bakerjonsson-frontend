//npm modules
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Params } from '@angular/router';

//local modules
import { StoreStatus, Search, CheckIfOrderedReviewed } from './store.interface';
import { baseUrl } from 'src/app/shared/constants';
import { AuthService } from '../auth/auth.service';
import { Product } from './product.model';
import handleError from 'src/app/shared/handleError';
import { CartItem } from 'src/app/shared/cartitem.interface';
import { setAuthHeader } from '../auth/set-auth-header';
import { SuccessBool } from 'src/app/shared/success.interface';
import { ExtendedProduct } from './front/product-categories.interface';
import { UIService } from 'src/app/components/menu/uiService';
import { removeDuplicates } from 'src/app/shared/removeDuplicateObjects';

@Injectable({providedIn: "root"})
export class StoreService {

    private authToken: string;

    constructor (private http: HttpClient, private authService: AuthService,
        private uiService: UIService) {

        this.authService.status.subscribe(({authToken}) => {
            this.authToken = authToken;
        });
    }

    readonly status: BehaviorSubject<StoreStatus> = new BehaviorSubject({
        error: "",
        info: "",
        loading: false,
        products: [],
        search: {},
        end: false
    });

    searchItems (search?: Search, spread?: boolean): Promise<Product[]> {

        const finalSearch = {
            ...this.status.value.search,
            ...search,
            limit: 20
        };

        return this.getItems(finalSearch, spread);
    }

    changeCount (itemToChange: CartItem): void {

        const {_id, count} = itemToChange;

        this.http.put<CartItem>(`${baseUrl}/products/cart/${_id}` , {
            count
        }, {headers: setAuthHeader(this.authToken)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: CartItem | string) => {

            if (typeof data === "string") {
                this.uiService.setError(data);
            } else {
                this.uiService.setInfo(`1 item has been updated`);
                this.authService.changeItem(data);
            }

        });
    }

    addToCart (itemToAdd: ExtendedProduct): void {

        const {_id, count} = itemToAdd;

        this.http.post<CartItem>(`${baseUrl}/products/cart/${_id}`, {
            count
        }, {headers: setAuthHeader(this.authToken)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: CartItem | string) => {

            if (typeof data === "string") {
                this.uiService.setError(data);
            } else {

                const castedItem = <Product>data._id;
                this.uiService.setInfo(`${castedItem.title} has been added to cart!`);

                this.authService.addToCart(data);
            }

        });
    }

    clearCart (): void {
        
        this.http.delete<SuccessBool>(`${baseUrl}/products/cart/clear`, {
            headers: setAuthHeader(this.authToken)
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | SuccessBool) => {

            if (typeof data === "string") {
                this.uiService.setError(data);
            } else {
                this.uiService.setInfo("Cart successfully cleared!");
                this.authService.clearCart();
            }
        });
    }

    deleteItem (id: string): void {

        this.http.delete<SuccessBool>(`${baseUrl}/products/cart/${id}`,
        {headers: setAuthHeader(this.authToken)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data : string | SuccessBool) => {
            if (typeof data === "string") {
                this.uiService.setError(data);
            } else {
                
                this.uiService.setInfo("1 item deleted from cart");
                this.authService.removeFromCart(id);
            }

        });
    }

    checkIfUserHasOrderedAndReviewed (id: string): void {

        const products = this.status.value.products;
        let productChangeIndex = -1;

        products.map((product, i) => {

            if (product._id === id) productChangeIndex = i;
        });

        if (productChangeIndex === -1) return;

        this.http.get<CheckIfOrderedReviewed>(`${baseUrl}/products/${id}/checkorderreview`,
        {headers: setAuthHeader(this.authToken)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: CheckIfOrderedReviewed | string) => {

            if (typeof data === "string") {
                this.uiService.setError(data);
            } else {

                products[productChangeIndex] = {
                    ...products[productChangeIndex],
                    hasOrdered: data.hasOrdered,
                    hasReviewed: data.hasReviewed
                }

                this.status.next({
                    ...this.status.value,
                    products
                });
            }
        });
    }

    clearProducts (): void {

        this.status.next({
            ...this.status.value,
            products: [],
            end: false
        });
    }

    clearSearch (): void {

        const search = {...this.status.value.search};

        delete search.higherprice;
        delete search.higherrating;
        delete search.lowerprice;
        delete search.lowerrating;

        this.status.next({
            ...this.status.value,
            search
        });
    }

    getItems (search?: Search, spread?: boolean): Promise<Product[]> {

        return new Promise((resolve, _) => {
            let headers = {};

            this.setLoading(true);

            if (this.authToken) headers = setAuthHeader(this.authToken);

            let params = "";

            if (search && search.searchstring) {

                StoreService.formatSearchString(search.searchstring);
            }

            if (search) {

                for (const key in search) {
                    if (key !== "searchstring" && key !== "product_ids" && key !== "type") {
                        if (params !== "") params += "&";
                        params += key + "=" + encodeURIComponent(search[key]);
                    }
                }

                if (search.searchstring) {

                    const formattedString = StoreService.formatSearchString(search.searchstring);

                    params += formattedString.map((word, i) => `${params || !params && i !== 0 ? '&' : ''}search[]=${word}`)
                    .join("");
                }

                if (search.product_ids) {

                    params += search.product_ids.map((id, i) => `${params || !params && i !== 0 ? '&' : ''}product_ids[]=${id}`)
                    .join("");
                }

                if (search.type) {

                    params += search.type.map((id, i) => `${params || !params && i !== 0 ? '&' : ''}type[]=${id}`)
                    .join(""); 
                }
            }

            this.http.get<Product[]>(`${baseUrl}/products${search ? '?' + params : ''}`, {
                headers
            }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
            .subscribe((data) => {
                this.setProducts(data, (search && search.limit ? search.limit : 20), spread)

                return resolve(typeof data === "string" ? null : data);
            });
        });
    }

    getSingleItem (id: string): void {

        let headers = {};

        if (this.authToken) headers = setAuthHeader(this.authToken);

        this.setLoading(true);

        this.http.get<Product>(`${baseUrl}/products/${id}`, {headers})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: Product | string) => {

            if (typeof data === "string") {
                this.setProducts(data);
            } else {
                this.setProducts([data]);
            }
        });
    }

    getEqual (): void {

        this.clearProducts();

        let headers = {};
        if (this.authToken) headers = setAuthHeader(this.authToken);

        this.setLoading(true);

        this.http.get<Product[]>(`${baseUrl}/products/getequalamount?limit=24`, {
            headers
        })
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data) => this.setProducts(data, 24));
    }

    resetError (): void {

        this.status.next({
            ...this.status.value,
            error: ""
        });
    }

    private setError (value: string): void {

        this.status.next({
            ...this.status.value,
            loading: false,
            error: value
        });
    }

    private setProducts (data: string | Product[], limit?: number, spread?: boolean) {

        if (typeof data === "string") {

            this.setError(data);
            this.uiService.setError(data);

        }  else {

            this.status.next({
                ...this.status.value,
                products: spread ? <Product[]>removeDuplicates([
                    ...this.status.value.products,
                    ...data
                ]) : data,
                error: "",
                loading: false,
                end: spread && limit ? data.length < limit : false
            });
        }
    }

    private setLoading (value: boolean): void {

        this.status.next({
            ...this.status.value,
            loading: value
        });
    }

    setSearch(search: Search, spread?: boolean): void {

        this.status.next({
            ...this.status.value,
            search: spread ? {
                ...this.status.value.search,
                ...search
            } : search
        });
    }

    static formatSearchString (search: string): string[] {

        const matches = search.match(new RegExp('"', 'gi'))
        let searchString = search.trim().split(' ')

        // search string is normally split word for word
        // Quote marks allows searching for whole strings. Code block
        // below handles that functionality

        if (matches && matches.length >= 2) {
        const indexes: number[] = []
        const splittedString = search.split('')
        for (let j = 0; j < splittedString.length; j++) {
            if (splittedString[j] === '"') {
            indexes.push(j)
            }
        }
        if (indexes.length % 2 !== 0) {
            indexes.splice(indexes.length - 1, 1)
        }
        for (let z = 1; z < indexes.length; z += 2) {
            for (let j = indexes[z - 1]; j <= indexes[z]; j++) {
            if (splittedString[j] === ' ') splittedString[j] = '-'
            if (splittedString[j] === '"') splittedString[j] = ''
            }
        }
        searchString = splittedString.join('').split(' ')
        }
        // cleanup of search string
        for (let r = 0; r < searchString.length; r++) {
            searchString[r] = searchString[r]
            .replace(/[\-]/g, ' ')
            .replace(/[^A-Za-z0-9\s]/g, '')

            if (!searchString[r]) {
                searchString.splice(r, 1);
                r--;
            }
        }

        return searchString;
    }

    createSearchUrl (): Params {

        const search = this.status.value.search;

        const params: Params = {};

        params.searchstring = search.searchstring.replace(/ /g, '+');

        if (search.available) params.available = true;

        if (search.lowerprice !== undefined && search.higherprice !== undefined) {

            params.lowerprice = search.lowerprice;
            params.higherprice = search.higherprice;
        }

        if (search.lowerrating !== undefined && search.higherrating !== undefined) {

            params.lowerrating = search.lowerrating;
            params.higherrating = search.higherrating;
        }

        if (search.type && search.type.length > 0) {

            params.type = JSON.stringify(search.type);
        }

        return params;
    }

    static checkCart = (id: string, shoppingCart: CartItem[]): boolean => {

        if (!shoppingCart) return false;
    
        return shoppingCart.filter((item) => {
    
            const castedItem = <Product>item._id;
    
            return castedItem._id === id;
        }).length > 0;
    }

    static checkChangedAdvanced (search: Search): boolean {

        return (!!search.available || !!search.type || !!search.higherprice || !!search.higherrating);
    }
}