//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Options } from '@angular-slider/ngx-slider';
import { Title } from '@angular/platform-browser';

//local modules
import { StoreService } from '../store.service';
import { Search } from '../store.interface';
import { AuthService } from '../../auth/auth.service';
import { ExtendedProduct } from '../front/product-categories.interface';
import { CartItem } from 'src/app/shared/cartitem.interface';
import { ProductType } from '../product-enums';
import { maxPrice } from 'src/app/shared/constants';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  filterSettingsToLoad: Search = {limit: 20};
  end: boolean;
  error: string;
  filterForm: FormGroup;
  readonly limitOptions = [20, 30, 50, 100];
  loading: boolean;
  loadTimes: number = 0;
  loadTimeout: ReturnType<typeof setTimeout> = null;
  private currentParams: Params;
  private preventRangeEvent: boolean = false;
  products: ExtendedProduct[] = [];
  readonly ProductType: typeof ProductType = ProductType;
  readonly priceOptions: Options = {
    ceil: maxPrice,
    floor: 0,
    step: 0.1
  };
  readonly ratingOptions: Options = {
    ceil: 5,
    floor: 1,
    step: 0.1,
    
  };
  private routeSub: Subscription;
  private shoppingCart: CartItem[];
  showFilter: boolean;
  private storeSub: Subscription;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router, 
    private storeService: StoreService,
    private titleService: Title) { }

  ngOnInit(): void {

    this.storeSub = this.storeService.status
    .subscribe((status) => {

      this.loading = status.loading;
      this.error = status.error;
      this.end = status.end;
      this.products = status.products.map(p => ({
        ...p,
        inShoppingCart: StoreService.checkCart(p._id, this.shoppingCart),
        count: 1
      }))
    });

    this.authSub = this.authService.status
    .subscribe((status) => {

      if (status.user && status.user.shoppingCart) {

        this.shoppingCart = [...status.user.shoppingCart.map(item => ({...item}))];
      } else {
        this.shoppingCart = null;
      }

      this.products = this.products.map(p => ({
        ...p,
        inShoppingCart: StoreService.checkCart(p._id, this.shoppingCart)
      }));
    });

    this.routeSub = this.route.queryParams.subscribe((params: Params) => {

      this.currentParams = {...params};

      this.storeService.clearProducts();
      this.loadTimes = 0;

      let err = false;

      if (params.type && Object.values(ProductType).includes(params.type)) {
        this.filterSettingsToLoad.type = [params.type];
      } 
      else {
        this.filterSettingsToLoad.type = null;

        if (params.type) {
          err = true;
          delete this.currentParams.type;
        }
      }

      if (params.lowerprice && params.higherprice &&
        typeof (+params.lowerprice) === "number" &&
        typeof (+params.higherprice) === "number"
        && +params.higherprice <= maxPrice && +params.lowerprice >= 0
        && +params.lowerprice <= maxPrice && +params.higherprice >= 0) {

          this.filterSettingsToLoad.lowerprice = +params.lowerprice;
          this.filterSettingsToLoad.higherprice = +params.higherprice;
      } 
      else {
        this.filterSettingsToLoad.lowerprice = null;
        this.filterSettingsToLoad.higherprice = null;

        if (params.lowerprice || params.higherprice) {

          delete this.currentParams.lowerprice;
          delete this.currentParams.higherprice;

          err = true;
        }
      }

      if (params.lowerrating && params.higherrating &&
        typeof (+params.lowerrating) === "number" &&
        typeof (+params.higherrating) === "number"
        && +params.higherrating <= 5 && +params.lowerrating >= 1 &&
        +params.higherrating >= 1 && +params.lowerrating <= 5) {

          this.filterSettingsToLoad.lowerrating = +params.lowerrating;
          this.filterSettingsToLoad.higherrating = +params.higherrating;
      } else {
        this.filterSettingsToLoad.lowerrating = null;
        this.filterSettingsToLoad.higherrating = null;

        if (params.lowerrating || params.higherrating) {

          delete this.currentParams.lowerrating;
          delete this.currentParams.higherrating;

          err = true;
        }
      }

      if (params.available === "true") {
        this.filterSettingsToLoad.available = true;
      } else {

        this.filterSettingsToLoad.available = false;

        if (params.available) {

          delete this.currentParams.available;
          err = true;
        }
      }

      if (params.limit && typeof +params.limit === "number" &&
      (+params.limit === 20 || +params.limit === 30 || +params.limit === 50 || +params.limit === 100)) {
        this.filterSettingsToLoad.limit = +params.limit;
      } else {
        this.filterSettingsToLoad.limit = 20;

        if (params.limit) {
          err = true;
          delete this.currentParams.limit;
        }
      }

      if (!this.filterForm) this.initForm();

      if (err) {

        this.fixUrl();

        return;
      }

      this.loadProducts();

    });

    this.titleService.setTitle("Browse Products - Bagare Jönsson");
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.routeSub.unsubscribe();
    this.storeSub.unsubscribe();
  }

  setLimit (value: number): void {

    this.filterSettingsToLoad.limit = value;
  }

  setCategory (value: ProductType): void {

    this.filterSettingsToLoad.type = [value];

    this.updateFilter();
  }

  setSlider (value: number, keyName: string): void {

    if (this.preventRangeEvent) return;

    this.filterForm.patchValue({
      ...this.filterForm.value,
      [keyName]: value
    });

    this.filterForm.controls[keyName].markAsTouched();

    this.updateFilter();
  }

  private fixUrl (): void {

    this.router.navigate([], {queryParams: this.currentParams});
  }

  loadProducts (): void {

    if (this.end) return;

    const objToSend: Search = {limit: this.filterSettingsToLoad.limit};
    if (this.products.length) objToSend.offset = this.products.length;

    if (this.filterSettingsToLoad.type) objToSend.type = this.filterSettingsToLoad.type;

    if (typeof this.filterSettingsToLoad.lowerprice === "number" &&
     typeof this.filterSettingsToLoad.higherprice === "number") {
      objToSend.lowerprice = this.filterSettingsToLoad.lowerprice;
      objToSend.higherprice = this.filterSettingsToLoad.higherprice;
    }

    if (typeof this.filterSettingsToLoad.lowerrating === "number" &&
     typeof this.filterSettingsToLoad.higherrating === "number") {
      objToSend.lowerrating = this.filterSettingsToLoad.lowerrating;
      objToSend.higherrating = this.filterSettingsToLoad.higherrating;
    }

    if (this.filterSettingsToLoad.available) objToSend.available = true;

    this.storeService.getItems(objToSend, true);
    this.loadTimes++;
  }

  toggleCheck (newValue: boolean): void {

    this.filterForm.patchValue({
      ...this.filterForm.value,
      available: newValue
    });

    this.filterForm.controls.available.markAsTouched();

    this.updateFilter();
  }

  setSelectTouched (): void {

    this.filterForm.controls.limit.markAsTouched();
  }

  clearFilters (): void {

    this.filterSettingsToLoad = {type: this.filterSettingsToLoad.type};
    this.initForm();
    
    this.router.navigate([], {queryParams: this.filterSettingsToLoad});
  }

  updateFilter (): void {

    if (this.loadTimeout) {
      clearTimeout(this.loadTimeout);
      this.loadTimeout = null;
    }

    if (this.filterForm.invalid) return;

    const query: Search = {};

    if (this.filterSettingsToLoad.type) query.type = this.filterSettingsToLoad.type;
    if (this.filterForm.value.available) query.available = true;

    if (this.filterForm.controls.priceLow.touched || this.filterForm.controls.priceHigh.touched ||
      this.filterForm.value.priceLow !== 0 || this.filterForm.value.priceHigh !== maxPrice) {
      query.lowerprice = this.filterForm.value.priceLow;
      query.higherprice = this.filterForm.value.priceHigh;
    }

    if (this.filterForm.controls.ratingLow.touched || this.filterForm.controls.ratingHigh.touched
      || this.filterForm.value.ratingLow !== 1 || this.filterForm.value.ratingHigh !== 5) {
      query.lowerrating = this.filterForm.value.ratingLow;
      query.higherrating = this.filterForm.value.ratingHigh;
    }

    if (this.filterForm.controls.limit.touched) {
      query.limit = this.filterForm.value.limit;
    }

    const r = this.router;

    const urlQuery = {
      ...query,
      type: query.type ? query.type[0] : null
    }

    if (!urlQuery.type) delete urlQuery.type;

    this.loadTimeout = setTimeout(() => { //sliders triggers the function tens of times per second,
      r.navigate([], {queryParams: urlQuery}); // need a limit to prevent duplication bugs
    }, 180);

  }

  private initForm (): void {

    this.filterForm = new FormGroup({
      limit: new FormControl(this.filterSettingsToLoad.limit ? this.filterSettingsToLoad.limit : 20,
        [Validators.required, Validators.min(20), Validators.max(100)]),
      available: new FormControl(!!this.filterSettingsToLoad.available),
      priceLow: new FormControl(this.filterSettingsToLoad.lowerprice ? this.filterSettingsToLoad.lowerprice : 0, 
        [Validators.min(0), Validators.max(maxPrice)]),
      priceHigh: new FormControl(this.filterSettingsToLoad.higherprice ? this.filterSettingsToLoad.higherprice : 20, 
        [Validators.required, Validators.min(0), Validators.max(maxPrice)]),
      ratingLow: new FormControl(this.filterSettingsToLoad.lowerrating ? this.filterSettingsToLoad.lowerrating : 1, 
        [Validators.required, Validators.min(1), Validators.max(5)]),
      ratingHigh: new FormControl(this.filterSettingsToLoad.higherrating ? this.filterSettingsToLoad.higherrating : 5,
        [Validators.required, Validators.min(1), Validators.max(5)])
    });

    this.preventRangeEvent = true;

    const instance = this;

    setTimeout(() => instance.preventRangeEvent = false, 600);
  }

}
