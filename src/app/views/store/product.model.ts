//local modules
import { ProductType, InSortiment } from './product-enums';
import { Review } from 'src/app/shared/review.model';

export class Product {

    constructor(
        public _id: string,
        public stock: number,
        public reviews: string[] | Review[],
        public title: string,
        public type: ProductType,
        public inSortiment: InSortiment,
        public description: string,
        public price: number,
        public averageRating: number,
        public imageUrl: string,
        public tags: string[],
        public hasOrdered?: boolean,
        public hasReviewed?: Review,
        public discountPrice?: number
    ){}
}