//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

//local modules
import { StoreService } from '../store.service';
import { maxPrice } from 'src/app/shared/constants';
import { Search } from '../store.interface';
import { UIService } from 'src/app/components/menu/uiService';
import { ProductType } from '../product-enums';
import { AuthService } from '../../auth/auth.service';
import { User } from '../../auth/user.model';
import { ExtendedProduct } from '../front/product-categories.interface';
import { ReviewsService } from '../../reviews/reviews.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private currentParams: Params;
  end: boolean;
  error: string;
  filter: Search = {};
  loading: boolean;
  loadTimes: number = 0;
  products: ExtendedProduct[] = [];
  private routeSub: Subscription;
  private storeSub: Subscription;
  user: User;

  constructor(
    private authService: AuthService,
    private reviewService: ReviewsService,
    private route: ActivatedRoute,
    private router: Router,
    private storeService: StoreService,
    private titleService: Title,
    private uiService: UIService) { }

  ngOnInit (): void {

    this.authSub = this.authService.status
    .subscribe(({user}) => {

      this.user = user;

      this.products = this.products.map(p => ({
        ...p,
        inShoppingCart: StoreService.checkCart(p._id, user ? user.shoppingCart : []),
        count: 1
      }))
    });

    this.routeSub = this.route.queryParams.subscribe((params: Params) => {

      this.currentParams = {...params};
      let err = false;

      if (params.searchstring) {

        this.filter.searchstring = params.searchstring.replace(/\+/g, " ");
        this.titleService.setTitle(`Search results for ${this.filter.searchstring} - Bagare Jönsson`);
      } else {

        this.uiService.setError("Error - no search string provided");
        this.router.navigate(['/']);
      }

      if (params.type) {

        try {

          const parsedArray = JSON.parse(params.type);

          for (const productType of parsedArray) {
  
            if (!Object.values(ProductType).includes(<any>productType)) {
              
              const index = parsedArray.indexOf(productType);
              parsedArray.splice(index, 1);
              
              err = true;
            }
          }
  
          this.currentParams.type = JSON.stringify([...parsedArray]);
          this.filter.type = [...parsedArray];
          
        } catch (_) {

          err = true;
          delete this.currentParams.type;
          delete this.filter.type;
        }

      } else {
        
        delete this.filter.type;
      }

      if (params.available && params.available === "true") {

        this.filter.available = true;
      } else {
        
        delete this.filter.available;

        if (params.available) {

          delete this.currentParams.available;
          err = true;
        }
      }

      if (params.lowerprice && params.higherprice &&
      typeof (+params.lowerprice) === "number" &&
      typeof (+params.higherprice) === "number"
      && +params.higherprice <= maxPrice && +params.lowerprice >= 0
      && +params.lowerprice <= maxPrice && +params.higherprice >= 0) {

        this.filter.lowerprice = +params.lowerprice;
        this.filter.higherprice = +params.higherprice;

      } else {
        
        delete this.filter.lowerprice;
        delete this.filter.higherprice;

        if (params.lowerprice || params.higherprice) {

          delete this.currentParams.lowerprice;
          delete this.currentParams.higherprice;

          err = true;
        }
      }

      if (params.lowerrating && params.higherrating &&
      typeof (+params.lowerrating) === "number" &&
      typeof (+params.higherrating) === "number"
      && +params.higherrating <= 5 && +params.lowerrating >= 1 &&
      +params.higherrating >= 1 && +params.lowerrating <= 5) {

        this.filter.lowerrating = +params.lowerrating;
        this.filter.higherrating = +params.higherrating;

      } else {

        delete this.filter.lowerrating;
        delete this.filter.higherrating;

        if (params.lowerrating || params.higherrating) {

          delete this.currentParams.lowerrating;
          delete this.currentParams.higherrating;

          err = true;
        }
      }

      if (err) {

        this.uiService.setError("Invalid url formatting");

        this.router.navigate([], {
          queryParams: this.currentParams
        });

        return;
      }

      this.storeService.setSearch({...this.filter})
      this.storeService.searchItems();

      this.loadTimes = 1;
    });

    this.storeSub = this.storeService.status
    .subscribe((status) => {

      this.error = status.error;
      this.end = status.end;
      this.loading = status.loading;
      this.products = status.products.map(p => ({
        ...p,
        inShoppingCart: StoreService.checkCart(p._id, this.user ? this.user.shoppingCart : []),
        count: 1
      }))
    });
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.routeSub.unsubscribe();
    this.storeSub.unsubscribe();

    this.reviewService.clearReviews();
  }

  loadMore (): void {

    if (this.end || !this.products.length) return;

    this.storeService.searchItems({offset: this.products.length}, true);
    this.loadTimes++;
  }

}
