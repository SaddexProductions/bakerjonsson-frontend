//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { filter, startWith, switchMap } from 'rxjs/operators';

//local modules
import { ContactService } from '../contact/contact.service';
import { Conversation } from '../contact/conversation.model';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';
import { objectIdRegex } from 'src/app/shared/constants';
import { UIService } from 'src/app/components/menu/uiService';
import { capitalizeNonPipe } from 'src/app/shared/capitalize.function';
import { OrdersService } from '../orders/orders.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private contactSub: Subscription;
  private convEnd: boolean;
  private convId: string;
  conversations: Conversation[] = [];
  private firstConvLoad: boolean = true;
  private iV: ReturnType<typeof setInterval>;
  private loading: boolean;
  private messageLoading: boolean;
  private readonly messageSound: HTMLAudioElement = new Audio('../../../assets/sounds/message.mp3');
  private routeSub: Subscription;
  selectedConversation: number = -1;
  private unreadMessageCount: number = -1;
  private user: User;
  ww: number = window.innerWidth;

  constructor(
    private authService: AuthService,
    private contactService: ContactService,
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private uiService: UIService
  ) { }

  ngOnInit (): void {

    this.orderService.clearOrders();

    this.iV = setInterval(() => {

      this.contactService.checkIfChanged();
    }, 2000);

    this.contactService.reset();

    this.contactService.getConversations({limit: 10});

    this.authSub = this.authService.status
    .subscribe((status) => {

      this.user = status.user;
    });

    this.contactSub = this.contactService.status
    .subscribe((status) => {

      this.conversations = status.conversations;
      this.loading = status.loading;

      this.messageLoading = status.messageLoading;
      this.convEnd = status.convEnd;

      if (this.firstConvLoad && this.conversations.length &&
        this.convId ||
        this.convId &&
        !this.loading &&
        this.selectedConversation === -1 
        && this.conversations.length || 
        this.selectedConversation > -1 &&
        this.convId !== this.conversations[this.selectedConversation]._id) this.selectConv();

      this.firstConvLoad = status.firstConvLoad;
      
      if (status.unreadMessageCount > this.unreadMessageCount && this.unreadMessageCount > -1)  {

        this.messageSound.play();
      }
      this.unreadMessageCount = status.unreadMessageCount;
    });

    this.routeSub = this.router.events.pipe(filter(e => e instanceof NavigationEnd),
    startWith(undefined),
    switchMap(_ => this.route.firstChild!.paramMap))
    .subscribe((paramMap: any) => {

      if (paramMap.params && paramMap.params.id) {

        const {params: { id }} = paramMap;

        this.contactService.clearMessages();

        const isValidObjectId = objectIdRegex.test(id);
        if (!isValidObjectId) {
          this.router.navigate(['/messages/']);
        } else {

          this.convId = id;

          if (!this.firstConvLoad && this.conversations.length) {

            this.selectConv();
          }
        }
      } else {

        this.selectedConversation = -1;
        this.convId = "";
        this.titleService.setTitle("Messages - Bagare Jönsson");
      }
    });
  }

  loadMore (): void {

    if (this.loading || this.convEnd) return;

    this.contactService.getConversations({limit: 10, skip: this.conversations.length}, {spread: true});
  }

  selectConv (): void {

    const convIds = this.conversations.map(c => c._id);

    this.selectedConversation = convIds.indexOf(this.convId);

    if (!this.messageLoading && this.conversations[this.selectedConversation]) {

      this.contactService.getMessages(this.conversations[this.selectedConversation]._id, {limit: 5});

      const otherParticipant = ContactService
      .getOtherParticipant(this.conversations[this.selectedConversation], this.user._id);

      this.titleService
      .setTitle(`Messages from ${capitalizeNonPipe(otherParticipant.name ? otherParticipant.name : otherParticipant.id)} - Bagare Jönsson`);
    }

    if (this.selectedConversation === -1) {

      if (this.convEnd) {

        this.convId = null;

        this.router.navigate(['/messages']);
        this.uiService.setError(`Conversation not found`);

      } else {

        this.loadMore();
      }
    }
  }

  setWidth (): void {

    this.ww = window.innerWidth;
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.contactSub.unsubscribe();
    this.routeSub.unsubscribe();

    if (this.iV) clearInterval(this.iV);

    this.contactService.reset();
  }

}
