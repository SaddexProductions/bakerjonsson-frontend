//npm modules
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { 
  faCheck, 
  faCheckDouble, 
  faHourglassHalf, 
  faExclamationTriangle,
  faTrash
} from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

//local modules
import { User } from '../../auth/user.model';
import { Message } from '../../contact/message.model';
import { OrdersService } from '../../orders/orders.service';
import { Order } from '../../orders/order.model';
import { OrderStatusEnum } from '../../orders/order-status.enum';
import { ContactService } from '../../contact/contact.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit, OnDestroy {

  private contactSub: Subscription;
  @Input() readonly date: boolean;
  readonly faCheck: IconDefinition = faCheck;
  readonly faCheckDouble: IconDefinition = faCheckDouble;
  readonly faExclamationTriangle: IconDefinition = faExclamationTriangle;
  readonly faTrash: IconDefinition = faTrash;
  readonly faHourglassHalf: IconDefinition = faHourglassHalf;
  finalOutput: string;
  @Input() readonly firstFromUser: boolean;
  @Input() readonly message: Message;
  order: Order;
  orderCancelled: boolean;
  orderError: string;
  orderId: string;
  orderLoading: boolean;
  readonly orderStatus: typeof OrderStatusEnum = OrderStatusEnum;
  private orderSub: Subscription;
  @Input() readonly retry: boolean;
  subject: boolean;
  timeStamp: Date = new Date();
  @Input() readonly user: User;
  you: boolean;

  constructor(
    private contactService: ContactService,
    private orderService: OrdersService) { }

  ngOnInit(): void {

    this.contactSub = this.contactService.status
    .subscribe(({timeStamp}) => {

      if (timeStamp) this.timeStamp = timeStamp;
    });

    this.you = this.message.from && this.user._id === this.message.from._id;

    this.finalOutput = this.message.content.replace(/(.*?)\&order\:[a-f\d]{24}(.*)/g,"");

    if (this.message.content.match(/(.*?)\&order\:[a-f\d]{24}(.*)/g)) {

      this.orderCancelled = /(?<=.)#cancelled\;/.test(this.message.content);

      this.orderId = this.message.content.split("&order:")[1].replace(/(?<=.)(\#cancelled)?;(.*)/g, "");

      if (this.orderId) this.orderSub = this.orderService.status
      .subscribe(status => {

        if (this.orderCancelled && this.order && this.order.status !== OrderStatusEnum.CANCELLED) 
          this.orderService.setOrderCancelled(this.order._id);

        this.orderError = status.error;

        if (!this.order) {
          this.orderLoading = status.loading;
        }

        const order = status.orders.find(o => o._id === this.orderId);
        if (!order && !this.orderLoading && !this.orderError) {

          this.orderService.getSingleOrder(this.orderId);
        }
        else if (order) this.order = order;
      });
    }
  }

  ngOnDestroy (): void {

    if (this.orderSub) this.orderSub.unsubscribe();

    this.contactSub.unsubscribe();
  }

  cancelOrder (): void {

    const proceed = confirm("Are you sure you want to cancel this order?");

    if (proceed) {

      this.orderService.cancelOrder(this.order._id);
    }
  }

  deleteUnsentMessage (): void {

    const shouldDelete
    = confirm("Are you sure you want to delete this message? This action can't be undone");

    if (shouldDelete && this.message.localId) 
      this.contactService.deleteUnsent(this.message.localId);
  }

  retrySend (): void {

    this.contactService.resendReplies(this.message.conversation._id);
  }

}
