//npm modules
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//local modules
import { SharedModule } from 'src/app/components/shared.module';
import { MessagesComponent } from './messages.component';
import { MessageComponent } from './message/message.component';
import { MessagePanelComponent } from './message-panel/message-panel.component';
import { ConversationPanelComponent } from './conversation-panel/conversation-panel.component';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {path: '', component: MessagesComponent, children: [
                {path: ':id', component: MessagePanelComponent},
                {path: '', component: ConversationPanelComponent}
            ]}
        ]),
        SharedModule
    ],
    declarations: [ConversationPanelComponent, MessagesComponent, MessageComponent, MessagePanelComponent]
})
export class MessagesModule {}