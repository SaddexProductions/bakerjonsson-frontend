//npm modules
import { 
  Component, 
  OnInit, 
  OnDestroy, 
  Input, 
  OnChanges, 
  ElementRef, 
  ViewChild, 
  AfterViewChecked } from '@angular/core';
import { Subscription } from 'rxjs';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { 
  faPaperPlane, 
  faTimes, 
  faEllipsisV,
  faEllipsisH,
  faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import cloneDeep from 'lodash/cloneDeep';

//local modules
import { Conversation, Participant } from '../../contact/conversation.model';
import { AuthService } from '../../auth/auth.service';
import { User } from '../../auth/user.model';
import { Message } from '../../contact/message.model';
import { ContactService } from '../../contact/contact.service';
import { objectIdRegex } from 'src/app/shared/constants';
import { Reply } from '../../contact/contact.interfaces';
import { swipe, SwipeInit } from 'src/app/shared/swipe';

@Component({
  selector: 'app-message-panel',
  templateUrl: './message-panel.component.html',
  styleUrls: ['./message-panel.component.css']
})
export class MessagePanelComponent implements OnInit, OnChanges, OnDestroy, AfterViewChecked {

  private authSub: Subscription;
  @ViewChild('bottomRef') private bottomRef: ElementRef;
  private contactSub: Subscription;
  conversation: Conversation;
  emailNotificationsPlaceholder: boolean;
  private convId: string;
  private conversations: Conversation[];
  readonly faArrowDown: IconDefinition = faArrowDown;
  readonly faEllipsisV: IconDefinition = faEllipsisV;
  readonly faEllipsisH: IconDefinition = faEllipsisH;
  readonly faPaperPlane: IconDefinition = faPaperPlane;
  readonly faTimes: IconDefinition = faTimes;
  @Input() readonly inputConversation: Conversation;
  loading: boolean;
  private loadmore: boolean = true;
  me: Participant;
  messageEnd: boolean;
  messageError: string;
  messageForm: FormGroup;
  messageLoading: boolean;
  messageMenu: boolean;
  messages: Message[] = [];
  messagesToRead: boolean;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  notScrolledDownEntirely: boolean;
  private routeSub: Subscription;
  scrollChanged: boolean;
  private shouldScroll: boolean;
  private sht: number = 0;
  subject: boolean;
  private swipeCoord: [number, number] = [0, 0];
  swipeDiff: number = 0;
  private swipeTime: number = 0;
  private timeout: ReturnType<typeof setTimeout>;
  private timeStamp: Date;
  @ViewChild('beginningSpan') private topRef: ElementRef;
  private unsentMessages: Message[] = [];
  user: User;

  constructor(
    private authService: AuthService,
    private contactService: ContactService,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {

    this.timeStamp = new Date();

    document.addEventListener('touchstart', () => {}, {passive: false});
    document.addEventListener('touchmove', () => {}, {passive: false});

    this.authSub = this.authService.status
    .subscribe((status) => {

      this.user = status.user;
    });

    this.contactSub = this.contactService.status
    .subscribe((status) => {
      this.conversations = status.conversations;

      this.messageEnd = status.messageEnd;
      if (status.timeStamp) this.timeStamp = status.timeStamp;
      this.messageLoading = status.messageLoading;
      this.loading = status.loading;

      if (this.conversation && !this.messages.length && status.messages.length && !this.loading && !this.messageLoading) {

        this.contactService.markAllAsRead(this.conversation._id);
      }

      if (status.messages.length !== this.messages.length || 
        this.unsentMessages.length !== status.unsentMessages.length || 
        this.messages.length && this.user && this.contactService.checkIfReadDiff(this.messages, this.user._id) ||
        this.messagesToRead && !status.messages.filter(m => !m.read && (
          m.fromAdmin || !m.from || m.from && m.from._id !== this.user._id
        )).length)
        this.messages = cloneDeep(status.messages);

        this.messagesToRead = this.checkUnread();

        this.notScrolledDownEntirely = !!this.messages.length && this.notScrolledDownEntirely;

      if (this.conversation &&
      this.messages.length &&
      this.messages[this.messages.length-1]._id !== this.conversation.messages[0]._id
       && !this.messageLoading && this.loadmore) {

        this.loadmore = false;

        this.contactService
        .getMessages(this.conversation._id, {limit: 5, newerThan: this.messages[this.messages.length-1].createdAt}, true);

        this.timeout = setTimeout(() => {

          this.loadmore = true;
        }, 5000);
      }

      if (status.unsentMessages.length) {

        this.messages = this.messages.concat(status.unsentMessages);
      }

      this.shouldScroll = status.unsentMessages.length !== this.unsentMessages.length;

      this.unsentMessages = cloneDeep(status.unsentMessages);

      this.messageError = status.messageError;
      if (status.timeStamp) this.timeStamp = status.timeStamp;

      if (!this.inputConversation && this.convId && objectIdRegex.test(this.convId)) this.setConv();

    });

    if (!this.inputConversation) {

      this.routeSub = this.route.params
      .subscribe((params: Params) => {

        this.convId = params.id;

        if (objectIdRegex.test(this.convId)) this.setConv();
      });
    }
  }

  touchMove (e: TouchEvent): void {

    if (!this.myScrollContainer.nativeElement.scrollTop) {

      if (e.changedTouches[0].clientY > this.swipeCoord[1]) e.preventDefault();

      if (this.swipeDiff < 50 && !this.messageEnd)
      this.swipeDiff = e.changedTouches[0].clientY - this.swipeCoord[1] > 0 ? e.changedTouches[0].clientY - this.swipeCoord[1] : 0;
      else if (this.messageEnd) this.swipeDiff = 0;
    } 
  }

  touchScroll (e: TouchEvent, when: 'start' | 'end'): void {

    const swipeResult = swipe({
      swipeTime: this.swipeTime,
      swipeCoord: this.swipeCoord,
      e,
      when
    });

    if (swipeResult instanceof(SwipeInit)) {

      this.swipeCoord = swipeResult.swipeCoord;
      this.swipeTime = swipeResult.swipeTime;
    } else if (swipeResult && swipeResult.plane === 'vertical') {

      this.swipeDiff = 0;

      if ((swipeResult.direction === 'next') && this.myScrollContainer.nativeElement.scrollTop < 100)
        this.loadMoreMessages();
    }
  }

  ngOnChanges (): void {

    if (this.inputConversation) {

      if (this.conversation && this.conversation._id !== this.inputConversation._id || !this.messageForm) {
        this.initForm();

        this.messageMenu = false;
      }

      if (this.conversation) this.scrollChanged = this.conversation._id === this.inputConversation._id && this.scrollChanged;
  
      this.subject = this.subject && this.conversation._id === this.inputConversation._id;
      this.conversation = this.inputConversation;

      this.setMe();

    }

    this.markAsReadIfScrolledDownToBottom();

  }

  preventAutoBottomScroll (e: WheelEvent): void {

    if (e.deltaY < 0) {

      this.scrollChanged = true;
    }
  }

  preventAutoBottomScrollTouchEvent (e): void {

    if (e.deltaY > 0) {

      this.scrollChanged = true;
    }
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.contactSub.unsubscribe();

    clearTimeout(this.timeout);

    if (this.routeSub) this.routeSub.unsubscribe();
  }

  ngAfterViewChecked (): void {

    if (!this.scrollChanged) this.scrollDown();

    if (this.shouldScroll) {

      this.scrollToBottom();
    }
  }

  dateDiffFiveMins (date1: string, date2: string): boolean {

    //IMPORTANT: date1 and date2 must be date strings

    return (new Date(date1).getTime() - new Date(date2).getTime())/1000/60 > 4;
  }

  async markAsReadIfScrolledDownToBottom (): Promise<void> {

    if (!!this.conversation && !!this.myScrollContainer &&
      !this.notScrolledDownEntirely &&
      !this.loading && this.messagesToRead) {

      const success = await this.contactService.markAllAsRead(this.conversation._id);

      if (success) {

        this.messagesToRead = this.checkUnread();
      }
    }
  }

  formKeyPress (e: KeyboardEvent): void {

    if (e.which === 13) {

      this.reply();
    }
  }

  toggleMessageMenu (e: Event): void {

    e.stopPropagation();

    this.messageMenu = !this.messageMenu;
  }

  async loadMoreMessages (scrollTop?: boolean): Promise<void> {

    this.setNotScrolledEntirelyToBottom();

    if (this.loading || this.messageLoading || !this.conversation || this.messageEnd) return;

    const getResult = await this.contactService
    .getMessages(this.conversation._id, {limit: 5, skip: this.messages.length}, true);

    if (scrollTop && getResult) {

      this.scrollChanged = true;

      this.topRef.nativeElement
      .scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
  }

  getOtherParticipantName (): string {

    const oP = this.getOtherParticipant();

    return oP.name ? oP.name : oP.id;
  }

  getOtherParticipant (): Participant {

    const part = ContactService.getOtherParticipant(this.conversation, this.user._id);

    part.typingNow = ContactService.setTypingNow(this.timeStamp, new Date(part.lastTyping));

    return part;
  }

  reply (): void {

    if (!this.subject && this.messageForm.value.subject) {

      this.messageForm.get("subject").reset();
    }

    if (this.messageForm.invalid) return;

    const objToSend: Reply = {
      content: this.messageForm.value.content,
      from: this.user._id,
      to: this.getOtherParticipant().id,
      conversation_id: this.conversation._id
    };

    if (this.messageForm.value.subject) {

      objToSend.subject = this.messageForm.value.subject;
    }

    this.messageForm.reset();

    this.contactService.chatReply(objToSend);
  }

  scrollMessageWindow (): void {

    this.sht = this.myScrollContainer.nativeElement.scrollTop + 
    this.myScrollContainer.nativeElement.offsetHeight;

    this.setNotScrolledEntirelyToBottom();

    this.scrollChanged = this.sht < this.myScrollContainer.nativeElement.scrollHeight;

  }

  private setNotScrolledEntirelyToBottom () {

    this.notScrolledDownEntirely
     = this.myScrollContainer.nativeElement.scrollHeight - this.sht > 200;
  }

  setConv (): void {

    this.conversation = this.conversations.find(c => c._id === this.convId);

    if (!this.messageForm) this.initForm();

    this.setMe();
  }

  private checkUnread (): boolean {

    return this.messages.length && !!this.messages
    .filter(m => m.from._id !== this.user._id && !m.read)
    .length;
  }

  private setMe (): void {

    if (this.conversation && this.user) {

      this.me = this.conversation.participants
      .find(p => p.id === this.user._id);
    }
  }

  setSendEmail (): void {

    if (!this.me) return;

    this.contactService
    .setEmailNotice(this.conversation._id, !this.me.sendEmail, this.user._id);
  }

  setTyping (): void {

    if (!this.conversation) return;

    this.contactService.setTyping(this.conversation._id);
  }

  scrollToBottom (): void {

    this.shouldScroll = false;

    this.bottomRef.nativeElement
    .scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
  }

  private scrollDown (): void {

    if (this.myScrollContainer) {

      this.sht = this.myScrollContainer.nativeElement.scrollTop + 
      this.myScrollContainer.nativeElement.offsetHeight;

      try {
        this.myScrollContainer.nativeElement.scrollTop = this.sht;
      } catch(err) { }
    }
  
  }

  private initForm (): void {

    const pattern = /^(?!\&order\:).*$/;

    this.messageForm = new FormGroup({
      subject: new FormControl("", [
        Validators.minLength(3), 
        Validators.maxLength(100), 
        Validators.pattern(pattern)]),
      content: new FormControl("", [
        Validators.required, 
        Validators.maxLength(300), 
        Validators.pattern(pattern)])
    });
  }

}
