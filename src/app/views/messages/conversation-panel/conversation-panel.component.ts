//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import cloneDeep from 'lodash/cloneDeep';

//local modules
import { AuthService } from '../../auth/auth.service';
import { Conversation } from '../../contact/conversation.model';
import { ContactService } from '../../contact/contact.service';
import { User } from '../../auth/user.model';

@Component({
  selector: 'app-conversation-panel',
  templateUrl: './conversation-panel.component.html',
  styleUrls: ['./conversation-panel.component.css']
})
export class ConversationPanelComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  private contactSub: Subscription;
  convEnd: boolean;
  conversations: Conversation[] = [];
  error: string;
  loading: boolean;
  messageCount: number = 0;
  noConvLoadingSpin: boolean;
  user: User;

  constructor(
    private authService: AuthService,
    private contactService: ContactService,
    private router: Router) { }

  ngOnInit(): void {

    this.authSub = this.authService.status
    .subscribe(status => {

      this.user = status.user;
    });

    this.contactSub = this.contactService.status
    .subscribe(status => {

      const isConvsInOrder = this.convIsInOrder(cloneDeep(status.conversations));

      if (!this.conversations.length || !isConvsInOrder) {

        this.conversations = cloneDeep(status.conversations);
      } else if (
        this.conversations.length && isConvsInOrder &&
        status.conversations.length > this.conversations.length
      ) {

        const diff = status.conversations.length - this.conversations.length;

        const indexToStart = status.conversations.length - diff;

        this.conversations.push.apply(this.conversations,
          status.conversations.slice(indexToStart, status.conversations.length)
        );

      }

      this.checkUpdateDates(status.conversations);

      this.convEnd = status.convEnd;
      this.error = status.error;
      this.loading = status.loading;
      this.noConvLoadingSpin = status.noConvLoadingSpin;
    });
  }

  ngOnDestroy(): void {

    this.authSub.unsubscribe();
    this.contactSub.unsubscribe();
  }

  loadMore (): void {

    if (this.loading || this.convEnd) return;

    this.contactService.getConversations({limit: 10, skip: this.conversations.length}, {spread: true});
  }

  async markAllAsRead (): Promise<void> {

    const result = await this.contactService.markAllAsRead();

    if (result) {

      this.contactService.reset();

      this.contactService.getConversations({limit: 10});
      this.router.navigate(['/messages']);
    }
  }

  private convIsInOrder (statusConvs: Conversation[]): boolean {

    if (!this.conversations) return false;

    if (statusConvs.length && statusConvs.length > this.conversations.length) {

      statusConvs.length = this.conversations.length;
    }

    return !!this.conversations && 
    JSON.stringify(this.conversations.map(c => c._id)) === JSON.stringify(statusConvs.map(c => c._id));

  }

  private checkUpdateDates (statusConvs: Conversation[]): void {

    this.conversations.map((conv, i, obj) => {

      if (conv.updatedAt !== statusConvs[i].updatedAt) obj[i] = statusConvs[i];
    });
  }

}
