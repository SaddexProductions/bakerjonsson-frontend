//npm modules
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//local modules
import { SharedModule } from 'src/app/components/shared.module';
import { OrdersComponent } from './orders.component';
import { SingleOrderComponent } from './single-order/single-order.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ConfirmPaymentComponent } from './checkout/confirm-payment/confirm-payment.component';
import { CancelComponent } from './checkout/cancel/cancel.component';
import { SuccessComponent } from './checkout/success/success.component';

@NgModule({
    imports: [
        CommonModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {path: "checkout", component: CheckoutComponent},
            {path: "checkout/success/:id", component: SuccessComponent},
            {path: "checkout/:id", component: ConfirmPaymentComponent},
            {path: "cancel/:id", component: CancelComponent},
            {path: ":id", component: SingleOrderComponent},
            {path: "", component: OrdersComponent}
        ]),
        SharedModule
    ],
    declarations: [OrdersComponent, SingleOrderComponent, CheckoutComponent, ConfirmPaymentComponent, CancelComponent, SuccessComponent]
})
export class OrdersModule {}