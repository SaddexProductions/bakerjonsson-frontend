//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { faTimes, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Title } from '@angular/platform-browser';

//local modules
import { Order } from './order.model';
import { OrdersService } from './orders.service';
import { User } from '../auth/user.model';
import { AuthService } from '../auth/auth.service';
import { OrderFilter, OrderStatusBoxes, SmallProduct } from './orders.interfaces';
import { SortOrdersEnum } from './sort-orders.enum';
import { OrderStatusEnum } from './order-status.enum';
import { SortOrder } from '../store/item/reviews/sort-order.enum';
import { UIService } from 'src/app/components/menu/uiService';
import { StoreService } from '../store/store.service';
import { Product } from '../store/product.model';
import { objectIdRegex } from 'src/app/shared/constants';
import { keyDirection } from 'src/app/shared/key-direction';
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  readonly checkedBoxes: OrderStatusBoxes = {
    processing: true,
    dispatched: true,
    delievered: true,
    cancelled: false
  };
  autoComplete: boolean;
  private currentParams: Params;
  end: boolean;
  error: string;
  private firstLoad: boolean = true;
  readonly faTimes: IconDefinition = faTimes;
  indLoading: string;
  loading: boolean;
  readonly options: OrderFilter = {};
  orders: Order[];
  private orderSub: Subscription;
  orderValue: SortOrder = SortOrder.DESCENDING;
  presetCountry: Country;
  private productIdExists: boolean;
  readonly productSearchTags: SmallProduct[] = [];
  productSuggestions: Product[];
  private routeSub: Subscription;
  searchFocus: boolean;
  searchValue: string = "";
  selectIndex: number = -1;
  readonly sortOrdersEnum: typeof SortOrdersEnum = SortOrdersEnum;
  sortValue: SortOrdersEnum = SortOrdersEnum.DATE;
  user: User;

  constructor(
    private authService: AuthService,
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private router: Router,
    private storeService: StoreService,
    private titleService: Title,
    private uiService: UIService) { }

  ngOnInit(): void {

    this.currentParams = {...this.route.snapshot.queryParams};

    this.setUrl(true);

    this.authService.getAndSaveCountry();

    this.authSub = this.authService.status
    .subscribe((status) => {

      this.user = status.user;
      this.presetCountry = status.presetCountry;
      this.titleService
      .setTitle(`Orders made by ${this.user.firstName} ${this.user.lastName} - Bagare Jönsson`);
    });

    this.orderSub = this.orderService.status
    .subscribe((status) => {

      this.end = status.end;
      this.error = status.error;
      this.indLoading = status.indLoading;
      this.loading = status.loading;
      this.orders = status.orders;
      this.productIdExists = !!status.productId;

      if (this.productIdExists || status.singleLoaded) this.orderService.clearOrders();
    });

    this.routeSub = this.route.queryParams
    .subscribe(async (params: Params) => {

      this.currentParams = {...params};

      this.options.ascending = params.order === SortOrder.ASCENDING;
      this.orderValue = params.order === SortOrder.ASCENDING ? SortOrder.ASCENDING : SortOrder.DESCENDING;
      this.options.sort = params.sort && params.sort === SortOrdersEnum.SUM ?
      this.options.sort = SortOrdersEnum.SUM : this.options.sort = SortOrdersEnum.DATE;
      this.sortValue = this.options.sort;

      let err = false;

      if (params.status) {

        try {

          const filterArray: string[] = JSON.parse(params.status);
  
          for (const key in this.checkedBoxes) {
  
            this.checkedBoxes[key] = false;
          }
          
          for (const filter of filterArray) {
  
            if (!Object.values(OrderStatusEnum).includes(<any>filter)) {
              
              const index = filterArray.indexOf(filter);
              filterArray.splice(index, 1);
              
              err = true;
            } else {
  
              this.checkedBoxes[filter.toLowerCase()] = true;
            }
          }
  
          this.currentParams.status = JSON.stringify([...filterArray]);
          this.options.status = [...filterArray];
  
        } catch (_) {
          
          delete this.currentParams.status;
          err = true;
          this.uiService.setError("Invalid url syntax");
        }
      } else {
        delete this.options.status;
      }

      if (params.order && !Object.values(SortOrder).includes(params.order) ||
      params.sort && !Object.values(SortOrdersEnum).includes(params.sort)) {

        delete this.currentParams.sort;
        delete this.currentParams.order;
        err = true;

        this.uiService.setError("Invalid sort and order values in url");
      }

      //the below function parses the products param,
      //and adds the product tags if the ids are correct
      //on the component initial load
      if (params.products) {

        try {
          const parsedArray: string[] = JSON.parse(params.products);

          if (parsedArray.length !== this.productSearchTags.length) {
            parsedArray.map((product_id) => {

              if (!objectIdRegex.test(product_id)) throw new Error(""); 
            });
  
            const products = await this.storeService
            .getItems({product_ids: [...parsedArray], limit: 5});
  
            if (products && products.length > 0) {
  
              if (products.length !== parsedArray.length) {
  
                parsedArray.map((el, i) => {
  
                  const isInArray = products.filter(p => p._id === el).length;
  
                  if (!isInArray) {
                    parsedArray.splice(i, 1);
                  }
                });
  
                this.currentParams.products = JSON.stringify(parsedArray);
  
                this.setUrl();
                return; 
              }
  
              this.productSearchTags.length = 0;
  
              products.map(p => this.productSearchTags.push({
                _id: p._id,
                title: p.title
              }));
  
            } else {
  
              throw new Error("404");
            }
          }

        } catch (ex) {

          delete this.options.product_id;
          this.uiService.setError(ex.message === "404" ?
          "No products with these ids found"
          : "Invalid url syntax");
          err = true;
        }
        this.options.product_id = this.productSearchTags.map(t => t._id);
      } else {

        delete this.options.product_id;
      }

      if (err) {
        this.setUrl();

        return;
      }

      if (params.reload) {

        delete this.currentParams.reload;
        this.firstLoad = false;

        if (!this.firstLoad) {
          this.setUrl();
          return;
        }
      }

      if (!this.orders.length) this.orderService.getOrders(this.options);
    });
  }

  addToSearchProducts (item: Product): void {

    this.searchValue = "";
    this.selectIndex = -1;

    this.productSearchTags.push({
      _id: item._id,
      title: item.title
    });

    this.autoComplete = false;

    this.productSuggestions = [];

    this.setUrl();
  }

  closeAutoComplete (): void {

    this.productSuggestions = [];
    this.selectIndex = -1;

    this.autoComplete = false
  }

  deleteTag (index: number): void {

    this.productSearchTags.splice(index, 1);

    this.setUrl();
  }

  async searchProducts (): Promise<void> {

    if (this.searchValue.length >= 2) {
      this.autoComplete = true;

      this.productSuggestions = 
      this.cleanDuplicates(await this.storeService.getItems({searchstring: this.searchValue, limit: 10}));

    } else {
      this.autoComplete = false;
    }
  }

  setSort (e: Event): void {
    const value = <SortOrdersEnum>(e.target as HTMLSelectElement).value;
    this.sortValue = value;

    this.setUrl();
  }

  setOrder (e: Event): void {
    const value = <SortOrder>(e.target as HTMLSelectElement).value;
    this.orderValue = value;

    this.setUrl();

  }

  enterKeyUp (e: KeyboardEvent): void {

    const dir = keyDirection(e);

    if (dir === 0) {

      this.addToSearchProducts(this.productSuggestions[this.selectIndex]);
    }
  }

  scrollThrough (e: KeyboardEvent): void {

    const dir = keyDirection(e);

    if (dir) {

      e.preventDefault();

      if (dir < 0 && this.selectIndex > 0 ||
        dir > 0 && this.selectIndex < (this.productSuggestions.length-1) && this.selectIndex >= 0) {
        this.selectIndex += dir
      } else if (dir < 0 && this.selectIndex <= 0) {


        this.selectIndex = -1;
      } else if (dir > 0 && this.selectIndex < 0) {
        this.selectIndex = 0;
      }
    }
  }

  loadMore (): void {

    if (this.end) return;

    this.orderService.getOrders({...this.options, offset: this.orders.length}, true);

  }

  private cleanDuplicates (newProducts: Product[]): Product[] {

    const result: Product[] = [];

    newProducts.map((product, i) => {

      const exists = this.productSearchTags.filter(p => p._id === product._id).length > 0;
      if (!exists) result.push(product);
    });

    return result;
  }

  //navigates to url once a filter setting is changed
  private setUrl (first?: boolean): void {

    //if it's not first load, orders are cleared
    if (!first) this.orderService.clearOrders();

    if (first && 
    (!this.currentParams.status || !this.currentParams.sort || !this.currentParams.order) ||
     !first) {

      const newParams = {...this.currentParams};
      const paramsArray = [];

      //order status param, array filter
      if (first && !this.currentParams.status || !first) {
        for (const key in this.checkedBoxes) {

          if (this.checkedBoxes[key]) paramsArray.push(OrderStatusEnum[key.toUpperCase()]);
        }
    
        if (paramsArray.length > 0) {
    
          newParams.status = JSON.stringify(paramsArray);
    
        } else if (paramsArray.length === 0 && newParams.status) {
          delete newParams.status;
        }
      }

      //order and sort params
      if (first && (!this.currentParams.order || !this.currentParams.sort) || !first) {

        newParams.order = this.orderValue;
        newParams.sort = this.sortValue;
      }

      //products search tags, array filter
      if (this.productSearchTags.length) {

        const productIds = this.productSearchTags.map(t => t._id);

        newParams.products = JSON.stringify([...productIds]);

      } else if (!this.productSearchTags.length && newParams.products) {

        delete newParams.products;
      }

      if (Object.keys(newParams).length) this.orderService.setQuery({...newParams});
  
      this.router.navigate([], {queryParams: newParams});
    }
  }

  toggleBox (value: boolean, key: string): void {

    this.checkedBoxes[key] = value;

    this.setUrl();
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.orderSub.unsubscribe();
    this.routeSub.unsubscribe();
  }

}
