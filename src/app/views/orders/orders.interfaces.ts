//local modules
import { Order } from './order.model';
import { SortOrdersEnum } from './sort-orders.enum';
import { SortOrder } from '../store/item/reviews/sort-order.enum';
import { StreetAddress } from 'src/app/shared/address.interface';

export interface OrderServiceStatus {
    end: boolean;
    error: string;
    info: string;
    indLoading: string;
    loading: boolean;
    orders: Order[];
    productId: string;
    savedQuery: SavedQuery;
    singleLoaded: boolean;
    successfullyUpdated: string;
}

export interface OrderFilter {

    product_id?: string[] | string;
    offset?: number;
    status?: string[];
    ascending?: boolean;
    sort?: SortOrdersEnum;
}

export interface OrderStatusBoxes {
    processing: boolean;
    dispatched: boolean;
    delievered: boolean;
    cancelled: boolean;
}

export interface SmallProduct {
    _id: string;
    title: string;
}

export interface SavedQuery {
    sort?: SortOrdersEnum;
    ascending?: SortOrder;
    status?: string;
    products?: string;
}

export interface CreateOrderInterface {
    express: boolean;
    address?: StreetAddress;
}

export interface PaypalResponse {
    href: string;
    rel: string;
    method: string;
}

export interface PaymentDTO {
    payer_id: string;
    payment_id: string;
}