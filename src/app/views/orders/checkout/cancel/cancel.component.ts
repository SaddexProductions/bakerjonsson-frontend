//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

//local modules
import { Order } from '../../order.model';
import { OrdersService } from '../../orders.service';
import { OrderStatusEnum } from '../../order-status.enum';
import { StreetAddress } from 'src/app/shared/address.interface';
import { AuthService } from 'src/app/views/auth/auth.service';

@Component({
  selector: 'app-cancel',
  templateUrl: './cancel.component.html',
  styleUrls: ['./cancel.component.css']
})
export class CancelComponent implements OnInit, OnDestroy {

  address: StreetAddress;
  private authSub: Subscription;
  error: string;
  loading: boolean;
  order: Order;
  private orderId: string;
  private orderSub: Subscription;

  constructor(
    private authService: AuthService,
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private titleService: Title) { }

  ngOnInit(): void {

    this.orderId = this.route.snapshot.params.id;
    this.titleService.setTitle("Order cancelled - Bagare Jönsson");

    this.getOrder();

    this.authSub = this.authService.status
    .subscribe((status) => {

      if (status.user.address && (!this.order || this.order.address)) {

        this.address = status.user.address;

      } else if (!status.user.address && !status.loading && !this.loading) {

        this.authService.getUserWithAddress();
      }
    });

    this.orderSub = this.orderService.status
    .subscribe((status) => {

      this.error = status.error;
      this.loading = status.loading;

      this.order = status.orders.find(o => o.order_id === this.orderId);
      if (this.order && this.order.status !== OrderStatusEnum.CANCELLED && !this.loading) {

        this.cancelOrder();
      }
    });
  }

  private cancelOrder (): void {

    this.orderService.cancelOrder(this.order._id);
  }

  getOrder (): void {

    this.orderService.getSingleOrder(this.orderId, true);
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.orderSub.unsubscribe();
  }

}
