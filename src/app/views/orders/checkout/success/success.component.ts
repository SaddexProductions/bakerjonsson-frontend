//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

//local modules
import { Order } from '../../order.model';
import { OrdersService } from '../../orders.service';
import { AuthService } from 'src/app/views/auth/auth.service';
import { StreetAddress } from 'src/app/shared/address.interface';
import { OrderStatusEnum } from '../../order-status.enum';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit, OnDestroy {

  address: StreetAddress;
  private authError: string;
  private authLoading: boolean;
  private authSub: Subscription;
  error: string;
  loading: boolean;
  order: Order;
  private orderError: string;
  private orderLoading: boolean;
  orderId: string;
  readonly orderStatus: typeof OrderStatusEnum = OrderStatusEnum;
  private orderSub: Subscription;

  constructor(
    private authService: AuthService,
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit(): void {

    this.orderId = this.route.snapshot
    .params.id;

    this.titleService.setTitle("Order successful - Bagare Jönsson");

    this.loadOrder();

    this.authSub = this.authService.status
    .subscribe((status) => {

      this.authLoading = status.loading;
      this.authError = status.error;
      this.loading = this.orderLoading || this.authLoading;

      if (this.authError && !this.orderError) this.error = this.authError;

      if (this.order && !this.order.address && status.user.address) {

        this.address = status.user.address;
      }
    });

    this.orderSub = this.orderService.status
    .subscribe((status) => {

      this.orderError = status.error;
      this.orderLoading = status.loading;
      this.loading = this.orderLoading || this.authLoading;

      this.order = status.orders.find(o => o.order_id === this.orderId);

      if (this.orderError && !this.authError || this.orderError && this.authError) 
        this.error = this.orderError;

      if (this.order && !this.order.address) {

        this.authService.getUserWithAddress();
      } else if (this.order && this.order.address) {

        this.address = this.order.address;
      }

      if (this.order && this.order.status === this.orderStatus.CANCELLED)
        this.router.navigate(['/orders/cancel/' + this.order.order_id], {replaceUrl: true});
    });
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.orderSub.unsubscribe();
  }

  cancel (): void {

    const cancel = confirm("Are you sure you want to cancel this order?");

    if (cancel) this.router.navigate(['/orders/cancel/' + this.order.order_id], {replaceUrl: true});
  }

  loadOrder (): void {
    this.orderService.getSingleOrder(this.orderId, true); 
  }

}
