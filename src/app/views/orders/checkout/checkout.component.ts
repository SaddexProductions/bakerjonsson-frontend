//npm modules
import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

//local modules
import { AuthService } from '../../auth/auth.service';
import { Option } from 'src/app/components/radio-buttons/option.interface';
import { Product } from '../../store/product.model';
import { nameRegex } from 'src/app/shared/constants';
import { CreateOrderInterface } from '../orders.interfaces';
import { OrdersService } from '../orders.service';
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit, OnDestroy, AfterViewChecked {

  acceptedTerms: boolean;
  addressForm: FormGroup;
  addressMode: number = 0;
  authSub: Subscription;
  error: string;
  express: boolean;
  loading: boolean;
  readonly options: Option[] = [
    {
      value: "standard",
      description: "Standard address"
    },
    {
      value: "standard",
      description: "Custom"
    }
  ];
  private notSent: boolean;
  private orderSub: Subscription;
  private presetCountry: Country;
  selectedCountry: Country;
  shoppingCart: {count: number; _id: Product}[] = [];
  sum: number = 0;

  constructor(private authService: AuthService,
    private orderService: OrdersService,
    private titleService: Title) { }

  ngOnInit (): void {

    this.orderService.resetError();

    this.titleService.setTitle("Checkout - Bagare Jönsson");

    this.authService.getAndSaveCountry();

    this.authSub = this.authService
    .status.subscribe((status) => {

      if (status.presetCountry) {
        this.presetCountry = status.presetCountry;
        this.selectedCountry = status.presetCountry;
      }

      if (status.user) {

        this.shoppingCart = status.user.shoppingCart.map(item => {

          const castedItem = <Product>item._id;

          return {
            count: item.count,
            _id: castedItem
          }
        });
      }

      this.calculateSum();

      if (this.presetCountry && !this.addressForm) this.initForm();
    });

    this.orderSub = this.orderService.status
    .subscribe((status) => {

      this.error = status.error;
      this.loading = status.loading;

      if (this.loading) this.notSent = false;
    });
  }

  ngAfterViewChecked(): void {
    if (this.error && !this.loading && !this.notSent) {

      this.notSent = true;

      window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    }
  }

  calculateSum (): void {

    this.sum = 0;

    this.shoppingCart.map(item => {

      const castedItem = <Product>item._id;

      this.sum += (castedItem.discountPrice ? castedItem.discountPrice : castedItem.price)*item.count;
    });

    const expressSum = this.express ? 3.99 : 0;

    this.sum = Math.floor((this.sum + expressSum)*100)/100;
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.orderSub.unsubscribe();
  }

  setCountry (country: Country): void {

    this.selectedCountry = country;

    this.addressForm.patchValue({
      ...this.addressForm.value,
      countryCode: country.code.toUpperCase()
    });
  }

  order (): void {

    const toSend: CreateOrderInterface = {
      express: this.express
    };

    if (this.addressMode) {

      if (this.addressForm.invalid) return;

      toSend.address = {
        ...this.addressForm.value
      };

    }

    this.orderService.order(toSend);
  }

  private initForm (): void {

    this.addressForm = new FormGroup({
      street: new FormControl("", [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(200)
      ]),
      additional: new FormControl("", [
        Validators.maxLength(50)
      ]),
      zip: new FormControl("", [
        Validators.required,
        Validators.maxLength(10),
      ]),
      city: new FormControl("", [
        Validators.required,
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      province: new FormControl("", [
        Validators.maxLength(200),
        Validators.pattern(nameRegex)
      ]),
      countryCode: new FormControl(this.presetCountry.code, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(3)
      ])
    });
  }

}
