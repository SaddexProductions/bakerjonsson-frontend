//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

//local modules
import { OrdersService } from '../../orders.service';
import { Order } from '../../order.model';
import { PaymentDTO } from '../../orders.interfaces';

@Component({
  selector: 'app-confirm-payment',
  templateUrl: './confirm-payment.component.html',
  styleUrls: ['./confirm-payment.component.css']
})
export class ConfirmPaymentComponent implements OnInit, OnDestroy {

  error: string;
  info: string;
  loading: boolean;
  order: Order;
  orderId: string;
  private paymentDetails: PaymentDTO;
  private orderSub: Subscription;

  constructor(
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit(): void {

    this.titleService.setTitle("Confirm your payment - Bagare Jönsson");

    this.orderId = this.route.snapshot.params.id;

    const { paymentId, PayerID } = this.route.snapshot.queryParams;
    if (!paymentId || !PayerID) this.router.navigate(['/'], {replaceUrl: true});

    this.paymentDetails = {
      payment_id: paymentId,
      payer_id: PayerID
    };

    this.loadOrder();

    this.orderSub = this.orderService.status
    .subscribe((status) => {

      this.error = status.error;
      this.loading = status.loading;
      this.info = status.info;

      if (this.info) this.router.navigate(['/orders/checkout/success/' + this.order.order_id], { replaceUrl: true});

      this.order = status.orders.find(o => o.order_id === this.orderId);
    });
  }

  ngOnDestroy (): void {

    this.orderSub.unsubscribe();

    this.orderService.clearOrders();
    this.orderService.resetError();
  }

  loadOrder (): void {

    this.orderService.getSingleOrder(this.orderId, true);
  }

  confirmPayment (): void {

    this.orderService.confirmOrder(this.orderId, this.paymentDetails);
  }

}
