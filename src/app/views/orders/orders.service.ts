//npm modules
import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import cloneDeep from 'lodash/cloneDeep';

//local modules
import { OrderServiceStatus, OrderFilter, SavedQuery, CreateOrderInterface, PaypalResponse, PaymentDTO } from './orders.interfaces';
import { getToken } from 'src/app/shared/getToken';
import { setAuthHeader } from '../auth/set-auth-header';
import { Order } from './order.model';
import { baseUrl } from 'src/app/shared/constants';
import handleError from 'src/app/shared/handleError';
import { StreetAddress } from 'src/app/shared/address.interface';
import { UIService } from 'src/app/components/menu/uiService';
import { SuccessBool } from 'src/app/shared/success.interface';
import { OrderStatusEnum } from './order-status.enum';
import { removeDuplicates } from 'src/app/shared/removeDuplicateObjects';

@Injectable({providedIn: 'root'})
export class OrdersService {

    private readonly initState: OrderServiceStatus = {

        loading: false,
        error: "",
        indLoading: null,
        info: "",
        orders: [],
        productId: "",
        savedQuery: null,
        singleLoaded: false,
        successfullyUpdated: null,
        end: false
    };

    readonly status: BehaviorSubject<OrderServiceStatus> = new BehaviorSubject({...this.initState});

    constructor (private http: HttpClient, private uiService: UIService) {}

    clearOrders (): void {

        this.status.next({
            ...this.initState
        });
    }

    getOrders (filter?: OrderFilter, spread?: boolean): void {

        let headers = {};
        const token = getToken();

        if (token) {
            headers = setAuthHeader(token);
        } else {
            return;
        }

        let params = "";

        if (filter) {
            for (const key in filter) {
                if (params[key] !== "" && key !== "status" && key !== "product_id") {
                    params += `${params ? '&' : ''}${key}=${encodeURIComponent(filter[key])}`;
                    
                }
                if (key === "status" || key === "product_id") { //those two keys will most of the time be arrays of strings

                    let arrayToParse: string[];
                    if (!Array.isArray(filter.product_id) && key === "product_id") 
                        arrayToParse = [filter.product_id];
                    else if (Array.isArray(filter[key])) arrayToParse = <string[]>filter[key];

                    params += arrayToParse.map((s, i) => `${params || !params && i !== 0 ? '&' : ''}${key}[]=${s}`
                    ).join("");
                }
            }

            params = params.replace(/%2C/g, ",");
        }

        if (!spread) {
            this.status.next({
                ...this.status.value,
                end: false
            });
        }

        this.setLoading(true);

        this.http.get<Order[]>(`${baseUrl}/orders/${filter ? '?' + params : ''}`,
        { headers }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Order[]) => {

            if (typeof data === "string") {
                this.setError(data);
            } else {

                this.status.next({
                    ...this.status.value,
                    orders: spread ? <Order[]>removeDuplicates([...this.status.value.orders, ...data])
                    : data,
                    loading: false,
                    error: "",
                    productId: filter && filter.product_id && !Array.isArray(filter.product_id) ? 
                        filter.product_id : "",
                    info: "",
                    end: data.length < 20,
                    singleLoaded: false
                });
            }
        });
    }

    getSingleOrder (id: string, uuid?: boolean): void {

        const token = getToken();
        if (!token) return;

        this.setLoading(true);

        this.http.get<Order>(`${baseUrl}/orders/${uuid ? "getbyuuid/" : ""}${id}`, {
            headers: setAuthHeader(token)
        })
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: Order | string) => {

            if (typeof data === "string") {

                this.uiService.setError(data);
                this.setError(data);
            } else {

                this.status.next({
                    ...this.status.value,
                    loading: false,
                    error: "",
                    singleLoaded: true,
                    orders: <Order[]>removeDuplicates([
                        ...this.status.value.orders,
                        data
                    ])
                });
            }
        });
    }

    editOrderAddress (id: string, data: StreetAddress | boolean): void {

        if (typeof data !== "boolean") {

            for (const key in data) {

                if (!data[key]) delete data[key];
            }
        }

        const token = getToken();
        if (!token) return;
        
        this.setIndLoading(id);

        this.http.put<Order>(`${baseUrl}/orders/changeaddress/${id}`, typeof data === "boolean" ? {
            useCustomAddress: false
        } : {
            useCustomAddress: true,
            newAddress: data
        },
        {
            headers: setAuthHeader(token)
        }
        ).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Order) => {

            if (typeof data === "string") {
                
                this.uiService.setError(data);
                this.setIndLoading(null);
            } else {

                this.status.next({
                    ...this.status.value,
                    successfullyUpdated: data._id
                });

                this.uiService.setInfo(`Delievery address for order
                ${data.order_id} successfully updated`);

                this.setOrder(data);
            }
        });
    }

    cancelOrder (id: string): void {
        
        const token = getToken();
        if (!token) return;

        this.setIndLoading(id);

        this.http.put<SuccessBool>(`${baseUrl}/orders/cancel/${id}`, {}, {
            headers: setAuthHeader(token)
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | SuccessBool) => {

            if (typeof data === "string") {
                
                this.uiService.setError(data);
                this.setIndLoading(null);
            } else {

                const orderToCancel = {...this.status.value
                .orders.filter(o => o._id === id)[0]};

                orderToCancel.status = OrderStatusEnum.CANCELLED;

                this.setOrder(orderToCancel);

                this.uiService.setInfo(`The order ${orderToCancel.order_id} was cancelled.
                Refund will commence shortly`);
            }
        });
    }

    setOrderCancelled (id: string): void {

        const orders = cloneDeep(this.status.value.orders);

        orders.map((o, i, obj) => {

            if (o._id === id) {

                obj[i].status = OrderStatusEnum.CANCELLED;
            };
        });

        this.status.next({
            ...this.status.value,
            orders
        });
    }

    order (orderInfo: CreateOrderInterface): void {

        const token = getToken();
        if (!token) return;

        this.setLoading(true);

        this.http.post<PaypalResponse[]>(`${baseUrl}/orders`, orderInfo,
        { headers: setAuthHeader(token)})
        .pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | PaypalResponse[]) => {

            if (typeof data === "string") {
                this.setError(data);

            } else {
                window.location.replace(data[0].href);
            }
        });
    }

    confirmOrder (uuid: string, paymentDTO: PaymentDTO): void {

        const token: string = getToken();
        if (!token) return;

        this.setLoading(true);

        this.http.post<Order>(`${baseUrl}/orders/checkout/${uuid}`,
        paymentDTO, {
            headers: setAuthHeader(token)
        }).pipe(catchError((err: HttpErrorResponse) => of(handleError(err))))
        .subscribe((data: string | Order) => {

            if (typeof data === "string") {
                
                this.setError(data);
            } else {

                this.setOrder(data);
                this.setInfo("Payment successful");
            }
        });
    }

    setQuery (value: SavedQuery): void {

        this.status.next({
            ...this.status.value,
            savedQuery: {
                ...value
            }
        });
    }

    private setIndLoading (value: string): void {

        this.status.next({
            ...this.status.value,
            indLoading: value
        });
    }

    private setLoading (value: boolean): void {
        this.status.next({
            ...this.status.value,
            loading: value
        });
    }

    private setOrder (order: Order): void {

        const orders: Order[] = [...this.status.value.orders];

        let index = -1;

        orders.map((o, i) => {

            if (o._id === order._id) index = i;
        });

        if (index > -1) {

            orders[index] = order;

        } else {
            orders.push(order);
        }

        this.status.next({
            ...this.status.value,
            indLoading: null,
            orders
        });
    }

    private setError (value: string): void {

        this.status.next({
            ...this.status.value,
            error: value,
            loading: false
        });
    }

    resetError (): void {
        this.status.next({
            ...this.status.value,
            error: ""
        });
    }

    private setInfo (value: string): void {

        this.status.next({
            ...this.status.value,
            info: value,
            loading: false
        });
    }
}