//local modules
import { CartItem } from 'src/app/shared/cartitem.interface';
import { Product } from '../store/product.model';
import { OrderStatusEnum } from './order-status.enum';
import { StreetAddress } from 'src/app/shared/address.interface';

export class Order {

    constructor (
        public _id: string,
        public sum: number,
        public products: CartItem[],
        public user: {
            _id: string;
        },
        public orderedAt: string,
        public estimatedDelievery: string,
        public status: OrderStatusEnum,
        public express?: boolean,
        public address?: StreetAddress,
        public product_docs?: Product[],
        public order_id?: string,
        public payer_id?: string,
        public payment_id?: string
    ){}
}