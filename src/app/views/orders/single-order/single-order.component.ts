//npm modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

//local modules
import { Order } from '../order.model';
import { OrdersService } from '../orders.service';
import { User } from '../../auth/user.model';
import { AuthService } from '../../auth/auth.service';
import { SavedQuery } from '../orders.interfaces';
import { Country } from 'src/app/shared/get-country.interface';

@Component({
  selector: 'app-single-order',
  templateUrl: './single-order.component.html',
  styleUrls: ['./single-order.component.css']
})
export class SingleOrderComponent implements OnInit, OnDestroy {

  private authSub: Subscription;
  edit: boolean;
  error: string;
  indLoading: string;
  loading: boolean;
  order: Order;
  private orderId: string;
  private orderSub: Subscription;
  presetCountry: Country;
  productId: string;
  query: SavedQuery;
  private routeSub: Subscription;
  user: User;

  constructor(
    private authService: AuthService,
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title) { }

  ngOnInit (): void {

    this.orderId = this.route.snapshot.params.id;

    this.authSub = this.authService.status
    .subscribe((status) => {

      this.user = status.user;
      this.presetCountry = status.presetCountry;

      if (!this.presetCountry) this.authService.getAndSaveCountry();
    });

    this.orderSub = this.orderService.status
    .subscribe((status) => {

      this.error = status.error;
      this.indLoading = status.indLoading;
      this.loading = status.loading;
      this.productId = status.productId;
      this.query = status.savedQuery;

      const orderFound = status.orders
      .find(o => o._id === this.orderId);

      if (!orderFound && !status.loading && !status.error) {

        this.loadOrder();
      } else {

        if (orderFound) this.order = orderFound;

        this.titleService.setTitle(`Order id ${this.orderId} - Bagare Jönsson`);
      }

      if (this.order && status.successfullyUpdated === this.order._id) {

        this.router.navigate([], {replaceUrl: true});
        this.edit = false;
      }

      if (!this.order && this.error && !this.loading) {

        this.titleService.setTitle("Error loading order - Bagare Jönsson");
      }
    });

    this.routeSub = this.route.queryParams
    .subscribe((params: Params) => {

      if (params.changeaddress) this.edit = true;
    });
  }

  ngOnDestroy (): void {

    this.authSub.unsubscribe();
    this.orderSub.unsubscribe();
    this.routeSub.unsubscribe();
  }

  loadOrder (): void {

    this.orderService.getSingleOrder(this.orderId);
  }

  findProductTitleFromId (id: string): string {

    const product = this.order.product_docs.find(p => p._id === id);

    return product.title;
  }
}
