//npm modules
import { FormGroup, ValidatorFn } from '@angular/forms';

//local modules
import * as postalCodeRegex from '../postalCodeData.json';

export const postalCodeValidator: ValidatorFn = (form: FormGroup): null => {
    
    const countryCode = form.value.countryCode.toUpperCase() || "DE" ;

    const regexString = postalCodeRegex.postalCodeData.find(p => p.territoryId === countryCode);
    let regex;

    if (regexString) {
        regex = new RegExp(regexString.regex, 'i');
    }

    const zip = form.get("zip");
    const errors = zip.errors;

    if (regex && !regex.test(zip.value) && zip.value && zip.value.length <= 10) {

        zip.setErrors({invalidFormat: true});
    } else {
        if (errors) errors.invalidFormat = null;

        zip.setErrors(errors);
    }

    return null;
}