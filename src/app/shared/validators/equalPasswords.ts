//npm modules
import { ValidatorFn, FormGroup } from '@angular/forms';

export const equalPasswordsValidator: ValidatorFn = (form: FormGroup): null => {
    
    const password = form.get("password");
    const repeatPassword = form.get("repeatPassword");

    if(password.value !== repeatPassword.value) {

        repeatPassword.setErrors({mustMatch: true});
    } else {
        repeatPassword.setErrors(null);
    }

    return null;
}