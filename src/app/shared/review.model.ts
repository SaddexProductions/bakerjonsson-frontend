//local modules
import { User } from '../views/auth/user.model';
import { Product } from '../views/store/product.model';

export class Review {

    constructor (
        public _id: string,
        public creator: {
            name: string;
            _id: string | User;
        },
        public product: {
            title: string;
            _id: string | Product;
        },
        public title: string,
        public rating: number,
        public createdAt: string,
        public updatedAt: string,
        public content?: string,
        public voteSum?: number,
        public hasVoted?: {
            positive: boolean;
        }
    ){}
}