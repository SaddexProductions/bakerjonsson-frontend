//local modules
import { HelperInterface } from './helper.interface';

export const twoFactorHelper: HelperInterface = {

    steps: [
        {
            desc: "Download and install Authy",
            link: true,
            url: "https://authy.com/download/",
            img: "qrcode.jpeg"
        },
        {
            desc: 'Open the app'
        },
        {
            desc: "Click on Baker Jönsson"
        },
        {
            desc: 'Enter the token when logging in'
        }
    ],
    or: [
        {
            desc: 'Request an SMS token when logging in'
        },
        {
            desc: 'Enter the token in the SMS'
        }
    ]
};