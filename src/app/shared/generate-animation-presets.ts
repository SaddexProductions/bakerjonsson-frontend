//npm modules
import { trigger, AnimationTriggerMetadata, state, style, animate, transition } from '@angular/animations'

export const generateExpandCollapse = (interpolation: string): AnimationTriggerMetadata => {

    return trigger('simpleExCoAnimation', [

        // the "in" style determines the "resting" state of the element when it is visible.
        state('in', style({height: "100%"})),
  
        // expand when created. this could also be written as transition('void => *')
        transition(':enter', [
          style({height: 0}),
          animate(interpolation)
        ]),
  
        // collapse when destroyed. this could also be written as transition('void => *')
        transition(':leave',
          animate(interpolation, style({height: 0})))
      ])
}

export const generateFadeAnimation = (duration: number): AnimationTriggerMetadata => {

    return trigger('fade', [

        // the "in" style determines the "resting" state of the element when it is visible.
        state('in', style({opacity: 1})),
  
        // fade in when created. this could also be written as transition('void => *')
        transition(':enter', [
          style({opacity: 0}),
          animate(duration)
        ]),
  
        // fade out when destroyed. this could also be written as transition('void => *')
        transition(':leave',
          animate(duration, style({opacity: 0})))
    ]);
}