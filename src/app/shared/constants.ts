//local modules
import { environment } from 'src/environments/environment';

export const baseUrl: string = environment.production ? '/api' : 'http://localhost:3000';

export const passwordRegex: RegExp = /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

export const nameRegex: RegExp = /^([ \u00c0-\u01ffa-zA-Z'\-])+$/i;

export const zipRegex: RegExp = /[0-9]{4,10}$/;

export const phoneRegex: RegExp = /^[\d\s]+$/;

export const objectIdRegex: RegExp = /^[a-f\d]{24}$/i;

export const maxPrice: number = 20;