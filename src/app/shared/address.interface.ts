export interface StreetAddress extends Map<string, string> {
    street: string;
    city: string;
    zip: string;
    additional?: string;
    province?: string;
    countryCode?: string;
};