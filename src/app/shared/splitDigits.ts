export const splitDigits = (inputString: string, obj: {
    value: string;
    offset: number;
    max?: number;
}) => {
    let str: string | string[];
    if (obj.value.length <= inputString.length) {
        str = inputString.replace(/\s/g, '').split('');
        const objOffset = obj.offset || 0;
        for (let i = 0; i < str.length; i++) {
            if ((i + objOffset) % 4 === 0 && str[i] !== " " && i !== 0) {
                str.splice(i, 0, ' ');
            }
        }
    }
    else {
        str = inputString.split('');
        if (str[str.length - 1] === ' ') str.splice(str.length - 1, 1);
    }
    if (obj.max && str.length > obj.max) str.length = 9;
    str = str.join('');

    return str;
}