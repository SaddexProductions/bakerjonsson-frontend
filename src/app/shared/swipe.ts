export const swipe = (swipeInputObj: swipeInput): SwipeInit | swipeResult => {

    const {e, when, useDuration, swipeTime, swipeCoord, distance} = swipeInputObj;
    
    const coord: [number, number] = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
    const time = new Date().getTime();
  
    if (when === 'start') {
      return new SwipeInit(time, coord);
    } else if (when === 'end') {
      const direction = [coord[0] - swipeCoord[0], coord[1] - swipeCoord[1]];
      const duration = time - swipeTime;

      const dirToUse = Math.abs(direction[0]) > Math.abs(direction[1]) ? 0 : 1;

      const distanceToUse = distance ? distance : 30;
  
      if ((!useDuration || useDuration && duration < useDuration) && distanceToUse < Math.abs(direction[dirToUse])) {

        return {
        plane: dirToUse ? 'vertical' : 'horizontal',
        direction: direction[dirToUse] > 0 ? 'next' : 'previous'
        } 
      }

      return null;
    }
}

interface swipeInput {

    e: TouchEvent;
    swipeCoord: [number, number];
    swipeTime: number;
    useDuration?: number;
    distance?: number;
    when: 'start' | 'end';
};

export class SwipeInit {

    constructor(public swipeTime: number, public swipeCoord: [number, number]) {}

}

interface swipeResult {

    plane: 'vertical' | 'horizontal';
    direction: 'next' | 'previous';
}