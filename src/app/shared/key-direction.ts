export const keyDirection = (e: KeyboardEvent): number => {

    let ret: number;

    switch (e.which) {

        case (38):
            ret = -1;
            break;
        case (40):
            ret = 1;
            break;
        case (13):
            ret = 0;
            break;
        default:
            ret = null;
    }

    return ret;
}