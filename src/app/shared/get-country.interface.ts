//npm modules
import { SafeHtml } from '@angular/platform-browser';

export interface GetCountry {
    code: string;
}

export interface Country {
    name: string;
    code: string;
    dial_code: string;
    icon: SafeHtml;
};