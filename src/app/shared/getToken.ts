export const getToken = (): string => {
    if (document.cookie && document.cookie.indexOf("token") > -1) {
        let cook;
        let value = "; " + document.cookie;
        let parts = value.split("; token=");
        if (parts.length == 2) cook = parts.pop().split(";").shift();
  
        if (cook) {
  
          return cook;

        }
    }

    return null;
}