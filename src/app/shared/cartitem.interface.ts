//local modules
import { Product } from '../views/store/product.model';

export interface CartItem {
    count: number;
    _id: string | Product;
    discountPriceAtOrder?: number;
}