export const isInViewport = (yourElement: Element, offset: number = 0): boolean => {
    if (!yourElement) return false;
    const top = yourElement.getBoundingClientRect().top;
    return (top + offset) >= 0 && (top - offset) <= window.innerHeight;
}