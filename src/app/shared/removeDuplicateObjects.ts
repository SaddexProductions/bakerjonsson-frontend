export const removeDuplicates = (inputArray: anyObject[]): anyObject[] => {

    return inputArray.filter((thing, index, self) =>
        index === self.findIndex((t) => (
            t._id === thing._id
        ))
    )
}

interface anyObject {

    _id: string;
    [key: string]: any;
}