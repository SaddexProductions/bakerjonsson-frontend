//npm modules
import { HttpErrorResponse } from '@angular/common/http';

//http statustext doesn't work properly if the backend has downtime problems on Chrome
const responseStatus500: {code: number; message: string}[] = [
    {
        code: 500,
        message: "Internal Server Error"
    },
    {
        code: 501,
        message: "Not Implemented"
    },
    {
        code: 502,
        message: "Bad Gateway"
    },
    {
        code: 503,
        message: "Service Unavailable"
    },
    {
        code: 504,
        message: "Gateway Timeout"
    },
    {
        code: 505,
        message: "HTTP Version Not Supported"
    },
    {
        code: 506,
        message: "Variant Also Negotiates"
    },
    {
        code: 507,
        message: "Insufficient Storage"
    },
    {
        code: 508,
        message: "Loop Detected"
    },
    {
        code: 510,
        message: "Not Extended"
    },
    {
        code: 511,
        message: "Network Authentication Required"
    },
    {
        code: 599,
        message: "Network Connect Timeout Error"
    }
]

export default function (error: HttpErrorResponse) {

    if (error.status !== 0 && error.status <= 500) {

        try {
            return JSON.parse(error.error.replace('/', '')).message;
        }   catch (_) {
            
            if (Array.isArray(error.error.message)) return error.error.message[0];

            return error.error.message;
        }
    } else if (error.status) {

        const toDisplay = error.statusText !== "OK" ? error.statusText
        : responseStatus500.find(code => code.code === error.status).message;

        return `${error.status} ${toDisplay}`;
    }   else {
        return 'Network Error';
    }
}