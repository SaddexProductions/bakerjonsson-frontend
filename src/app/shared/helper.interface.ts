export interface HelperInterface {

    steps: {
        link?: boolean;
        url?: string;
        img?: string;
        desc: string;
    }[];

    or?: {
        desc: string;
    }[]
}